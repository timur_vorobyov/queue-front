import { routes } from "src/config/routes";
import { townId, userId } from "src/helpers/auth-data";
import apiClient from "src/helpers/httpInterceptor";
import {
  BranchOffice,
  BranchOffices,
  VisitPurpose,
  VisitPurposes,
} from "./ProfilePageService";

class ProfilePageAdapter {
  public async getBranchOfficeList(): Promise<BranchOffices> {
    const response = await apiClient.get(
      routes.api.branchOfficesList + encodeURIComponent(townId())
    );

    return this.toBranchOfficeArray(response.data);
  }

  private toBranchOfficeArray(branchOfficeList: []): BranchOffices {
    const formatedBranchOffices = branchOfficeList.map(
      (branchOffice: BranchOffice) => ({
        id: branchOffice.id,
        branchOfficeName: branchOffice.branchOfficeName,
      })
    );

    return formatedBranchOffices;
  }

  public async getVisitPurposeList(
    branchOfficeId: number
  ): Promise<VisitPurposes> {
    const response = await apiClient.get(
      routes.api.visitPurposesList + encodeURIComponent(branchOfficeId)
    );
    return this.toVisitPurposeArray(response.data);
  }

  private toVisitPurposeArray(visitPurposeList: []): VisitPurposes {
    const formatedVisitPurposes = visitPurposeList.map(
      (visitPurpose: VisitPurpose) => ({
        id: visitPurpose.id,
        name: visitPurpose.name,
      })
    );

    return formatedVisitPurposes;
  }

  public async updateUserSettings(
    userIdWithSameDeskNumber: number | null,
    deskNumber: string,
    branchOfficeId: string,
    visitPurposesIds: number[]
  ): Promise<string> {
    const response = await apiClient.post(routes.api.updateUserWorkData, {
      userId: userId(),
      userIdWithSameDeskNumber: userIdWithSameDeskNumber,
      deskNumber: deskNumber,
      branchOfficeId: branchOfficeId,
      visitPurposesIds: visitPurposesIds,
    });
    const message = response.data.message;

    if (message == "User's settings are updated") {
      localStorage.setItem(
        "queue-user-data",
        JSON.stringify(response.data.userData)
      );
    }

    return message;
  }

  public async checkDeskNumberExistance(
    deskNumber: string,
    branchOfficeId: string,
    visitPurposesIds: number[]
  ): Promise<{
    userIdWithSameDeskNumber: number | null;
    userFirstName: string;
    message: string;
  }> {
    const response = await apiClient.post(routes.api.deskNumberCheck, {
      userId: userId(),
      deskNumber: deskNumber,
      branchOfficeId: branchOfficeId,
      visitPurposesIds: visitPurposesIds,
    });
    return response.data;
  }
}

export default ProfilePageAdapter;
