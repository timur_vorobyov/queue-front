import ProfilePageAdapter from "./ProfilePageAdapter";

export type BranchOffices = { id: number; branchOfficeName: string }[];
export type BranchOffice = { id: number; branchOfficeName: string };
export type VisitPurposes = { id: number; name: string }[];
export type VisitPurpose = { id: number; name: string };

class ProfilePageService {
  private serviceAdapter: ProfilePageAdapter;

  constructor() {
    this.serviceAdapter = new ProfilePageAdapter();
  }

  public getBranchOfficeList(): Promise<BranchOffices> {
    return this.serviceAdapter.getBranchOfficeList();
  }

  public getVisitPurposeList(branchOfficeId: number): Promise<VisitPurposes> {
    return this.serviceAdapter.getVisitPurposeList(branchOfficeId);
  }

  public updateUserSettings(
    userIdWithSameDeskNumber: number | null,
    deskNumber: string,
    branchOfficeId: string,
    visitPurposesIds: number[]
  ): Promise<string> {
    return this.serviceAdapter.updateUserSettings(
      userIdWithSameDeskNumber,
      deskNumber,
      branchOfficeId,
      visitPurposesIds
    );
  }

  public checkDeskNumberExistance(
    deskNumber: string,
    branchOfficeId: string,
    visitPurposesIds: number[]
  ): Promise<{
    userIdWithSameDeskNumber: number | null;
    userFirstName: string;
    message: string;
  }> {
    return this.serviceAdapter.checkDeskNumberExistance(
      deskNumber,
      branchOfficeId,
      visitPurposesIds
    );
  }
}

export default ProfilePageService;
