import QueueServicePageAdapter from "./QueueServicePageAdapter";

export type Tickets = {
  id: number;
  crmCustomerId: number;
  ticketFullNumber: string;
  customerFullName: string;
  customerPhoneNumber: string;
  createdAt: string;
  pendingTime: string;
  visitPurposeId: number;
  visitPurposeName: string;
  isSalomCache: boolean;
}[];
export type Ticket = {
  id: number;
  crmCustomerId: number;
  ticketFullNumber: string;
  customerFullName: string;
  customerPhoneNumber: string;
  createdAt: string;
  pendingTime: string;
  visitPurposeName: string;
  visitPurposeId: number;
  isSalomCache: boolean;
};
export type VisitPurposes = { id: number; name: string }[];
export type VisitPurpose = { id: number; name: string };
export type Services = { id: number; name: string }[];
export type Service = { id: number; name: string };
export type UserStatuses = {
  id: number;
  name: string;
}[];

export type UserStatus = {
  id: number;
  name: string;
};
class QueueServicePageService {
  private serviceAdapter: QueueServicePageAdapter;

  constructor() {
    this.serviceAdapter = new QueueServicePageAdapter();
  }

  public getTicketsPendingList(): Promise<Tickets | null> {
    return this.serviceAdapter.getTicketsPendingList();
  }

  public getAcceptedTicket(): Promise<Ticket | null> {
    return this.serviceAdapter.getAcceptedTicket();
  }

  public getTicketServing(): Promise<Ticket | null> {
    return this.serviceAdapter.getTicketServing();
  }

  public acceptTicket(): void {
    return this.serviceAdapter.acceptTicket();
  }

  public serveTicket(ticketId: number): void {
    return this.serviceAdapter.serveTicket(ticketId);
  }

  public declineTicket(ticketId: number): void {
    return this.serviceAdapter.declineTicket(ticketId);
  }

  public completeService(
    ticketId: number,
    serviceId: string,
    visitPurposeId: string
  ): void {
    return this.serviceAdapter.completeService(
      ticketId,
      serviceId,
      visitPurposeId
    );
  }

  public getVisitPurposeList(): Promise<VisitPurposes> {
    return this.serviceAdapter.getVisitPurposeList();
  }

  public getServiceList(visitPurposeId: number): Promise<VisitPurposes | null> {
    return this.serviceAdapter.getServiceList(visitPurposeId);
  }

  public getCustomersAmountUserServed(): Promise<string> {
    return this.serviceAdapter.getCustomersAmountUserServed();
  }

  public getUserAverageServiceTime(): Promise<string | null> {
    return this.serviceAdapter.getUserAverageServiceTime();
  }

  public getUsersAverageServiceTime(): Promise<string | null> {
    return this.serviceAdapter.getUsersAverageServiceTime();
  }

  public getUsersMaxServiceTime(): Promise<string | null> {
    return this.serviceAdapter.getUsersMaxServiceTime();
  }
}

export default QueueServicePageService;
