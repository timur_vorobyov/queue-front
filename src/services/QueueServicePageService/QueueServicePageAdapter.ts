import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import {
  Service,
  Services,
  Ticket,
  Tickets,
  UserStatus,
  UserStatuses,
  VisitPurpose,
  VisitPurposes,
} from "./QueueServicePageService";
import {
  branchOfficeId,
  userId,
  visitPurposesIds,
} from "src/helpers/auth-data";

class QueueServicePageAdapter {
  public async getTicketsPendingList(): Promise<Tickets | null> {
    if (branchOfficeId() != null && visitPurposesIds().length != 0) {
      const response = await apiClient.get(routes.api.fullTicketsList, {
        params: {
          branchOfficeId: branchOfficeId(),
          statusName: "new",
          visitPurposesIds: visitPurposesIds(),
        },
      });
      return response.data;
    }

    return null;
  }

  public async getAcceptedTicket(): Promise<Ticket | null> {
    if (branchOfficeId() != null && visitPurposesIds().length != 0) {
      const response = await apiClient.get(routes.api.ticket, {
        params: {
          branchOfficeId: branchOfficeId(),
          statusName: "invited",
          visitPurposesIds: visitPurposesIds(),
          userId: userId(),
        },
      });

      return response.data;
    }

    return null;
  }

  public async getTicketServing(): Promise<Ticket | null> {
    if (branchOfficeId() != null && visitPurposesIds().length != 0) {
      const response = await apiClient.get(routes.api.ticket, {
        params: {
          branchOfficeId: branchOfficeId(),
          statusName: "serving",
          visitPurposesIds: visitPurposesIds(),
          userId: userId(),
        },
      });

      return response.data;
    }

    return null;
  }

  public acceptTicket(): void {
    apiClient.put(routes.api.acceptTicket, {
      userId: userId(),
    });
  }

  public serveTicket(ticketId: number): void {
    apiClient.put(routes.api.serveTicket, {
      userId: userId(),
      ticketId: ticketId,
    });
  }

  public declineTicket(ticketId: number): void {
    apiClient.put(routes.api.declineTicket, {
      userId: userId(),
      ticketId: ticketId,
    });
  }

  public completeService(
    ticketId: number,
    serviceId: string,
    visitPurposeId: string
  ): void {
    apiClient.put(routes.api.completeService, {
      visitPurposeId: Number(visitPurposeId),
      userId: userId(),
      ticketId: ticketId,
      serviceId: serviceId,
    });
    this.updateStatisticsData();
  }

  private async updateStatisticsData(): Promise<void> {
    await apiClient.post(routes.api.updateStatisticsData, {
      userId: userId(),
      visitPurposesIds: visitPurposesIds(),
      branchOfficeId: branchOfficeId(),
    });
  }

  public async getVisitPurposeList(): Promise<VisitPurposes> {
    const response = await apiClient.get(
      routes.api.visitPurposesList + encodeURIComponent(branchOfficeId())
    );
    return this.toVisitPurposeFrontFormat(response.data);
  }

  private toVisitPurposeFrontFormat(visitPurposeList: []): VisitPurposes {
    const formatedVisitPurposes = visitPurposeList.map(
      (visitPurpose: VisitPurpose) => ({
        id: visitPurpose.id,
        name: visitPurpose.name,
      })
    );

    return formatedVisitPurposes;
  }

  public async getServiceList(
    visitPurposeId: number
  ): Promise<VisitPurposes | null> {
    if (visitPurposeId != 0) {
      const response = await apiClient.post(routes.api.servicesList, {
        visitPurposeId: visitPurposeId,
      });
      return this.toServiceFrontFormat(response.data);
    }

    return null;
  }

  private toServiceFrontFormat(serviceList: []): Services {
    const formatedServices = serviceList.map((service: Service) => ({
      id: service.id,
      name: service.name,
    }));

    return formatedServices;
  }

  public async getCustomersAmountUserServed(): Promise<string> {
    const response = await apiClient.get(
      routes.api.getCustomersAmountUserServed,
      {
        params: {
          userId: userId(),
        },
      }
    );
    return response.data.customersAmountUserServed;
  }

  public async getUserAverageServiceTime(): Promise<string | null> {
    if (visitPurposesIds().length != 0) {
      const response = await apiClient.get(
        routes.api.getUserAverageServiceTime,
        {
          params: {
            userId: userId(),
            visitPurposesIds: visitPurposesIds(),
          },
        }
      );
      return response.data.userAverageServiceTime;
    }

    return null;
  }

  public async getUsersAverageServiceTime(): Promise<string | null> {
    const response = await apiClient.get(
      routes.api.getUsersAverageServiceTime,
      {
        params: {
          branchOfficeId: branchOfficeId(),
        },
      }
    );
    return response.data.usersAverageServiceTime;
  }

  public async getUsersMaxServiceTime(): Promise<string | null> {
    const response = await apiClient.get(routes.api.getUsersMaxServiceTime, {
      params: {
        branchOfficeId: branchOfficeId(),
      },
    });

    return response.data.usersMaxServiceTime;
  }

  public async getUserStatusesList(): Promise<UserStatuses> {
    const response = await apiClient.get(routes.api.getUserStatusesList);
    return this.toUserStatusFrontFormat(response.data);
  }

  private toUserStatusFrontFormat(userStatusesList: []): UserStatuses {
    const formattedVisitPurposes = userStatusesList.map(
      (userStatus: UserStatus) => ({
        id: userStatus.id,
        name: userStatus.name,
      })
    );

    return formattedVisitPurposes;
  }

  public async updateUserStatus(statusId: number): Promise<string> {
    const response = await apiClient.post(routes.api.updateUserStatus, {
      userId: userId(),
      statusId: statusId,
    });
    const message = response.data.message;

    if (message == "STATUS_IS_UPDATED") {
      localStorage.setItem(
        "queue-user-data",
        JSON.stringify(response.data.userData)
      );
    }

    return message;
  }
}

export default QueueServicePageAdapter;
