import CreateTicketPagesAdapter from "./CreateTicketPagesAdapter";

class CreateTicketPagesService {
  private serviceAdapter: CreateTicketPagesAdapter;

  constructor() {
    this.serviceAdapter = new CreateTicketPagesAdapter();
  }

  public createTicket(): Promise<string> {
    return this.serviceAdapter.createTicket();
  }
}

export default CreateTicketPagesService;
