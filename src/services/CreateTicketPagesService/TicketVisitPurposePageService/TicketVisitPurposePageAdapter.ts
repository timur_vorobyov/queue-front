import { routes } from "src/config/routes";
import { VisitPurpose, VisitPurposes } from "./TicketVisitPurposePageService";
import apiClient from "src/helpers/httpInterceptor";

class TicketVisitPurposePageServiceAdapter {
  public async getVisitPurposeList(
    branchOfficeId: string
  ): Promise<VisitPurposes> {
    const response = await apiClient.get(
      routes.api.visitPurposesList + encodeURIComponent(branchOfficeId)
    );
    return this.toVisitPurposeArray(response.data);
  }

  private toVisitPurposeArray(visitPurposeList: []): VisitPurposes {
    const formatedVisitPurposes = visitPurposeList.map(
      (visitPurpose: VisitPurpose) => ({
        id: visitPurpose.id,
        name: visitPurpose.name,
      })
    );

    return formatedVisitPurposes;
  }
}

export default TicketVisitPurposePageServiceAdapter;
