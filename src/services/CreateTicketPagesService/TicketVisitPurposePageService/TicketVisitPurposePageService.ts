import TicketVisitPurposePageAdapter from "./TicketVisitPurposePageAdapter";

export type VisitPurposes = { id: number; name: string }[];
export type VisitPurpose = { id: number; name: string };

class TicketVisitPurposePageService {
  private serviceAdapter: TicketVisitPurposePageAdapter;

  constructor() {
    this.serviceAdapter = new TicketVisitPurposePageAdapter();
  }

  public getVisitPurposeList(branchOfficeId: string): Promise<VisitPurposes> {
    return this.serviceAdapter.getVisitPurposeList(branchOfficeId);
  }
}

export default TicketVisitPurposePageService;
