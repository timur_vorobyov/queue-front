import { routes } from "src/config/routes";
import { branchOfficeId } from "src/helpers/auth-data";
import apiClient from "src/helpers/httpInterceptor";
import store from "src/redux/CreateTicket";

class CreateTicketPagesAdapter {
  public async createTicket(): Promise<string> {
    const ticketAtrributes = store.getState();
    const response = await apiClient.post(routes.api.createTicket, {
      visitPurposeId: ticketAtrributes.visitPurposeId,
      customerPhoneNumber: ticketAtrributes.customerPhoneNumber,
      branchOfficeId: branchOfficeId(),
    });

    return this.toTicketFrontFormat(response.data.ticket);
  }

  private toTicketFrontFormat(ticket: { ticketFullNumber: string }): string {
    return ticket.ticketFullNumber;
  }
}

export default CreateTicketPagesAdapter;
