import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import { PaginationData } from "src/components/Pagination/Pagination";

export type VisitPurposes = {
  id: number;
  name: string;
}[];
export type VisitPurpose = {
  id: number;
  name: string;
};
export type Services = {
  id: number;
  name: string;
  isActive: boolean;
}[];
export type Service = {
  id: number;
  name: string;
  isActive: boolean;
};

class ServicePageAdapter {
  private pagination: PaginationData;

  constructor() {
    this.pagination = { currentPage: 0, totalPages: 0, totalRecords: 0 };
  }

  public async storeService(
    serviceName: string,
    isActive: boolean
  ): Promise<string> {
    const response = await apiClient.post(routes.api.superAdmin.storeService, {
      name: serviceName,
      isActive: isActive,
    });

    if (response.data.statusCode) {
      return response.data.statusCode;
    }

    return response.data.message;
  }

  public async updateService(
    serviceId: string | number,
    serviceName: string,
    isActive: boolean
  ): Promise<string> {
    const response = await apiClient.put(
      routes.api.superAdmin.updateService + encodeURIComponent(serviceId),
      {
        name: serviceName,
        isActive: isActive,
      }
    );
    return response.data.message;
  }

  public async deleteService(serviceId: string | number): Promise<string> {
    const response = await apiClient.delete(
      routes.api.superAdmin.deleteService + encodeURIComponent(serviceId)
    );
    return response.data.message;
  }

  public async getServicesList(
    visitPurposeId: string | number,
    searchExpression: string,
    limit: number,
    nextPage: number
  ): Promise<Services> {
    const response = await apiClient.get(
      routes.api.superAdmin.getServicesList +
        "?visitPurposeId=" +
        visitPurposeId +
        "&searchExpression=" +
        searchExpression +
        "&limit=" +
        limit +
        "&page=" +
        nextPage
    );
    this.pagination = response.data.pagination;
    return this.toServiceFrontFormat(response.data.servicesPureList);
  }

  private toServiceFrontFormat(servicesList: []): Services {
    const formatedServices = servicesList.map((service: Service) => ({
      id: service.id,
      name: service.name,
      isActive: service.isActive,
    }));

    return formatedServices;
  }

  public async getVisitPurposesList(): Promise<VisitPurposes> {
    const response = await apiClient.get(routes.api.getVisitPurposesList);
    return this.toVisitPurposFrontFormat(response.data);
  }

  private toVisitPurposFrontFormat(visitPurposesList: []): VisitPurposes {
    const formattedBranchOffices = visitPurposesList.map(
      (visitPurpose: VisitPurpose) => ({
        id: visitPurpose.id,
        name: visitPurpose.name,
      })
    );

    return formattedBranchOffices;
  }

  public getPaginationData(): PaginationData {
    return this.pagination;
  }
}

export default ServicePageAdapter;
