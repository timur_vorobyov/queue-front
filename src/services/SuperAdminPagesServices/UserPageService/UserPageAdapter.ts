import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import { PaginationData } from "src/components/Pagination/Pagination";

export type Users = {
  id: number;
  branchOfficeName: string;
  branchOfficeId: string;
  visitPurposesIds: number[];
  email: string;
  deskNumber: string;
  isActive: boolean;
  isAllowedHaveSameDeskNumber: boolean;
  fullName: string;
  firstName: string;
  lastName: string;
  createdAt: string;
  role: { name: string; id: number };
}[];
export type User = {
  id: number;
  branchOfficeName: string;
  branchOfficeId: string;
  visitPurposesIds: number[];
  email: string;
  deskNumber: string;
  isActive: boolean;
  isAllowedHaveSameDeskNumber: boolean;
  fullName: string;
  firstName: string;
  lastName: string;
  role: { name: string; id: number };
  createdAt: string;
};
export type BranchOffices = {
  id: number;
  branchOfficeName: string;
  townId: number;
  initial: string;
  townName: string;
}[];
export type BranchOffice = {
  id: number;
  branchOfficeName: string;
  townId: number;
  initial: string;
  townName: string;
};
export type VisitPurposes = {
  id: number;
  name: string;
}[];
export type VisitPurpose = {
  id: number;
  name: string;
};
export type Roles = { id: number; name: string }[];
export type Role = { id: number; name: string };
export type Towns = { id: number; name: string }[];
export type Town = { id: number; name: string };

class UserPageAdapter {
  private pagination: PaginationData;

  constructor() {
    this.pagination = { currentPage: 0, totalPages: 0, totalRecords: 0 };
  }

  public async storeUser(data: {
    branchOfficeId: string;
    visitPurposesIds: number[];
    firstName: string;
    lastName: string;
    password: string;
    email: string;
    isActive: boolean;
    isAllowedHaveSameDeskNumber: boolean;
    roleId: string;
  }): Promise<string> {
    const response = await apiClient.post(routes.api.superAdmin.storeUser, {
      branchOfficeId: data.branchOfficeId,
      visitPurposesIds: data.visitPurposesIds,
      firstName: data.firstName,
      lastName: data.lastName,
      password: data.password,
      email: data.email,
      isActive: data.isActive,
      isAllowedHaveSameDeskNumber: data.isAllowedHaveSameDeskNumber,
      roleId: data.roleId,
    });

    if (response.data.message == "EMAIL_ERROR") {
      return response.data.message;
    }

    if (response.data.statusCode) {
      return response.data.statusCode;
    }

    return response.data.message;
  }

  public async updateUser(
    userId: string | number,
    data: {
      branchOfficeId: string;
      visitPurposesIds: number[];
      firstName: string;
      lastName: string;
      password: string;
      email: string;
      isActive: boolean;
      isAllowedHaveSameDeskNumber: boolean;
      roleId: string;
    }
  ): Promise<string> {
    const response = await apiClient.put(
      routes.api.superAdmin.updateUser + encodeURIComponent(userId),
      {
        branchOfficeId: data.branchOfficeId,
        visitPurposesIds: data.visitPurposesIds,
        firstName: data.firstName,
        lastName: data.lastName,
        password: data.password,
        email: data.email,
        isActive: data.isActive,
        isAllowedHaveSameDeskNumber: data.isAllowedHaveSameDeskNumber,
        roleId: data.roleId,
      }
    );

    if (response.data.message == "EMAIL_ERROR") {
      return response.data.message;
    }

    if (response.data.statusCode) {
      return response.data.statusCode;
    }

    return response.data.message;
  }

  public async getPaginatedUsersList(
    branchOfficeId: string,
    searchExpression: string,
    limit: number,
    nextPage: number
  ): Promise<Users> {
    const response = await apiClient.get(
      routes.api.superAdmin.getPaginatedUsersList +
        "?branchOfficeId=" +
        branchOfficeId +
        "&searchExpression=" +
        searchExpression +
        "&limit=" +
        limit +
        "&page=" +
        nextPage
    );

    this.pagination = response.data.pagination;
    return this.toUserFrontFormat(response.data.usersPureList);
  }

  private toUserFrontFormat(usersList: []): Users {
    const formattedUsers = usersList.map((user: User) => ({
      id: user.id,
      branchOfficeId: user.branchOfficeId,
      branchOfficeName: user.branchOfficeName,
      visitPurposesIds: user.visitPurposesIds,
      email: user.email,
      deskNumber: user.deskNumber,
      createdAt: user.createdAt,
      fullName: user.fullName,
      isActive: user.isActive,
      firstName: user.firstName,
      lastName: user.lastName,
      role: {
        id: user.role ? user.role.id : 0,
        name: user.role ? user.role.name : "",
      },
      isAllowedHaveSameDeskNumber: user.isAllowedHaveSameDeskNumber,
    }));

    return formattedUsers;
  }

  public getPaginationData(): PaginationData {
    return this.pagination;
  }

  public async getTownsList(): Promise<Towns> {
    const response = await apiClient.get(routes.api.superAdmin.getTownsList);
    return this.toTownFrontFormat(response.data);
  }

  private toTownFrontFormat(townsList: []): Towns {
    const formattedTowns = townsList.map((town: Town) => ({
      id: town.id,
      name: town.name,
    }));

    return formattedTowns;
  }

  public async getBranchOfficesList(): Promise<BranchOffices> {
    const response = await apiClient.get(
      routes.api.superAdmin.getBranchOfficesList
    );
    return this.toBranchOfficeFrontFormat(response.data);
  }

  public async getVisitPurposesListFilteredByTownId(
    townId: string
  ): Promise<BranchOffices> {
    const response = await apiClient.get(
      routes.api.superAdmin.getBranchOfficeListFilteredByTownId +
        encodeURIComponent(townId)
    );
    return this.toBranchOfficeFrontFormat(response.data);
  }

  private toBranchOfficeFrontFormat(branchOfficesList: []): BranchOffices {
    const formattedBranchOffices = branchOfficesList.map(
      (bracnhOffice: BranchOffice) => ({
        id: bracnhOffice.id,
        branchOfficeName: bracnhOffice.branchOfficeName,
        townId: bracnhOffice.townId,
        initial: bracnhOffice.initial,
        townName: bracnhOffice.townName,
      })
    );

    return formattedBranchOffices;
  }

  public async deleteUser(userId: number | string): Promise<string> {
    const response = await apiClient.delete(
      routes.api.superAdmin.deleteUser + encodeURIComponent(userId)
    );
    return response.data.message;
  }

  public async getVisitPurposesList(
    branchOfficeId: string
  ): Promise<VisitPurposes> {
    const response = await apiClient.get(
      routes.api.superAdmin.getUsersVisitPurposesList +
        encodeURIComponent(branchOfficeId)
    );
    return this.toVisitPurposeFrontFormat(response.data);
  }

  private toVisitPurposeFrontFormat(visitPurposesList: []): VisitPurposes {
    const formattedVisitPurposes = visitPurposesList.map(
      (visitPurpose: VisitPurpose) => ({
        id: visitPurpose.id,
        name: visitPurpose.name,
      })
    );

    return formattedVisitPurposes;
  }

  public async getRolesList(): Promise<Roles> {
    const response = await apiClient.get(routes.api.superAdmin.getRolesList);
    return this.toRoleFrontFormat(response.data);
  }

  private toRoleFrontFormat(rolesList: []): Roles {
    const formattedRoles = rolesList.map((role: Role) => ({
      id: role.id,
      name: role.name,
    }));

    return formattedRoles;
  }
}

export default UserPageAdapter;
