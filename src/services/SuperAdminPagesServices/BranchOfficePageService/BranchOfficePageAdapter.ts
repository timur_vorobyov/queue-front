import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";

export type Towns = { id: number; name: string }[];
export type Town = { id: number; name: string };
export type BranchOffices = {
  id: number;
  branchOfficeName: string;
  townId: number;
  initial: string;
  townName: string;
  timer: number;
  visitPurposes: { name: string }[];
}[];
export type BranchOffice = {
  id: number;
  branchOfficeName: string;
  townId: number;
  initial: string;
  townName: string;
  timer: number;
  visitPurposes: { name: string }[];
};
export type VisitPurposes = {
  id: number;
  name: string;
}[];
export type VisitPurpose = {
  id: number;
  name: string;
};
export type CrmBranches = {
  crmBranchLabel: string;
  crmBranchName: string;
}[];
export type CrmBranch = {
  crmBranchLabel: string;
  crmBranchName: string;
};
export type CrmBranchBackendFromat = {
  name: string;
  display_name: string;
};

class BranchOfficePageAdapter {
  public async storeTown(townName: string): Promise<string> {
    const response = await apiClient.post(routes.api.superAdmin.storeTown, {
      name: townName,
    });

    if (response.data.statusCode) {
      return response.data.statusCode;
    }

    return response.data.message;
  }

  public async updateTown(
    townId: string | number,
    townName: string
  ): Promise<string> {
    const response = await apiClient.put(
      routes.api.superAdmin.updateTown + encodeURIComponent(townId),
      {
        name: townName,
      }
    );

    return response.data.message;
  }

  public async deleteTown(townId: number | string): Promise<string> {
    const response = await apiClient.delete(
      routes.api.superAdmin.deleteTown + encodeURIComponent(townId)
    );
    return response.data.message;
  }

  public async getTownsList(): Promise<Towns> {
    const response = await apiClient.get(routes.api.superAdmin.getTownsList);
    return this.toTownFrontFormat(response.data);
  }

  private toTownFrontFormat(townsList: []): Towns {
    const formatedTowns = townsList.map((town: Town) => ({
      id: town.id,
      name: town.name,
    }));

    return formatedTowns;
  }

  public async storeBranchOffice(
    townId: number | string,
    branchOfficeName: string,
    initial: string,
    timer: number,
    visitPurposesIds: number[],
    crmBranchName: string,
    displayName: string,
    id: number
  ): Promise<string> {
    const response = await apiClient.post(
      routes.api.superAdmin.storeBranchOffice,
      {
        townId: townId,
        name: branchOfficeName,
        initial: initial,
        timer: timer,
        visitPurposesIds: visitPurposesIds,
        crmBranchName: crmBranchName,
        displayName: displayName,
        id: id,
      }
    );

    if (response.data.message == "INITIAL_ERROR") {
      return response.data.message;
    }

    if (response.data.statusCode) {
      return response.data.statusCode;
    }

    return response.data.message;
  }

  public async updateBranchOffice(
    townId: number | string,
    branchOfficeId: number,
    branchOfficeName: string,
    initial: string,
    timer: number,
    visitPurposesIds: number[]
  ): Promise<string> {
    const response = await apiClient.put(
      routes.api.superAdmin.updateBranchOffice +
        encodeURIComponent(branchOfficeId),
      {
        townId: townId,
        name: branchOfficeName,
        initial: initial,
        timer: timer,
        visitPurposesIds: visitPurposesIds,
      }
    );
    return response.data.message;
  }

  public async deleteBranchOffice(
    branchOfficeId: number | string
  ): Promise<string> {
    const response = await apiClient.delete(
      routes.api.superAdmin.deleteBranchOffice +
        encodeURIComponent(branchOfficeId)
    );
    return response.data.message;
  }

  public async getBranchOfficesList(): Promise<BranchOffices> {
    const response = await apiClient.get(
      routes.api.superAdmin.getBranchOfficesList
    );
    return this.toBranchOfficeFrontFormat(response.data);
  }

  private toBranchOfficeFrontFormat(branchOfficesList: []): BranchOffices {
    const formattedBranchOffices = branchOfficesList.map(
      (bracnhOffice: BranchOffice) => ({
        id: bracnhOffice.id,
        branchOfficeName: bracnhOffice.branchOfficeName,
        townId: bracnhOffice.townId,
        initial: bracnhOffice.initial,
        townName: bracnhOffice.townName,
        timer: bracnhOffice.timer,
        visitPurposes: bracnhOffice.visitPurposes,
      })
    );

    return formattedBranchOffices;
  }

  public async getVisitPurposesList(): Promise<VisitPurposes> {
    const response = await apiClient.get(routes.api.getVisitPurposesList);
    return this.toServiceFrontFormat(response.data);
  }

  private toServiceFrontFormat(servicesList: []): VisitPurposes {
    const formattedServices = servicesList.map((service: VisitPurpose) => ({
      id: service.id,
      name: service.name,
    }));

    return formattedServices;
  }

  public async assosiateBranches(data: {
    crmBranchName: string | null;
    displayName: string | null;
    branchOfficeId: number;
  }): Promise<boolean> {
    const response = await apiClient.post(routes.api.crm.associateBranches, {
      crmBranchName: data.crmBranchName,
      displayName: data.displayName,
      id: data.branchOfficeId,
    });

    if (response.data.message == "BRANCHES_ARE_ASSOCIATED") {
      return true;
    }

    return false;
  }

  public async getCrmBranches(): Promise<CrmBranches> {
    const response = await apiClient.post(routes.api.crm.branches);
    return this.toCrmBranchesFrontFormat(response.data);
  }

  private toCrmBranchesFrontFormat(crmBranchesList: []): CrmBranches {
    const formattedCrmBranches = crmBranchesList.map(
      (crmBranch: CrmBranchBackendFromat) => ({
        crmBranchLabel: crmBranch.display_name,
        crmBranchName: crmBranch.name,
      })
    );

    return formattedCrmBranches;
  }
}

export default BranchOfficePageAdapter;
