import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";

export type Roles = { id: number; name: string; permissions: Permissions }[];
export type Role = { id: number; name: string; permissions: Permissions };
export type Permissions = { id: number; name: string }[];
export type Permission = { id: number; name: string };

class RolePageAdapter {
  public async storeRole(
    roleName: string,
    permissionIds: number[]
  ): Promise<string> {
    const response = await apiClient.post(routes.api.superAdmin.storeRole, {
      name: roleName,
      permissionIds: permissionIds,
    });

    if (response.data.statusCode) {
      return response.data.statusCode;
    }

    return response.data.message;
  }

  public async updateRole(
    roleId: string | number,
    roleName: string,
    permissionIds: number[]
  ): Promise<string> {
    const response = await apiClient.put(
      routes.api.superAdmin.updateRole + encodeURIComponent(roleId),
      {
        name: roleName,
        permissionIds: permissionIds,
      }
    );

    return response.data.message;
  }

  public async deleteRole(roleId: number | string): Promise<string> {
    const response = await apiClient.delete(
      routes.api.superAdmin.deleteRole + encodeURIComponent(roleId)
    );
    return response.data.message;
  }

  public async getRolesList(): Promise<Roles> {
    const response = await apiClient.get(routes.api.superAdmin.getRolesList);
    return this.toRoleFrontFormat(response.data);
  }

  private toRoleFrontFormat(rolesList: []): Roles {
    const formatedRoles = rolesList.map((role: Role) => ({
      id: role.id,
      name: role.name,
      permissions: role.permissions,
    }));

    return formatedRoles;
  }

  public async getPermissionsList(): Promise<Permissions> {
    const response = await apiClient.get(
      routes.api.superAdmin.getPermissionsList
    );
    return this.toPermissionFrontFormat(response.data);
  }

  private toPermissionFrontFormat(permissionsList: []): Permissions {
    const formatedPermissions = permissionsList.map(
      (permission: Permission) => ({
        id: permission.id,
        name: permission.name,
      })
    );

    return formatedPermissions;
  }
}

export default RolePageAdapter;
