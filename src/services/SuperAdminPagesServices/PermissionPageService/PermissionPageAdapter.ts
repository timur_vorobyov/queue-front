import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";

export type Permissions = { id: number; name: string }[];
export type Permission = { id: number; name: string };

class PermissionPageAdapter {
  public async storePermission(permissionName: string): Promise<string> {
    const response = await apiClient.post(
      routes.api.superAdmin.storePermission,
      {
        name: permissionName,
      }
    );

    if (response.data.statusCode) {
      return response.data.statusCode;
    }

    return response.data.message;
  }

  public async updatePermission(
    permissionId: string | number,
    permissionName: string
  ): Promise<string> {
    const response = await apiClient.put(
      routes.api.superAdmin.updatePermission + encodeURIComponent(permissionId),
      {
        name: permissionName,
      }
    );
    return response.data.message;
  }

  public async deletePermission(
    permissionId: number | string
  ): Promise<string> {
    const response = await apiClient.delete(
      routes.api.superAdmin.deletePermission + encodeURIComponent(permissionId)
    );
    return response.data.message;
  }

  public async getPermissionsList(): Promise<Permissions> {
    const response = await apiClient.get(
      routes.api.superAdmin.getPermissionsList
    );
    return this.toPermissionFrontFormat(response.data);
  }

  private toPermissionFrontFormat(permissionsList: []): Permissions {
    const formatedPermissions = permissionsList.map(
      (permission: Permission) => ({
        id: permission.id,
        name: permission.name,
      })
    );

    return formatedPermissions;
  }
}

export default PermissionPageAdapter;
