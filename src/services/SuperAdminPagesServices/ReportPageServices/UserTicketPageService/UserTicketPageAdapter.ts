import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import { PaginationData } from "src/components/Pagination/Pagination";
import { format } from "date-fns";

export type Users = { id: number; firstName: string }[];
export type User = { id: number; firstName: string };
export type BranchOffices = { id: number; branchOfficeName: string }[];
export type BranchOffice = { id: number; branchOfficeName: string };
export type Statuses = {
  id: number;
  name: string;
}[];
export type Status = {
  id: number;
  name: string;
};
export type UserActionTimes = {
  id: number;
  userFullName: string;
  lunchTime: string;
  workTime: string;
  breakTime: string;
  offlineTime: string;
}[];
export type UserActionTime = {
  id: number;
  userFullName: string;
  lunchTime: string;
  workTime: string;
  breakTime: string;
  offlineTime: string;
};

class UserReportPageAdapter {
  private pagination: PaginationData;

  constructor() {
    this.pagination = { currentPage: 0, totalPages: 0, totalRecords: 0 };
  }

  public async getUserActionTimesList(
    branchOfficeId: string,
    statusId: string,
    userId: string,
    searchExpression: string,
    limit: number,
    dateFrom: Date,
    dateTo: Date,
    nextPage: number
  ): Promise<UserActionTimes> {
    const response = await apiClient.get(
      routes.api.superAdmin.report.getPaginatedUserActionTimesList +
        "?branchOfficeId=" +
        branchOfficeId +
        "&statusId=" +
        statusId +
        "&userId=" +
        userId +
        "&searchExpression=" +
        searchExpression +
        "&limit=" +
        limit +
        "&page=" +
        nextPage +
        "&dateFrom=" +
        format(Number(dateFrom), "yyyy-MM-dd") +
        "&dateTo=" +
        format(Number(dateTo), "yyyy-MM-dd")
    );
    this.pagination = response.data.pagination;
    return this.toUserActionTimeFrontFormat(response.data.userActionTimesList);
  }

  private toUserActionTimeFrontFormat(
    userActionTimesList: []
  ): UserActionTimes {
    const formattedUserActionTimesList = userActionTimesList.map(
      (userActionTime: UserActionTime) => ({
        id: userActionTime.id,
        userFullName: userActionTime.userFullName,
        lunchTime: userActionTime.lunchTime,
        workTime: userActionTime.workTime,
        breakTime: userActionTime.breakTime,
        offlineTime: userActionTime.offlineTime,
      })
    );

    return formattedUserActionTimesList;
  }

  public async getBranchOfficesList(): Promise<BranchOffices> {
    const response = await apiClient.get(
      routes.api.superAdmin.getBranchOfficesList
    );
    return this.toBranchOfficeFrontFormat(response.data);
  }

  private toBranchOfficeFrontFormat(branchOfficesList: []): BranchOffices {
    const formattedBranchOffices = branchOfficesList.map(
      (bracnhOffice: BranchOffice) => ({
        id: bracnhOffice.id,
        branchOfficeName: bracnhOffice.branchOfficeName,
      })
    );

    return formattedBranchOffices;
  }

  public async getStatusesList(): Promise<Statuses> {
    const response = await apiClient.get(routes.api.getUserStatusesList);
    return this.toStatusFrontFormat(response.data);
  }

  private toStatusFrontFormat(statusesList: []): Statuses {
    const formattedStatuses = statusesList.map((status: Status) => ({
      id: status.id,
      name: status.name,
    }));

    return formattedStatuses;
  }

  public async getUsersList(): Promise<Users> {
    const response = await apiClient.get(routes.api.getUsersList);
    return this.toUserFrontFormat(response.data);
  }

  private toUserFrontFormat(usersList: []): Users {
    const formattedUsers = usersList.map((user: User) => ({
      id: user.id,
      firstName: user.firstName,
    }));

    return formattedUsers;
  }

  public getPaginationData(): PaginationData {
    return this.pagination;
  }

  public async export(
    branchOfficeId: string,
    statusId: string,
    userId: string,
    searchExpression: string,
    dateFrom: Date,
    dateTo: Date
  ): Promise<void> {
    window.open(
      process.env.BACKEND_URL +
        routes.api.superAdmin.usersExport +
        "?branchOfficeId=" +
        branchOfficeId +
        "&statusId=" +
        statusId +
        "&userId=" +
        userId +
        "&searchExpression=" +
        searchExpression +
        "&dateFrom=" +
        format(Number(dateFrom), "yyyy-MM-dd") +
        "&dateTo=" +
        format(Number(dateTo), "yyyy-MM-dd"),
      "_blank"
    );
  }
}

export default UserReportPageAdapter;
