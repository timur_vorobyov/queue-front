import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import { PaginationData } from "src/components/Pagination/Pagination";
import { format } from "date-fns";

export type Users = { id: number; firstName: string }[];
export type User = { id: number; firstName: string };
export type BranchOffices = { id: number; branchOfficeName: string }[];
export type BranchOffice = { id: number; branchOfficeName: string };
export type VisitPurposes = {
  id: number;
  name: string;
}[];
export type VisitPurpose = {
  id: number;
  name: string;
};
export type Tickets = {
  id: number;
  ticketFullNumber: string;
  visitPurposeName: string;
  serviceName: string;
  customerPhoneNumber: string;
  crmUserId: number;
  date: string;
  time: string;
  customerPendingTime: string;
  customerServiceTime: string;
  userFirstName: string;
  branchOfficeName: string;
}[];
export type Ticket = {
  id: number;
  ticketFullNumber: string;
  visitPurposeName: string;
  serviceName: string;
  customerPhoneNumber: string;
  crmUserId: number;
  date: string;
  time: string;
  customerPendingTime: string;
  customerServiceTime: string;
  userFirstName: string;
  branchOfficeName: string;
};

class TicketReportPageAdapter {
  private pagination: PaginationData;

  constructor() {
    this.pagination = { currentPage: 0, totalPages: 0, totalRecords: 0 };
  }

  public async getTicketsList(
    branchOfficeId: string,
    visitPurposeId: string,
    userId: string,
    searchExpression: string,
    limit: number,
    dateFrom: Date,
    dateTo: Date,
    nextPage: number
  ): Promise<Tickets | null> {
    const response = await apiClient.get(
      routes.api.superAdmin.report.getPaginatedTicketsList +
        "?branchOfficeId=" +
        branchOfficeId +
        "&visitPurposeId=" +
        visitPurposeId +
        "&userId=" +
        userId +
        "&searchExpression=" +
        searchExpression +
        "&limit=" +
        limit +
        "&page=" +
        nextPage +
        "&dateFrom=" +
        format(Number(dateFrom), "yyyy-MM-dd") +
        "&dateTo=" +
        format(Number(dateTo), "yyyy-MM-dd")
    );
    this.pagination = response.data.pagination;
    if (response.data.tickets) {
      return this.toTicektFrontFormat(response.data.tickets);
    }

    return null;
  }

  private toTicektFrontFormat(ticketsList: []): Tickets {
    const formattedUsers = ticketsList.map((ticket: Ticket) => ({
      id: ticket.id,
      ticketFullNumber: ticket.ticketFullNumber,
      visitPurposeName: ticket.visitPurposeName,
      serviceName: ticket.serviceName,
      customerPhoneNumber: ticket.customerPhoneNumber,
      crmUserId: ticket.crmUserId,
      date: ticket.date,
      time: ticket.time,
      customerPendingTime: ticket.customerPendingTime,
      customerServiceTime: ticket.customerServiceTime,
      userFirstName: ticket.userFirstName,
      branchOfficeName: ticket.branchOfficeName,
    }));

    return formattedUsers;
  }

  public async getBranchOfficesList(): Promise<BranchOffices> {
    const response = await apiClient.get(
      routes.api.superAdmin.getBranchOfficesList
    );
    return this.toBranchOfficeFrontFormat(response.data);
  }

  private toBranchOfficeFrontFormat(branchOfficesList: []): BranchOffices {
    const formattedBranchOffices = branchOfficesList.map(
      (bracnhOffice: BranchOffice) => ({
        id: bracnhOffice.id,
        branchOfficeName: bracnhOffice.branchOfficeName,
      })
    );

    return formattedBranchOffices;
  }

  public async getVisitPurposesList(): Promise<VisitPurposes> {
    const response = await apiClient.get(routes.api.getVisitPurposesList);
    return this.toVisitPurposeFrontFormat(response.data);
  }

  private toVisitPurposeFrontFormat(visitPurposesList: []): VisitPurposes {
    const formattedVisitPurposes = visitPurposesList.map(
      (visitPurpose: VisitPurpose) => ({
        id: visitPurpose.id,
        name: visitPurpose.name,
      })
    );

    return formattedVisitPurposes;
  }

  public async getUsersList(): Promise<Users> {
    const response = await apiClient.get(routes.api.getUsersList);
    return this.toUserFrontFormat(response.data);
  }

  private toUserFrontFormat(usersList: []): Users {
    const formattedUsers = usersList.map((user: User) => ({
      id: user.id,
      firstName: user.firstName,
    }));

    return formattedUsers;
  }

  public getPaginationData(): PaginationData {
    return this.pagination;
  }

  public async export(
    branchOfficeId: string,
    visitPurposeId: string,
    userId: string,
    searchExpression: string,
    dateFrom: Date,
    dateTo: Date
  ): Promise<void> {
    window.open(
      process.env.BACKEND_URL +
        routes.api.superAdmin.ticketsExport +
        "?branchOfficeId=" +
        branchOfficeId +
        "&visitPurposeId=" +
        visitPurposeId +
        "&userId=" +
        userId +
        "&searchExpression=" +
        searchExpression +
        "&dateFrom=" +
        format(Number(dateFrom), "yyyy-MM-dd") +
        "&dateTo=" +
        format(Number(dateTo), "yyyy-MM-dd"),
      "_blank"
    );
  }
}

export default TicketReportPageAdapter;
