import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import { PaginationData } from "src/components/Pagination/Pagination";

export type VisitPurposes = {
  id: number;
  name: string;
  series: string;
  services: { name: string }[];
  tjMessage: string;
  ruMessage: string;
}[];
export type VisitPurpose = {
  id: number;
  name: string;
  series: string;
  services: { name: string }[];
  tjMessage: string;
  ruMessage: string;
};
export type BranchOffices = {
  id: number;
  branchOfficeName: string;
}[];
export type BranchOffice = {
  id: number;
  branchOfficeName: string;
};
export type Services = {
  id: number;
  name: string;
  isActive: boolean;
}[];
export type Service = {
  id: number;
  name: string;
  isActive: boolean;
};

class VisitPurposePageAdapter {
  private pagination: PaginationData;

  constructor() {
    this.pagination = { currentPage: 0, totalPages: 0, totalRecords: 0 };
  }

  public async storeVisitPurpose(
    visitPurposeName: string,
    servicesIds: number[],
    series: string,
    tjMessage: string,
    ruMessage: string
  ): Promise<string> {
    const response = await apiClient.post(
      routes.api.superAdmin.storeVisitPurpose,
      {
        name: visitPurposeName,
        servicesIds: servicesIds,
        series: series,
        tjMessage: tjMessage,
        ruMessage: ruMessage,
      }
    );

    if (response.data.message == "SERIES_ERROR") {
      return response.data.message;
    }

    if (response.data.statusCode) {
      return response.data.statusCode;
    }

    return response.data.message;
  }

  public async updateVisitPurpose(
    visitPurposeId: string | number,
    visitPurposeName: string,
    servicesIds: number[],
    series: string,
    tjMessage: string,
    ruMessage: string
  ): Promise<string> {
    const response = await apiClient.put(
      routes.api.superAdmin.updateVisitPurpose +
        encodeURIComponent(visitPurposeId),
      {
        name: visitPurposeName,
        servicesIds: servicesIds,
        series: series,
        tjMessage: tjMessage,
        ruMessage: ruMessage,
      }
    );
    return response.data.message;
  }

  public async deleteVisitPurpose(
    visitPurposeId: number | string
  ): Promise<string> {
    const respone = await apiClient.delete(
      routes.api.superAdmin.deleteVisitPurpose +
        encodeURIComponent(visitPurposeId)
    );
    return respone.data.message;
  }

  public async getVisitPurposesList(
    branchOfficeId: string,
    searchExpression: string,
    limit: number,
    nextPage: number
  ): Promise<VisitPurposes> {
    const response = await apiClient.get(
      routes.api.superAdmin.getPaginatedVisitPurposesList +
        "?branchOfficeId=" +
        branchOfficeId +
        "&searchExpression=" +
        searchExpression +
        "&limit=" +
        limit +
        "&page=" +
        nextPage
    );
    this.pagination = response.data.pagination;
    return this.tovisitPurposeFrontFormat(response.data.visitPurposesPureList);
  }

  private tovisitPurposeFrontFormat(visitPurposesList: []): VisitPurposes {
    const formatedVisitPurposes = visitPurposesList.map(
      (visitPurpose: VisitPurpose) => ({
        id: visitPurpose.id,
        name: visitPurpose.name,
        series: visitPurpose.series,
        services: visitPurpose.services,
        tjMessage: visitPurpose.tjMessage,
        ruMessage: visitPurpose.ruMessage,
      })
    );

    return formatedVisitPurposes;
  }

  public async getBranchOfficesList(): Promise<BranchOffices> {
    const response = await apiClient.get(routes.api.getBranchOfficesList);
    return this.toBranchOfficeFrontFormat(response.data);
  }

  private toBranchOfficeFrontFormat(branchOfficesList: []): BranchOffices {
    const formattedBranchOffices = branchOfficesList.map(
      (bracnhOffice: BranchOffice) => ({
        id: bracnhOffice.id,
        branchOfficeName: bracnhOffice.branchOfficeName,
      })
    );

    return formattedBranchOffices;
  }

  public async getServicesList(): Promise<Services> {
    const response = await apiClient.get(routes.api.superAdmin.getServicesList);
    return this.toServiceFrontFormat(response.data.servicesPureList);
  }

  private toServiceFrontFormat(servicesList: []): Services {
    const formattedServices = servicesList.map((service: Service) => ({
      id: service.id,
      name: service.name,
      isActive: service.isActive,
    }));

    return formattedServices;
  }

  public getPaginationData(): PaginationData {
    return this.pagination;
  }
}

export default VisitPurposePageAdapter;
