const getUserData = () => {
  const userJsonData = localStorage.getItem("queue-user-data");
  const userData = userJsonData ? JSON.parse(userJsonData) : "";
  return userData;
};

export const userId = () => {
  const userData = getUserData();
  return userData.id;
};
export const userFullName = () => {
  const userData = getUserData();
  return userData.fullName;
};
export const branchOfficeId = () => {
  const userData = getUserData();
  return userData.branchOfficeId;
};
export const branchOfficeFullName = () => {
  const userData = getUserData();
  return userData.branchOfficeFullName;
};
export const deskNumber = () => {
  const userData = getUserData();
  return userData.deskNumber;
};
export const visitPurposesIds = () => {
  const userData = getUserData();
  return userData.visitPurposesIds;
};
export const visitPurposesNames = () => {
  const userData = getUserData();
  return userData.visitPurposesNames;
};
export const roleName = () => {
  const userData = getUserData();
  return userData.roleName;
};
export const townId = () => {
  const userData = getUserData();
  return userData.townId;
};
export const timer = () => {
  const userData = getUserData();
  return userData.timer;
};
export const accessToken = () => {
  const accessToken = localStorage.getItem("queue-access-token")
    ? localStorage.getItem("queue-access-token")
    : "";
  return accessToken;
};

export const userStatusId = () => {
  const userData = getUserData();
  return userData.userStatusId;
};

export const userStatusName = () => {
  const userData = getUserData();
  return userData.userStatusName;
};
