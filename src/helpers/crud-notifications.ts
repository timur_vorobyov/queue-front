// @ts-ignore
import { NotificationManager } from "react-notifications";

export const createNotification = (type: string, message: string) => {
  switch (type) {
    case "success":
      NotificationManager.success(message);
      break;
    case "error":
      NotificationManager.error(message);
      break;
    case "warning":
      NotificationManager.warning(message);
      break;
  }
};
