import axios from "axios";
import { accessToken } from "./auth-data";

const apiUrl = process.env.BACKEND_URL;

export default axios.create({
  baseURL: apiUrl,
  headers: {
    Authorization: "Bearer " + accessToken(),
    "Content-Type": "application/json",
    Accept: "application/json",
  },
});
