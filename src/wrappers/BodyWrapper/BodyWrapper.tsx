import React, { PureComponent } from "react";
import classes from "./BodyWrapper.css";
import cn from "clsx";
import SideBar from "src/components/SideBar";
import { Container } from "react-bootstrap";
import { Redirect, Route, Switch } from "react-router-dom";
import { routes } from "src/config/routes";
import QueueServicePage from "src/pages/UserPage/QueueServicePage";
import ProfilePage from "src/pages/UserPage/ProfilePage";
import BranchOfficePage from "src/pages/Admin/SuperAdminPages/BranchOfficePage";
import SuperAdminUserPage from "src/pages/Admin/SuperAdminPages/UserPage/UserPage";
import PermissionPage from "src/pages/Admin/SuperAdminPages/PermissionPage";
import RolePage from "src/pages/Admin/SuperAdminPages/RolePage";
import VisitPurposePage from "src/pages/Admin/SuperAdminPages/VisitPurposePage";
import ServicePage from "src/pages/Admin/SuperAdminPages/ServicePage";
import HallAdminUserPage from "src/pages/Admin/HallAdminPages/UserPage/UserPage";
import AdminTicketBasketPage from "src/pages/Admin/TicketBasketPage/TicketBasketPage";
import NavBar from "src/components/NavBar";
import TicketReportPage from "src/pages/Admin/SuperAdminPages/Reports/TicketReportPage";
import UserReportPage from "src/pages/Admin/SuperAdminPages/Reports/UserReportPage";
import { userStatusName } from "src/helpers/auth-data";

class BodyWrapper extends PureComponent<
  {},
  { isAcceptTicketButtonDisabled: boolean }
> {
  constructor(props: {}) {
    super(props);
    this.state = {
      isAcceptTicketButtonDisabled:
        userStatusName() == "work time" ? false : true,
    };
  }

  setCustomersAmountUserServed = (isButtonDisabled: boolean) => {
    this.setState({ isAcceptTicketButtonDisabled: isButtonDisabled });
  };

  render(): React.ReactNode {
    return (
      <div className={cn(classes.container)}>
        <div className={cn(classes.container)}>
          <SideBar />
          <div className={cn(classes.mainContent)}>
            <div className={cn(classes.blueSpace)}>
              <NavBar onStatusChanged={this.setCustomersAmountUserServed} />
            </div>
            <Container className={cn(classes.bootstrapContainer)}>
              <div className={cn(classes.content)}>
                <Switch>
                  <Route
                    path={routes.queueService}
                    exact
                    component={() => (
                      <QueueServicePage
                        isAcceptTicketButtonDisabled={
                          this.state.isAcceptTicketButtonDisabled
                        }
                      />
                    )}
                  />
                  <Route path={routes.profile} exact component={ProfilePage} />
                  <Route
                    path={routes.superAdmin.branchOffices}
                    exact
                    component={BranchOfficePage}
                  />
                  <Route
                    path={routes.superAdmin.users}
                    exact
                    component={SuperAdminUserPage}
                  />
                  <Route
                    path={routes.superAdmin.permissions}
                    exact
                    component={PermissionPage}
                  />
                  <Route
                    path={routes.superAdmin.roles}
                    exact
                    component={RolePage}
                  />
                  <Route
                    path={routes.superAdmin.visitPurposes}
                    exact
                    component={VisitPurposePage}
                  />
                  <Route
                    path={routes.superAdmin.services}
                    exact
                    component={ServicePage}
                  />
                  <Route
                    path={routes.superAdmin.reports.tickets}
                    exact
                    component={TicketReportPage}
                  />
                  <Route
                    path={routes.superAdmin.reports.users}
                    exact
                    component={UserReportPage}
                  />

                  <Route
                    path={routes.hallAdmin.users}
                    exact
                    component={HallAdminUserPage}
                  />
                  <Route
                    path={routes.admin.declinedTickets}
                    exact
                    component={AdminTicketBasketPage}
                  />

                  <Route path="*" exact>
                    <Redirect to={routes.notFound} />
                  </Route>
                </Switch>
              </div>
            </Container>
          </div>
        </div>
      </div>
    );
  }
}

export default BodyWrapper;
