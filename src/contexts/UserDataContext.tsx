import React from "react";
import { PureComponent } from "react";
import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import { createNotification } from "src/helpers/crud-notifications";
import { userId } from "src/helpers/auth-data";

interface UserDataContextProviderStates {
  accessToken: string;
  userData: {};
}
interface UserDataContextState {
  isAuth: boolean;
  fetchUserData: (email: string, password: string) => void;
}

export const UserDataContext = React.createContext<UserDataContextState>({
  isAuth: false,
  fetchUserData: () => {
    "";
  },
});

export class UserDataContextProvider extends PureComponent<
  {},
  UserDataContextProviderStates
> {
  constructor(props: {}) {
    super(props);
    this.state = {
      accessToken: "",
      userData: {},
    };
  }

  componentDidMount(): void {
    if (userId()) {
      this.setUserData(userId());
    }
  }

  public fetchUserData = async (
    email: string,
    password: string
  ): Promise<void> => {
    const response = await apiClient.post(routes.login, {
      email: email,
      password: password,
    });

    await this.setUserData(response.data.id);
    this.setToken(response.data.accessToken);
  };

  private setToken(accessToken: string): void {
    if (accessToken == null) {
      createNotification("error", "Неверно введён пароль или логин");
    }
    localStorage.setItem("queue-access-token", accessToken);
    this.setState({ accessToken: accessToken });
    setTimeout(() => {
      localStorage.clear();
    }, 180 * 60 * 1000);
  }

  private getToken(): string | null {
    const accessToken = localStorage.getItem("queue-access-token");
    return accessToken;
  }

  private async setUserData(id: number): Promise<void> {
    if (id != null) {
      const userData = await apiClient.get(
        routes.api.getUserData + encodeURIComponent(id)
      );

      if (userData.data == "User not found") {
        localStorage.removeItem("queue-user-data");
        localStorage.removeItem("queue-access-token");
        return window.location.replace(routes.login);
      }

      await localStorage.setItem(
        "queue-user-data",
        JSON.stringify(userData.data)
      );
      await this.setState({ userData: userData });
    }
  }

  render(): React.ReactNode {
    const { props } = this;
    const providerValue = {
      isAuth:
        this.getToken() == "null" || this.getToken() == null ? false : true,
      fetchUserData: this.fetchUserData,
    };

    return (
      <UserDataContext.Provider value={providerValue}>
        {props.children}
      </UserDataContext.Provider>
    );
  }
}
