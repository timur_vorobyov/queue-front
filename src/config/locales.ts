export const locales = ["en-GB", "ru", "tj"] as const;

export type Locale = typeof locales[number];
