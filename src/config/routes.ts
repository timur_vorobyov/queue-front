export const routes = {
  /**Person */
  queueService: "/",
  login: "/login/",
  profile: "/profile/",
  ticketVisitPurpose: "/register-ticket/visit-purpose/",
  ticketCustomerPhoneNumber: "/register-ticket/customer-phone-number/",
  showTicket: "/register-ticket/show-ticket/",
  registerTicket: "/register-ticket",
  screens: "/screens/",
  tv: "/tv",
  led: "/led",
  ledWhite: "/led-white",
  notFound: "/not-found/",

  /**Admin */
  admin: {
    declinedTickets: "/admin/declined-tickets/",
  },

  /**Hall Admin */
  hallAdmin: {
    users: "/hall-admin/users/",
  },

  /**Super Admin */
  superAdmin: {
    branchOffices: "/super-admin/branch-offices/",
    users: "/super-admin/users/",
    roles: "/super-admin/roles/",
    permissions: "/super-admin/permissions/",
    visitPurposes: "/super-admin/visit-purposes/",
    services: "/super-admin/services/",
    reports: {
      tickets: "/super-admin/reports/tickets",
      users: "/super-admin/reports/users",
    },
  },

  /**API */

  api: {
    townsList: "towns-list/",
    branchOfficesList: "branch-offices-list/",
    servicesList: "services-list/",
    visitPurposesList: "visit-purposes-list/",
    updateUserWorkData: "update-work-data/",
    deskNumberCheck: "desk-number-check/",
    createTicket: "tickets/",
    acceptTicket: "accept-ticket/",
    serveTicket: "serve-ticket/",
    declineTicket: "decline-ticket/",
    completeService: "complete-service/",
    fullTicketsList: "full-tickets-list",
    ticket: "ticket",
    shortTicketsList: "short-tickets-list",
    getCustomersAmountUserServed: "get-customers-amount-user-served/",
    getUserAverageServiceTime: "get-user-average-service-time/",
    getUsersAverageServiceTime: "get-users-average-service-time/",
    getUsersMaxServiceTime: "get-users-max-service-time/",
    getUserData: "get-user-data/",
    updateStatisticsData: "update-statistics-data/",
    updateUserStatus: "update-user-status/",

    /**Pure Models List */
    getUsersList: "/users/",
    getBranchOfficesList: "/branch-offices/",
    getVisitPurposesList: "/visit-purposes/",
    getUserStatusesList: "/user-statuses",

    /**Super Admin */
    superAdmin: {
      /**Branch Office */
      storeBranchOffice: "/super-admin/branch-offices/",
      deleteBranchOffice: "/super-admin/branch-office/",
      updateBranchOffice: "/super-admin/branch-offices/",
      getBranchOfficesList: "/super-admin/branch-offices/",

      /**Town */
      storeTown: "/super-admin/towns/",
      deleteTown: "/super-admin/town/",
      updateTown: "/super-admin/towns/",
      getTownsList: "/super-admin/towns/",

      /**Permission */
      storePermission: "/super-admin/permissions/",
      deletePermission: "/super-admin/permission/",
      updatePermission: "/super-admin/permissions/",
      getPermissionsList: "/super-admin/permissions/",

      /**Role */
      storeRole: "/super-admin/roles/",
      deleteRole: "/super-admin/role/",
      updateRole: "/super-admin/roles/",
      getRolesList: "/super-admin/roles/",

      /**User */
      storeUser: "/super-admin/users/",
      getPaginatedUsersList: "/super-admin/paginated-users/",
      updateUser: "/super-admin/users/",
      deleteUser: "/super-admin/user/",
      getUsersVisitPurposesList: "/super-admin/get-visit-purposes-list/",
      getBranchOfficeListFilteredByTownId:
        "/super-admin/get-branch-office-list/",

      /**Visit Purpose  */
      storeVisitPurpose: "/super-admin/visit-purposes/",
      getPaginatedVisitPurposesList: "/super-admin/paginated-visit-purposes",
      updateVisitPurpose: "/super-admin/visit-purposes/",
      deleteVisitPurpose: "/super-admin/visit-purpose/",

      /**Service  */
      storeService: "/super-admin/services/",
      getServicesList: "/super-admin/services/",
      updateService: "/super-admin/services/",
      deleteService: "/super-admin/service/",

      /**Report */
      report: {
        getPaginatedTicketsList: "/super-admin/reports/tickets",
        getPaginatedUserActionTimesList: "/super-admin/reports/users",
      },

      /**Ticket */
      ticketsExport: "super-admin/tickets/export",
      usersExport: "super-admin/users/export",
    },

    /**Hall Admin */
    hallAdmin: {
      /**User */
      deleteUser: "/hall-admin/user/",
      updateUser: "/hall-admin/users/",
      getPaginatedUsersList: "/hall-admin/users/",
    },

    /**Admin */
    admin: {
      /** Ticket */
      getDeclinedTicketsList: "/admin/declined-tickets/",
      recoverTicket: "/admin/recover-ticket/",
    },
    crm: {
      //get all branches from crm
      branches: "/crm/branches/",
      //associate crm branch with queue branch
      associateBranches: "/super-admin/associate/branches",
    },
  },
} as const;

export type Route = typeof routes[keyof typeof routes];
