import React from "react";
import { PureComponent } from "react";
import classes from "./NavBar.css";
import cn from "clsx";
import Typography from "../Typography";
import { routes } from "src/config/routes";
import ticketImg from "src/components/NavBar/images/ticket.svg";
import tvImg from "src/components/NavBar/images/tv.svg";
import logoutImg from "src/components/NavBar/images/logout.svg";
import { Form } from "react-bootstrap";
import { userStatusId } from "src/helpers/auth-data";
import { userStatusName } from "src/helpers/auth-data";
import {
  UserStatus,
  UserStatuses,
} from "src/services/QueueServicePageService/QueueServicePageService";
import QueueServicePageAdapter from "src/services/QueueServicePageService/QueueServicePageAdapter";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { createNotification } from "src/helpers/crud-notifications";

interface INavBar {
  userStatus: string;
  userStatusesList: UserStatuses;
}

class NavBar extends PureComponent<{ onStatusChanged: any }, INavBar> {
  private adapter: QueueServicePageAdapter;

  constructor(props: { onStatusChanged: any }) {
    super(props);
    this.state = { userStatus: "", userStatusesList: [{ id: 0, name: "" }] };
    this.adapter = new QueueServicePageAdapter();
  }

  componentDidMount(): void {
    this.setUserStatusesList();
  }

  async setUserStatusesList(): Promise<void> {
    const userStatusesList = await this.adapter.getUserStatusesList();
    this.setState({ userStatusesList: userStatusesList });
  }

  setUserStatus = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const stausId = event.target.value;
    const response = await this.adapter.updateUserStatus(Number(stausId));
    if (response == "STATUS_IS_UPDATED") {
      await this.setState({ userStatus: stausId });
      createNotification("success", "Статус обновлён");
    } else {
      createNotification("error", "Статус не обновлён");
    }
    if (userStatusName() == "work time") {
      this.props.onStatusChanged(false);
    } else {
      this.props.onStatusChanged(true);
    }
  };

  logout = async () => {
    localStorage.removeItem("queue-user-data");
    localStorage.removeItem("queue-access-token");
    window.location.reload();
  };

  private getUserStatusesOptions(): (JSX.Element | null)[] {
    return this.state.userStatusesList.map(
      (userStatus: UserStatus, index: number) => {
        if (userStatus.id != userStatusId()) {
          return (
            <option key={index} value={userStatus.id}>
              {this.renderSwitch(userStatus.name)}
            </option>
          );
        }
        return null;
      }
    );
  }

  private renderSwitch(param: string): string {
    switch (param) {
      case "lunch time":
        return "Время обеда";
      case "break time":
        return "Время перерыва";
      case "work time":
        return "Время работы";
      case "offline time":
        return "Ушёл домой";
    }

    return "";
  }

  render(): React.ReactNode {
    return (
      <main>
        <NotificationContainer />
        <div className={cn(classes.navbarMenuBlock)}>
          <Form.Group className={cn(classes.formGroup)}>
            <Form.Control
              as="select"
              value={this.state.userStatus}
              onChange={this.setUserStatus}
              className={cn(classes.select)}
            >
              <option value={userStatusId()}>
                {this.renderSwitch(userStatusName())}
              </option>
              {this.getUserStatusesOptions()}
            </Form.Control>
          </Form.Group>
          <ul className={cn(classes.navbarMenuList)}>
            <Typography variant="li">
              <a
                className={cn(classes.iconButton)}
                href={routes.registerTicket}
              >
                <img src={ticketImg} />
              </a>
            </Typography>
            <Typography variant="li">
              <a className={cn(classes.iconButton)} href={routes.screens}>
                <img src={tvImg} />
              </a>
            </Typography>
            <Typography variant="li">
              <button className={cn(classes.iconButton)} onClick={this.logout}>
                <img src={logoutImg} />
              </button>
            </Typography>
          </ul>
        </div>
      </main>
    );
  }
}

export default NavBar;
