import React, { useState } from "react";
import classes from "./SideBar.css";
import cn from "clsx";

const SubMenu = ({ item }: any) => {
  const [subnav, setSubnav] = useState(false);
  const showSubnav = () => setSubnav(!subnav);
  const location = window.location.pathname;
  return (
    <>
      <ul className={cn(classes.menuList)}>
        <li className={cn(classes.sidebarListItem)}>
          <a className={cn(classes.sidebarLink)} onClick={showSubnav}>
            <span>{item.title}</span>
            {subnav ? item.iconOpened() : item.iconClosed()}
          </a>
        </li>
        {subnav
          ? item.subNavs.map((subNav: any, index: number) => {
              return (
                <li className={cn(classes.sidebarListItem)} key={index}>
                  <a
                    className={cn(
                      classes.sidebarLink,
                      location == subNav.locationPathname
                        ? classes.activeSidebarLink
                        : ""
                    )}
                    href={subNav.path}
                  >
                    <span>{subNav.icon()}</span>
                    <span>{subNav.title}</span>
                  </a>
                </li>
              );
            })
          : null}
      </ul>
    </>
  );
};

export default SubMenu;
