export function setIsRegisteredTicketValue<T>(isRegisteredTicket: T) {
  return {
    type: "SET_IS_REGISTERED_TICKET_VALUE",
    isRegisteredTicket: isRegisteredTicket,
  };
}

export function setVisitPurposeIdValue<T>(visitPurposeId: T) {
  return {
    type: "SET_VISIT_PURPOSE_ID_VALUE",
    visitPurposeId: visitPurposeId,
  };
}

export function setCustomerPhoneNumberValue<T>(customerPhoneNumber: T) {
  return {
    type: "SET_CUSTOMER_PHONE_NUMBER_VALUE",
    customerPhoneNumber: customerPhoneNumber,
  };
}
