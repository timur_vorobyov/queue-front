export default function (state: any, action: any) {
  switch (action.type) {
    case "SET_IS_REGISTERED_TICKET_VALUE":
      state.isRegisteredTicket = action.isRegisteredTicket;
      return state;
    case "SET_VISIT_PURPOSE_ID_VALUE":
      state.visitPurposeId = action.visitPurposeId;
      return state;
    case "SET_CUSTOMER_PHONE_NUMBER_VALUE":
      state.customerPhoneNumber = action.customerPhoneNumber;
      return state;
    default:
      return state;
  }
}
