import { createStore } from "redux";
import reducer from "./reducers";

const initialState = { values: "" };
const store = createStore(reducer, initialState);
export default store;
