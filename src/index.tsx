import React from "react";
import { render } from "react-dom";
import AppWrapper from "src/wrappers/AppWrapper";
import "!style-loader!css-loader!bootstrap/dist/css/bootstrap.css";
import "!style-loader!css-loader!react-notifications/lib/notifications.css";
import "!style-loader!css-loader!react-datepicker/dist/react-datepicker.css";
import BodyWrapper from "./wrappers/BodyWrapper/BodyWrapper";
import PagesWithoutSideBars from "./pages";
import {
  UserDataContext,
  UserDataContextProvider,
} from "./contexts/UserDataContext";
import { Redirect } from "react-router-dom";
import { routes } from "./config/routes";

render(
  <UserDataContextProvider>
    <AppWrapper>
      <PagesWithoutSideBars>
        <UserDataContext.Consumer>
          {(value) =>
            value.isAuth ? <BodyWrapper /> : <Redirect to={routes.login} />
          }
        </UserDataContext.Consumer>
      </PagesWithoutSideBars>
    </AppWrapper>
  </UserDataContextProvider>,
  document.getElementById("root") as HTMLElement
);
