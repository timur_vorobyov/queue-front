import React, { PureComponent } from "react";
import { Switch, Route } from "react-router-dom";
import { routes } from "src/config/routes";
import LoginPage from "./LoginPage";
import NotFound from "./ErrorsPages/NotFound";
import ScreensPages from "./ScreensPages";
import VerticalScreenPage from "./ScreensPages/VerticalScreenPage";
import HorizontalScreenPage from "./ScreensPages/HorizontalScreenPage";
import CreateTicketPages from "./UserPage/CreateTicketPages";
import CustomerPhoneNumberPage from "./UserPage/CreateTicketPages/CustomerPhoneNumberPage";
import ShowTicketPage from "./UserPage/CreateTicketPages/ShowTicketPage";
import TicketVisitPurposePage from "./UserPage/CreateTicketPages/TicketVisitPurposePage";

interface PagesWithoutNavAndSideBarsProps {
  children: React.ReactNode;
}

class PagesWithoutNavAndSideBars extends PureComponent<
  PagesWithoutNavAndSideBarsProps
> {
  render(): React.ReactNode {
    return (
      <Switch>
        <Route path={routes.notFound} exact component={NotFound} />
        <Route path={routes.login} exact component={LoginPage} />
        <Route path={routes.screens} exact component={ScreensPages} />
        <Route path={routes.tv} exact component={HorizontalScreenPage} />
        <Route path={routes.led} exact component={VerticalScreenPage} />
        <Route path={routes.ledWhite} exact component={VerticalScreenPage} />
        <Route
          path={routes.registerTicket}
          exact
          component={CreateTicketPages}
        />
        <Route
          path={routes.ticketVisitPurpose}
          exact
          component={TicketVisitPurposePage}
        />
        <Route
          path={routes.ticketCustomerPhoneNumber}
          exact
          component={CustomerPhoneNumberPage}
        />
        <Route path={routes.showTicket} exact component={ShowTicketPage} />
        <Route>{this.props.children}</Route>
        <Route path={"*"} component={NotFound} />
      </Switch>
    );
  }
}

export default PagesWithoutNavAndSideBars;
