import classes from "./UserPage.css";
import cn from "clsx";
import React, { PureComponent } from "react";
import Typography from "src/components/Typography";
import { Button, Form, Table } from "react-bootstrap";
import UserPageAdapter, {
  User,
  Users,
} from "src/services/HallAdminPagesServices/UserPageService/UserPageAdapter";
import Pagination, {
  PaginationData,
} from "src/components/Pagination/Pagination";
import TableRow from "src/pages/Admin/HallAdminPages/UserPage/components/TableRow/TableRow";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { createNotification } from "src/helpers/crud-notifications";

interface UserPageState {
  usersList: Users;
  userId: number;
  branchOfficeId: string;
  searchExpression: string;
  currentPage: number;
  pageLimit: number;
  paginationData: PaginationData;
  activeRowId: number;
}

class UserPage extends PureComponent<{}, UserPageState> {
  private adapter: UserPageAdapter;
  private pagesCountLimmit: Array<number>;

  constructor(props: {}) {
    super(props);
    this.state = {
      userId: 0,
      branchOfficeId: "",
      searchExpression: "",
      currentPage: 1,
      pageLimit: 5,
      usersList: [
        {
          id: 0,
          branchOfficeName: "",
          email: "",
          deskNumber: "",
          isActive: false,
          fullName: "",
          firstName: "",
          lastName: "",
          createdAt: "",
          branchOfficeId: "",
          visitPurposeId: "",
          role: { id: 0, name: "" },
        },
      ],
      paginationData: { currentPage: 0, totalPages: 0, totalRecords: 0 },
      activeRowId: 0,
    };

    this.pagesCountLimmit = [5, 10, 15, 20, 50, 100];
    this.adapter = new UserPageAdapter();
  }

  componentDidMount(): void {
    this.setUsersList();
  }

  async setUsersList(): Promise<void> {
    const usersList = await this.adapter.getPaginatedUsersList(
      this.state.searchExpression,
      this.state.pageLimit,
      this.state.currentPage
    );
    const paginationData = this.adapter.getPaginationData();
    await this.setState({
      usersList: usersList,
      paginationData: paginationData,
    });
  }

  onPageChanged = async (page: number): Promise<void> => {
    await this.setState({ currentPage: page });
    await this.setUsersList();
  };

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <NotificationContainer />
        <div>
          <div className={cn(classes.filters)}>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.pageLimit}
                onChange={this.setPage}
                custom
              >
                {this.getPageOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                type="text"
                value={this.state.searchExpression}
                onChange={this.setSearchExpression}
              />
            </Form.Group>
            <Button variant="primary" onClick={this.filter}>
              <Typography color="white" variant="span">
                Фильтр
              </Typography>
            </Button>
          </div>
          <div className={cn(classes.userContent)}>
            <Table className={cn(classes.userTable)} striped size="lg">
              <thead className={cn(classes.tableHeader)}>
                <tr>
                  <th>
                    <Typography variant="span">Имя</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Роль</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Филиал</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Окно</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Дата создания</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Статус/Действия</Typography>
                  </th>
                </tr>
              </thead>
              <tbody>{this.getPaginatedUsersList()}</tbody>
            </Table>
          </div>
        </div>
        <div>
          {this.state.paginationData.totalPages != 0 ? (
            <Pagination
              totalPages={this.state.paginationData.totalPages}
              totalRecords={this.state.paginationData.totalRecords}
              onPageChanged={this.onPageChanged}
              currentPage={this.state.paginationData.currentPage}
            />
          ) : null}
        </div>
      </main>
    );
  }

  setPage = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    this.setState({ pageLimit: Number(event.target.value) });
  };

  getPageOptions(): JSX.Element[] {
    return this.pagesCountLimmit.map((pageCount: number, index: number) => {
      return (
        <option key={index} value={pageCount}>
          {pageCount}
        </option>
      );
    });
  }

  setSearchExpression = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ searchExpression: event.target.value });
  };

  filter = (): void => {
    this.setUsersList();
  };

  getPaginatedUsersList(): JSX.Element[] {
    return this.state.usersList.map((user: User) => {
      return (
        <TableRow
          key={user.id}
          activeRowId={this.state.activeRowId}
          handleRowClick={this.handleRowClick}
          handleDeleteUserClick={this.handleDeleteUserClick}
          handleUpdateUserClick={this.handleUpdateUserClick}
          setUserList={this.setUsersList}
          user={user}
        />
      );
    });
  }

  handleRowClick = async (userId: number): Promise<void> => {
    await this.setState({ activeRowId: userId });
  };

  handleDeleteUserClick = async (userId: number): Promise<void> => {
    const response = await this.adapter.deleteUser(userId);
    if (response == "User is removed") {
      await this.setUsersList();
      createNotification("error", "Пользователь удалён");
    }
  };

  handleUpdateUserClick = async (
    userId: number,
    deskNumber: number | undefined
  ): Promise<void> => {
    const response = await this.adapter.updateUser(userId, deskNumber);

    if (response == "Desk number already exists") {
      await this.setUsersList();
      createNotification("error", "Пользователь с таким столом уже существует");
    }

    if (response == "User is updated") {
      await this.setUsersList();
      createNotification("success", "Пользователь обновлён");
    }
  };
}

export default UserPage;
