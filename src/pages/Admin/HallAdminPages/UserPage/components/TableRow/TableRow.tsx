import { useState } from "react";
import cn from "clsx";
import classes from "src/pages/Admin/HallAdminPages/UserPage/components/TableRow/TableRow.css";
import commonClasses from "src/pages/Admin/HallAdminPages/HallAdminPages.css";
import React from "react";
import { Form } from "react-bootstrap";
import Delete from "src/pages/Admin/HallAdminPages/images/Delete";
import Save from "src/pages/Admin/HallAdminPages/images/Save";

const TableRow = (props: any) => {
  const [deskNumber, setDeskNumber] = useState<number>(1);

  const handleDeskNuberChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setDeskNumber(Number(event.target.value));
  };

  return (
    <tr
      className={cn(classes.tableRaw, {
        [classes.active]: props.activeRowId == props.user.id,
        [classes.tableActiveRaw]: props.activeRowId == props.user.id,
      })}
      onClick={() => {
        props.handleRowClick(props.user.id);
      }}
    >
      <td className={cn(classes.column)}>{props.user.fullName}</td>
      <td className={cn(classes.column)}>{props.user.role.name}</td>
      <td className={cn(classes.column)}>{props.user.branchOfficeName}</td>
      <td className={cn(classes.deskNumberColumn)}>
        {props.activeRowId == props.user.id ? (
          <Form.Control
            className={cn(classes.deskNumberInput, classes.column)}
            type="number"
            value={deskNumber}
            onChange={handleDeskNuberChange}
            min="1"
          />
        ) : (
          props.user.deskNumber
        )}
      </td>
      <td className={cn(classes.column)}>{props.user.createdAt}</td>
      <td className={cn(classes.column)}>
        {props.activeRowId == props.user.id ? (
          <div>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                props.handleDeleteUserClick(props.user.id);
              }}
            >
              {Delete()}
            </button>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                props.handleUpdateUserClick(props.user.id, deskNumber);
              }}
            >
              {Save()}
            </button>
          </div>
        ) : props.user.isActive ? (
          "Активен"
        ) : (
          "Не активен"
        )}
      </td>
    </tr>
  );
};

export default TableRow;
