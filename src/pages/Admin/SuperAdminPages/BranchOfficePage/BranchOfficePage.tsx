import classes from "./BranchOfficePage.css";
import commonClasses from "../SuperAdminPages.css";
import cn from "clsx";
import React, { PureComponent } from "react";
import Typography from "src/components/Typography";
import { Button, Form, Modal, Table } from "react-bootstrap";
import BranchOfficePageAdapter, {
  BranchOffice,
  BranchOffices,
  CrmBranch,
  CrmBranches,
  Town,
  Towns,
  VisitPurpose,
  VisitPurposes,
} from "src/services/SuperAdminPagesServices/BranchOfficePageService/BranchOfficePageAdapter";
import Delete from "../images/Delete";
import Edit from "../images/Edit";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { createNotification } from "src/helpers/crud-notifications";
// @ts-ignore
import Select from "react-select";

interface BranchOfficePageStates {
  showTownModal: boolean;
  showDeleteTownModal: boolean;
  showBranchOfficeModal: boolean;
  showDeleteBranchOfficeModal: boolean;
  townName: string;
  action: string;
  townsList: Towns;
  townId: number | string;
  branchOfficeId: number;
  branchOfficesList: BranchOffices;
  initial: string;
  branchOfficeName: string;
  timer: number;
  visitPurposesList: VisitPurposes;
  visitPurposesIds: number[];
  crmBranch: boolean;
  crmBranchesList: CrmBranches;
  crmBranchData: { id: number; displayName: string; crmBranchName: string };
}

class BranchOfficePage extends PureComponent<{}, BranchOfficePageStates> {
  private adapter: BranchOfficePageAdapter;

  constructor(props: {}) {
    super(props);
    this.adapter = new BranchOfficePageAdapter();
    this.state = {
      showTownModal: false,
      showDeleteTownModal: false,
      townName: "",
      townsList: [{ id: 1, name: "" }],
      townId: 0,
      showBranchOfficeModal: false,
      showDeleteBranchOfficeModal: false,
      branchOfficesList: [
        {
          id: 0,
          townName: "",
          branchOfficeName: "",
          initial: "",
          townId: 0,
          timer: 5000,
          visitPurposes: [{ name: "" }],
        },
      ],
      branchOfficeId: 0,
      action: "",
      branchOfficeName: "",
      initial: "",
      visitPurposesList: [{ id: 0, name: "" }],
      visitPurposesIds: [],
      timer: 5000,
      crmBranch: false,
      crmBranchesList: [{ crmBranchName: "", crmBranchLabel: "" }],
      crmBranchData: { id: 0, displayName: "", crmBranchName: "" },
    };
  }

  componentDidMount(): void {
    this.setLists();
  }

  async setLists(): Promise<void> {
    const townsList = await this.adapter.getTownsList();
    const branchOfficesList = await this.adapter.getBranchOfficesList();
    const visitPurposesList = await this.adapter.getVisitPurposesList();
    const crmBranchesList = await this.adapter.getCrmBranches();
    this.setState({
      townsList: townsList,
      branchOfficesList: branchOfficesList,
      visitPurposesList: visitPurposesList,
      crmBranchesList: crmBranchesList,
    });
  }

  getCrmBranchOptions(): any {
    const options = this.state.crmBranchesList.map((crmBranch: CrmBranch) => {
      return {
        value: crmBranch.crmBranchName,
        label: crmBranch.crmBranchLabel,
      };
    });
    return options;
  }

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <NotificationContainer />
        <div className={cn(classes.branchOfficeContent)}>
          <div className={cn(classes.tableContent)}>
            <div className={cn(classes.tableHeader)}>
              <Typography variant="span">Город</Typography>
              <Button
                variant="primary"
                onClick={() => {
                  this.handleShowTownModalClick("create", 0);
                }}
              >
                {" "}
                <Typography color="white" variant="span">
                  Добавить
                </Typography>{" "}
              </Button>
            </div>
            <Table className={cn(classes.townTable)} striped size="lg">
              <thead className={cn(classes.townTableHeader)}>
                <tr>
                  <th>
                    <Typography variant="span">#</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Название</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Действие</Typography>
                  </th>
                </tr>
              </thead>
              <tbody>{this.getTownsList()}</tbody>
            </Table>
            {this.getTownModal()}
            {this.getDeleteTownModal()}
          </div>

          <div className={cn(classes.tableContent, classes.officeTable)}>
            <div className={cn(classes.tableHeader)}>
              <Typography variant="span">Филиал</Typography>
              <Button
                variant="primary"
                onClick={() => {
                  this.handleShowBranchOfficeModalClick("create", 0);
                }}
              >
                {" "}
                <Typography color="white" variant="span">
                  Добавить
                </Typography>{" "}
              </Button>
            </div>
            <Table className={cn(classes.townTable)} striped size="lg">
              <thead className={cn(classes.townTableHeader)}>
                <tr>
                  <th>
                    <Typography variant="span">#</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Название</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Цели визита</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Город</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Инициал</Typography>
                  </th>
                  <th>
                    <Typography variant="span">
                      Время браузерного оповещения
                    </Typography>
                  </th>
                  <th>
                    <Typography variant="span">Действие</Typography>
                  </th>
                </tr>
              </thead>
              <tbody>{this.getBranchOfficesList()}</tbody>
            </Table>
            {this.getBranchOfficeModal()}
            {this.getDeleteBranchOfficeModal()}
          </div>
        </div>
      </main>
    );
  }

  getTownsList(): JSX.Element[] {
    return this.state.townsList.map((town: Town, index: number) => {
      return (
        <tr key={town.id} className={cn(classes.townTableRaw)}>
          <td>{index + 1}</td>
          <td>{town.name}</td>
          <td>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                this.handleDeleteTownClick(town.id);
              }}
            >
              {Delete()}
            </button>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                this.handleShowTownModalClick("update", town.id);
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  }

  handleDeleteTownClick = async (townId: number): Promise<void> => {
    await this.setState({ showDeleteTownModal: true });
    await this.setState({ townId: townId });
  };

  handleShowTownModalClick = (action: string, townId: number): void => {
    const town = this.state.townsList.find((i) => i.id == townId);

    if (action == "update") {
      if (town) {
        this.setState({ townName: town.name });
      }
    } else {
      this.setState({ townName: "" });
    }

    this.setState({ action: action });
    this.setState({ townId: townId });
    this.setState({ showTownModal: true });
  };

  getTownModal(): JSX.Element {
    return (
      <Modal
        show={this.state.showTownModal}
        onHide={this.handleCloseTownModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              {this.state.action == "create"
                ? "Добавить город"
                : "Изменить город"}
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <Form.Group>
              <Typography variant="label">Имя города</Typography>
              <Form.Control
                type="text"
                value={this.state.townName}
                onChange={this.setTownName}
              />
            </Form.Group>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.handleCloseTownModalClick}>
            <Typography color="white" variant="span">
              Закрыть
            </Typography>
          </Button>
          <Button
            variant="primary"
            onClick={
              this.state.action == "create"
                ? this.handleCreateTownClick
                : this.handleEditTownClick
            }
          >
            <Typography color="white" variant="span">
              {this.state.action == "create" ? "Сохранить" : "Обновить"}
            </Typography>
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  handleCloseTownModalClick = (): void => {
    this.setState({ showTownModal: false });
  };

  setTownName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ townName: event.target.value });
  };

  handleCreateTownClick = async (): Promise<void> => {
    this.setState({ showTownModal: false });
    const response = await this.adapter.storeTown(this.state.townName);

    if (response == "422") {
      createNotification(
        "error",
        "Город не сохранен, не все данные были введены"
      );
    }

    if (response == "Town is saved") {
      this.setTownsList();
      createNotification("success", "Город сохранен");
    }
  };

  handleEditTownClick = async (): Promise<void> => {
    this.setState({ showTownModal: false });
    const response = await this.adapter.updateTown(
      this.state.townId,
      this.state.townName
    );
    if (response == "Town is updated") {
      this.setTownsList();
      createNotification("success", "Город обновлен");
    }
  };

  async setTownsList(): Promise<void> {
    const townsList = await this.adapter.getTownsList();
    this.setState({ townsList: townsList });
  }

  getDeleteTownModal(): JSX.Element {
    return (
      <Modal
        className={cn(commonClasses.modalDiolog)}
        show={this.state.showDeleteTownModal}
        onHide={this.handleRenouncementDeleteTownModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              Вы уверены что хотите удалить выбранный пункт?
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="secondary"
            onClick={this.handleConfirmationDeleteTownModalClick}
          >
            <Typography color="white" variant="span">
              Да
            </Typography>
          </Button>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="primary"
            onClick={this.handleRenouncementDeleteTownModalClick}
          >
            <Typography color="white" variant="span">
              Нет
            </Typography>
          </Button>
        </Modal.Body>
      </Modal>
    );
  }

  handleConfirmationDeleteTownModalClick = async (): Promise<void> => {
    this.setState({ showDeleteTownModal: false });
    const response = await this.adapter.deleteTown(this.state.townId);

    if (response == "Town is removed") {
      this.setTownsList();
      createNotification("error", "Город удалён");
    }
  };

  handleRenouncementDeleteTownModalClick = (): void => {
    this.setState({ showDeleteTownModal: false });
  };

  handleShowBranchOfficeModalClick = (
    action: string,
    branchOfficeId: number
  ): void => {
    this.setState({ action: action });
    if (action == "update") {
      const branchOffice = this.state.branchOfficesList.find(
        (i) => i.id == branchOfficeId
      );

      if (branchOffice) {
        this.setState({ branchOfficeId: branchOffice.id });
        this.setState({ branchOfficeName: branchOffice.branchOfficeName });
        this.setState({ townId: branchOffice.townId });
        this.setState({ initial: branchOffice.initial });
      }
    } else {
      this.setState({ branchOfficeId: 0 });
      this.setState({ branchOfficeName: "" });
      this.setState({ townId: "" });
      this.setState({ initial: "" });
    }

    this.setState({ showBranchOfficeModal: true });
  };

  getBranchOfficesList = (): JSX.Element[] => {
    return this.state.branchOfficesList.map(
      (branchOffice: BranchOffice, index: number) => {
        return (
          <tr key={branchOffice.id} className={cn(classes.townTableRaw)}>
            <td>{index + 1}</td>
            <td>{branchOffice.branchOfficeName}</td>
            <td>{this.getVisitPurposesRows(branchOffice.visitPurposes)}</td>
            <td>{branchOffice.townName}</td>
            <td>{branchOffice.initial}</td>
            <td>{branchOffice.timer}</td>
            <td>
              <button
                className={cn(commonClasses.buttonAction)}
                type="button"
                onClick={() => {
                  this.handleDeleteBranchOfficeClick(branchOffice.id);
                }}
              >
                {Delete()}
              </button>
              <button
                className={cn(commonClasses.buttonAction)}
                type="button"
                onClick={() => {
                  this.handleShowBranchOfficeModalClick(
                    "update",
                    branchOffice.id
                  );
                }}
              >
                {Edit()}
              </button>
            </td>
          </tr>
        );
      }
    );
  };

  getVisitPurposesRows(visitPurposes: { name: string }[]): any {
    const arrayLength = visitPurposes.length;
    return visitPurposes.map(
      (visitPurpose: { name: string }, index: number) => {
        if (arrayLength == index + 1) {
          return visitPurpose.name;
        }

        return visitPurpose.name + ", ";
      }
    );
  }

  handleDeleteBranchOfficeClick = async (
    branchOfficeId: number
  ): Promise<void> => {
    await this.setState({ showDeleteBranchOfficeModal: true });
    await this.setState({ branchOfficeId: branchOfficeId });
  };

  getBranchOfficeModal(): JSX.Element {
    return (
      <Modal
        show={this.state.showBranchOfficeModal}
        onHide={this.handleCloseBranchOfficenModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              {this.state.action == "create"
                ? "Добавить филиал"
                : "Изменить филиал"}
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <Form.Group>
              <Typography variant="label">Имя города</Typography>
              <Form.Control
                as="select"
                value={this.state.townId}
                onChange={this.setTownId}
                custom
              >
                <option>Пожалуйста выберите город</option>
                {this.getTownOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group>
              <Typography variant="label">Имя филиала</Typography>
              <Form.Control
                type="text"
                value={this.state.branchOfficeName}
                onChange={this.setBranchOfficeName}
              />
            </Form.Group>
            <Form.Group>
              <Typography variant="label">Инициал филиала</Typography>
              <Form.Control
                type="text"
                value={this.state.initial}
                onChange={this.setInitial}
              />
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Typography variant="label">Цели визита</Typography>
              <Select
                options={this.getVisitPurposeOptions()}
                isMulti
                onChange={this.setVisitPurposesIds}
              />
            </Form.Group>
            <Form.Group>
              <Typography variant="label">Таймер</Typography>
              <Form.Control
                type="text"
                value={this.state.timer}
                onChange={this.setTimer}
              />
            </Form.Group>
            {this.getCrmBranchesModal()}
            {this.state.action == "create" ? (
              <> </>
            ) : (
              <Form.Check
                className={cn(classes.crmBranchSwitch)}
                type="switch"
                id="custom-switch"
                label="Связать филиалы?"
                checked={this.state.crmBranch}
                onChange={this.setCrmBranch}
              />
            )}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleCloseBranchOfficenModalClick}
          >
            <Typography color="white" variant="span">
              Закрыть
            </Typography>
          </Button>
          <Button
            variant="primary"
            onClick={
              this.state.action == "create"
                ? this.handleCreateBranchOfficeClick
                : this.handleEditBranchOfficeClick
            }
          >
            <Typography color="white" variant="span">
              {this.state.action == "create" ? "Сохранить" : "Обновить"}
            </Typography>
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  handleCloseBranchOfficenModalClick = (): void => {
    this.setState({ showBranchOfficeModal: false });
  };

  setTownId = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const townId = event.target.value;
    this.setState({ townId: townId });
    const town = this.state.townsList.find((i) => i.id == Number(townId));

    if (town) {
      this.setState({ townName: town.name });
    }
  };

  setBranchOfficeName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ branchOfficeName: event.target.value });
  };

  setInitial = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ initial: event.target.value });
  };

  getVisitPurposeOptions(): any {
    const options = this.state.visitPurposesList.map(
      (visitPurpose: VisitPurpose) => {
        return { value: visitPurpose.id, label: visitPurpose.name };
      }
    );
    return options;
  }

  setVisitPurposesIds = async (
    selectedVisitPurpose: [{ value: number }]
  ): Promise<void> => {
    const options = selectedVisitPurpose;
    const selectedOptions = [];

    for (let i = 0; i < options.length; i++) {
      selectedOptions.push(options[i].value);
    }

    await this.setState({ visitPurposesIds: selectedOptions });
  };

  setTimer = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ timer: Number(event.target.value) });
  };

  getTownOptions(): JSX.Element[] {
    return this.state.townsList.map((town: Town, index: number) => {
      return (
        <option key={index} value={town.id}>
          {town.name}
        </option>
      );
    });
  }

  handleCreateBranchOfficeClick = async (): Promise<void> => {
    this.setState({ showBranchOfficeModal: false });
    const response = await this.adapter.storeBranchOffice(
      this.state.townId,
      this.state.branchOfficeName,
      this.state.initial,
      this.state.timer,
      this.state.visitPurposesIds,
      this.state.crmBranchData.crmBranchName,
      this.state.crmBranchData.displayName,
      this.state.crmBranchData.id
    );

    if (response == "INITIAL_ERROR") {
      createNotification("error", "Филиал с таким инициалом уже существует");
    }

    if (response == "422") {
      createNotification(
        "error",
        "Филиал не сохранен, не все данные были введены"
      );
    }

    if (response == "Branch office is saved") {
      this.setBranchOfficesList();
      createNotification("success", "Филиал создан");
    }
  };

  handleEditBranchOfficeClick = async (): Promise<void> => {
    this.setState({ showBranchOfficeModal: false });
    const response = await this.adapter.updateBranchOffice(
      this.state.townId,
      this.state.branchOfficeId,
      this.state.branchOfficeName,
      this.state.initial,
      this.state.timer,
      this.state.visitPurposesIds
    );
    if (response == "Branch office is updated") {
      this.setBranchOfficesList();
      createNotification("success", "Филиал обновлен");
    }
  };

  getDeleteBranchOfficeModal(): JSX.Element {
    return (
      <Modal
        className={cn(commonClasses.modalDiolog)}
        show={this.state.showDeleteBranchOfficeModal}
        onHide={this.handleRenouncementDeleteBranchOfficeModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              Вы уверены что хотите удалить выбранный пункт?
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="secondary"
            onClick={this.handleConfirmationDeleteBranchOfficeModalClick}
          >
            <Typography color="white" variant="span">
              Да
            </Typography>
          </Button>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="primary"
            onClick={this.handleRenouncementDeleteBranchOfficeModalClick}
          >
            <Typography color="white" variant="span">
              Нет
            </Typography>
          </Button>
        </Modal.Body>
      </Modal>
    );
  }

  handleConfirmationDeleteBranchOfficeModalClick = async (): Promise<void> => {
    this.setState({ showDeleteBranchOfficeModal: false });
    const response = await this.adapter.deleteBranchOffice(
      this.state.branchOfficeId
    );
    if (response == "Branch office is removed") {
      this.setBranchOfficesList();
      createNotification("error", "Филиал удалён");
    }
  };

  async setBranchOfficesList(): Promise<void> {
    const branchOfficesList = await this.adapter.getBranchOfficesList();
    this.setState({ branchOfficesList: branchOfficesList });
  }

  handleRenouncementDeleteBranchOfficeModalClick = (): void => {
    this.setState({ showDeleteBranchOfficeModal: false });
  };

  getCrmBranchesModal(): JSX.Element {
    if (this.state.crmBranch) {
      return (
        <Form.Group className={cn(classes.formGroup)}>
          <Typography variant="label">Связать филиал с СРМ</Typography>
          <Select
            options={this.getCrmBranchOptions()}
            onChange={this.handleCrmBranchChange}
          />
        </Form.Group>
      );
    }

    return <></>;
  }

  handleCrmBranchChange = async (selectedCrmBranch: {
    value: string | null;
    label: string | null;
  }): Promise<void> => {
    const data = {
      crmBranchName: selectedCrmBranch.value,
      displayName: selectedCrmBranch.label,
      branchOfficeId: this.state.branchOfficeId,
    };

    const isBranchesAssociated = await this.adapter.assosiateBranches(data);

    if (isBranchesAssociated) {
      createNotification("success", "Филиал синхронизирован с филмалом CRM");
    } else {
      createNotification(
        "error ",
        "Филиал не удалось синхронизировать с филмалом CRM"
      );
    }
  };

  setCrmBranch = (): void => {
    if (this.state.crmBranch) {
      this.diassosiateBranch();
      this.setState({ crmBranch: false });
    } else {
      this.setState({ crmBranch: true });
    }
  };

  diassosiateBranch() {
    const selectedCrmBranch = {
      value: null,
      label: null,
    };
    this.handleCrmBranchChange(selectedCrmBranch);
  }
}

export default BranchOfficePage;
