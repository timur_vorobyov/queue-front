import classes from "./ServicePage.css";
import commonClasses from "../SuperAdminPages.css";
import cn from "clsx";
import React, { PureComponent } from "react";
import Typography from "src/components/Typography";
import { Button, Form, Modal, Table } from "react-bootstrap";
import Pagination, {
  PaginationData,
} from "src/components/Pagination/Pagination";
import ServicePageAdapter, {
  Service,
  Services,
  VisitPurpose,
  VisitPurposes,
} from "src/services/SuperAdminPagesServices/ServicePageService/ServiceAdapterPage";
import Delete from "../images/Delete";
import Edit from "../images/Edit";
import { createNotification } from "src/helpers/crud-notifications";
// @ts-ignore
import { NotificationContainer } from "react-notifications";

interface ServicePageStates {
  showServiceModal: boolean;
  showDeleteServiceModal: boolean;
  action: string;
  visitPurposesList: VisitPurposes;
  isActive: boolean;
  searchExpression: string;
  currentPage: number;
  pageLimit: number;
  paginationData: PaginationData;
  servicesList: Services;
  serviceId: string | number;
  serviceName: string;
  filterVisitPurposeId: string;
}

class ServicePage extends PureComponent<{}, ServicePageStates> {
  private adapter: ServicePageAdapter;
  private pagesCountLimmit: Array<number>;

  constructor(props: {}) {
    super(props);
    this.adapter = new ServicePageAdapter();
    this.pagesCountLimmit = [5, 10, 15, 20, 50, 100];

    this.state = {
      showServiceModal: false,
      showDeleteServiceModal: false,
      visitPurposesList: [
        {
          id: 0,
          name: "",
        },
      ],
      filterVisitPurposeId: "",
      isActive: false,
      servicesList: [
        {
          id: 0,
          name: "",
          isActive: false,
        },
      ],
      action: "",
      searchExpression: "",
      currentPage: 1,
      pageLimit: 5,
      paginationData: { currentPage: 0, totalPages: 0, totalRecords: 0 },
      serviceName: "",
      serviceId: 0,
    };
  }

  componentDidMount(): void {
    this.setServicesList();
    this.setVisitPurposesList();
  }

  async setVisitPurposesList(): Promise<void> {
    const visitPurposesList = await this.adapter.getVisitPurposesList();
    await this.setState({
      visitPurposesList: visitPurposesList,
    });
  }

  async setServicesList(currentPage: null | number = null): Promise<void> {
    const nextPage = currentPage ? currentPage : this.state.currentPage;
    const servicesList = await this.adapter.getServicesList(
      this.state.filterVisitPurposeId,
      this.state.searchExpression,
      this.state.pageLimit,
      nextPage
    );
    const paginationData = this.adapter.getPaginationData();
    await this.setState({
      servicesList: servicesList,
      paginationData: paginationData,
    });
  }

  onPageChanged = async (page: number): Promise<void> => {
    await this.setState({ currentPage: page });
    await this.setServicesList();
  };

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <NotificationContainer />
        <div className={cn(classes.tableContent)}>
          <div className={cn(classes.filters)}>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.pageLimit}
                onChange={this.setPage}
                custom
              >
                {this.getPageOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.filterVisitPurposeId}
                onChange={this.setFilterVisitPurposeId}
                custom
              >
                <option value="0">Все цели визита</option>
                {this.getVisitPurposeOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                type="text"
                value={this.state.searchExpression}
                onChange={this.setSearchExpression}
              />
            </Form.Group>
            <Button variant="primary" onClick={this.filter}>
              <Typography color="white" variant="span">
                Фильтр
              </Typography>
            </Button>
            <Button
              variant="primary"
              onClick={() => {
                this.handleShowServiceModalClick("create", 0);
              }}
            >
              <Typography color="white" variant="span">
                Добавить
              </Typography>
            </Button>
          </div>
          <Table className={cn(classes.table)} striped size="lg">
            <thead className={cn(classes.tableHeader)}>
              <tr>
                <th>
                  <Typography variant="span">Название</Typography>
                </th>
                <th>
                  <Typography variant="span">Статус</Typography>
                </th>
                <th>
                  <Typography variant="span">Действие</Typography>
                </th>
              </tr>
            </thead>
            <tbody>{this.getServicesList()}</tbody>
          </Table>
          {this.getServiceModal()}
          {this.getDeleteServiceModal()}
          <div>
            {this.state.paginationData.totalPages != 0 ? (
              <Pagination
                totalPages={this.state.paginationData.totalPages}
                totalRecords={this.state.paginationData.totalRecords}
                onPageChanged={this.onPageChanged}
                currentPage={this.state.paginationData.currentPage}
              />
            ) : null}
          </div>
        </div>
      </main>
    );
  }

  setPage = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    this.setState({ pageLimit: Number(event.target.value) });
  };

  getPageOptions(): JSX.Element[] {
    return this.pagesCountLimmit.map((pageCount: number, index: number) => {
      return (
        <option key={index} value={pageCount}>
          {pageCount}
        </option>
      );
    });
  }

  setFilterVisitPurposeId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const filterVisitPurposeId = event.target.value;
    this.setState({ filterVisitPurposeId: filterVisitPurposeId });
  };

  getVisitPurposeOptions(): JSX.Element[] {
    return this.state.visitPurposesList.map(
      (visitPurpose: VisitPurpose, index: number) => {
        return (
          <option key={index} value={visitPurpose.id}>
            {visitPurpose.name}
          </option>
        );
      }
    );
  }

  setSearchExpression = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ searchExpression: event.target.value });
  };

  filter = (): void => {
    this.setServicesList(1);
  };

  handleShowServiceModalClick = (action: string, serviceId: number): void => {
    const service = this.state.servicesList.find((i) => i.id == serviceId);

    if (action == "update") {
      if (service) {
        this.setState({ serviceName: service.name });
        this.setState({ isActive: service.isActive });
      }
    } else {
      this.setState({ serviceName: "" });
      this.setState({ isActive: true });
    }

    this.setState({ action: action });
    this.setState({ serviceId: serviceId });
    this.setState({ showServiceModal: true });
  };

  getServicesList(): JSX.Element[] {
    return this.state.servicesList.map((service: Service) => {
      return (
        <tr key={service.id} className={cn(classes.tableRaw)}>
          <td>{service.name}</td>
          <td>{service.isActive ? "Активный" : "Не актвный"}</td>
          <td>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                this.handleDeleteServiceClick(service.id);
              }}
            >
              {Delete()}
            </button>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                this.handleShowServiceModalClick("update", service.id);
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  }

  handleDeleteServiceClick = async (serviceId: number): Promise<void> => {
    await this.setState({ showDeleteServiceModal: true });
    await this.setState({ serviceId: serviceId });
  };

  getServiceModal(): JSX.Element {
    return (
      <Modal
        className={cn(commonClasses.modalDiolog)}
        show={this.state.showServiceModal}
        onHide={this.handleCloseServiceModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              {this.state.action == "create"
                ? "Добавить услугу"
                : "Изменить услугу"}
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <Form.Group>
              <Typography variant="label">Имя услуги</Typography>
              <Form.Control
                type="text"
                value={this.state.serviceName}
                onChange={this.setServiceName}
              />
            </Form.Group>
            <Form.Group>
              <Form.Check
                type="switch"
                id="custom-switch"
                label="Активный?"
                checked={this.state.isActive}
                onChange={this.setIsActive}
              />
            </Form.Group>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={
              this.state.action == "create"
                ? this.handleCreateServiceClick
                : this.handleEditServiceClick
            }
          >
            <Typography color="white" variant="span">
              {this.state.action == "create" ? "Сохранить" : "Обновить"}
            </Typography>
          </Button>
          <Button variant="primary" onClick={this.handleCloseServiceModalClick}>
            <Typography color="white" variant="span">
              Закрыть
            </Typography>
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  handleCloseServiceModalClick = (): void => {
    this.setState({ showServiceModal: false });
  };

  setIsActive = (): void => {
    if (this.state.isActive) {
      this.setState({ isActive: false });
    } else {
      this.setState({ isActive: true });
    }
  };

  setServiceName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ serviceName: event.target.value });
  };

  setServiceId = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const serviceId = event.target.value;
    this.setState({ serviceId: serviceId });
    const service = this.state.servicesList.find(
      (i) => i.id == Number(serviceId)
    );

    if (service) {
      this.setState({ serviceName: service.name });
    }
  };

  handleCreateServiceClick = async (): Promise<void> => {
    this.setState({ showServiceModal: false });
    const response = await this.adapter.storeService(
      this.state.serviceName,
      this.state.isActive
    );

    if (response == "422") {
      createNotification(
        "error",
        "Сервис не сохранён, не все данные были введены"
      );
    }

    if (response == "Service is saved") {
      this.setServicesList();
      createNotification("success", "Сервис сохранён");
    }
  };

  handleEditServiceClick = async (): Promise<void> => {
    this.setState({ showServiceModal: false });
    const response = await this.adapter.updateService(
      this.state.serviceId,
      this.state.serviceName,
      this.state.isActive
    );

    if (response == "Service is updated") {
      this.setServicesList();
      createNotification("success", "Сервис обновлён");
    }
  };

  getDeleteServiceModal(): JSX.Element {
    return (
      <Modal
        className={cn(commonClasses.modalDiolog)}
        show={this.state.showDeleteServiceModal}
        onHide={this.handleRenouncementDeleteServiceModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              Вы уверены что хотите удалить выбранный пункт?
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="secondary"
            onClick={this.handleConfirmationDeleteServiceModalClick}
          >
            <Typography color="white" variant="span">
              Да
            </Typography>
          </Button>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="primary"
            onClick={this.handleRenouncementDeleteServiceModalClick}
          >
            <Typography color="white" variant="span">
              Нет
            </Typography>
          </Button>
        </Modal.Body>
      </Modal>
    );
  }

  handleConfirmationDeleteServiceModalClick = async (): Promise<void> => {
    this.setState({ showDeleteServiceModal: false });
    const response = await this.adapter.deleteService(this.state.serviceId);

    if (response == "Service is removed") {
      this.setServicesList();
      createNotification("error", "Сервис удалён");
    }
  };

  handleRenouncementDeleteServiceModalClick = (): void => {
    this.setState({ showDeleteServiceModal: false });
  };
}

export default ServicePage;
