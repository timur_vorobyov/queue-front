import classes from "./PermissionPage.css";
import commonClasses from "../SuperAdminPages.css";
import cn from "clsx";
import React, { PureComponent } from "react";
import Typography from "src/components/Typography";
import { Button, Form, Modal, Table } from "react-bootstrap";
import PermissionPageAdapter, {
  Permission,
  Permissions,
} from "src/services/SuperAdminPagesServices/PermissionPageService/PermissionPageAdapter";
import Delete from "../images/Delete";
import Edit from "../images/Edit";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { createNotification } from "src/helpers/crud-notifications";

interface PermissionPageStates {
  showPermissionModal: boolean;
  showDeletePermissionModal: boolean;
  permissionName: string;
  action: string;
  permissionsList: Permissions;
  permissionId: number | string;
}

class PerrmissionPage extends PureComponent<{}, PermissionPageStates> {
  private adapter: PermissionPageAdapter;

  constructor(props: {}) {
    super(props);
    this.adapter = new PermissionPageAdapter();
    this.state = {
      showPermissionModal: false,
      showDeletePermissionModal: false,
      permissionName: "",
      permissionsList: [{ id: 1, name: "" }],
      permissionId: 0,
      action: "",
    };
  }

  componentDidMount(): void {
    this.setPermissionsList();
  }

  async setPermissionsList(): Promise<void> {
    const permissionsList = await this.adapter.getPermissionsList();
    this.setState({ permissionsList: permissionsList });
  }

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <NotificationContainer />
        <div className={cn(classes.tableContent)}>
          <div className={cn(classes.tableHeader)}>
            <Typography variant="span">Пермишин</Typography>
            <Button
              variant="primary"
              onClick={() => {
                this.handleShowPermissionModalClick("create", 0);
              }}
            >
              <Typography color="white" variant="span">
                Добавить
              </Typography>
            </Button>
          </div>
          <Table className={cn(classes.permissionTable)} striped size="lg">
            <thead className={cn(classes.permissionTableHeader)}>
              <tr>
                <th>
                  <Typography variant="span">#</Typography>
                </th>
                <th>
                  <Typography variant="span">Название</Typography>
                </th>
                <th>
                  <Typography variant="span">Действие</Typography>
                </th>
              </tr>
            </thead>
            <tbody>{this.getPermissionsList()}</tbody>
          </Table>
          {this.getPermissionModal()}
          {this.getDeletePermissionModal()}
        </div>
      </main>
    );
  }

  handleShowPermissionModalClick = (
    action: string,
    permissionId: number
  ): void => {
    const permission = this.state.permissionsList.find(
      (i) => i.id == permissionId
    );

    if (action == "update") {
      if (permission) {
        this.setState({ permissionName: permission.name });
      }
    } else {
      this.setState({ permissionName: "" });
    }

    this.setState({ action: action });
    this.setState({ permissionId: permissionId });
    this.setState({ showPermissionModal: true });
  };

  getPermissionsList(): JSX.Element[] {
    return this.state.permissionsList.map(
      (permission: Permission, index: number) => {
        return (
          <tr key={permission.id} className={cn(classes.permissionTableRaw)}>
            <td>{index + 1}</td>
            <td>{permission.name}</td>
            <td>
              <button
                className={cn(commonClasses.buttonAction)}
                type="button"
                onClick={() => {
                  this.handleDeletePermissionClick(permission.id);
                }}
              >
                {Delete()}
              </button>
              <button
                className={cn(commonClasses.buttonAction)}
                type="button"
                onClick={() => {
                  this.handleShowPermissionModalClick("update", permission.id);
                }}
              >
                {Edit()}
              </button>
            </td>
          </tr>
        );
      }
    );
  }

  handleDeletePermissionClick = async (permissionId: number): Promise<void> => {
    await this.setState({ showDeletePermissionModal: true });
    await this.setState({ permissionId: permissionId });
  };

  getPermissionModal(): JSX.Element {
    return (
      <Modal
        className={cn(classes.modalPermissionDiolog)}
        show={this.state.showPermissionModal}
        onHide={this.handleClosePermissionModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              {this.state.action == "create"
                ? "Добавить пермишн"
                : "Изменить пермишн"}
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <Form.Group>
              <Typography variant="label">Имя пермишина</Typography>
              <Form.Control
                type="text"
                value={this.state.permissionName}
                onChange={this.setPermissionName}
              />
            </Form.Group>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={
              this.state.action == "create"
                ? this.handleCreatePermissionClick
                : this.handleEditPermissionClick
            }
          >
            <Typography color="white" variant="span">
              {this.state.action == "create" ? "Сохранить" : "Обновить"}
            </Typography>
          </Button>
          <Button
            variant="primary"
            onClick={this.handleClosePermissionModalClick}
          >
            <Typography color="white" variant="span">
              Закрыть
            </Typography>
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  handleClosePermissionModalClick = (): void => {
    this.setState({ showPermissionModal: false });
  };

  setPermissionName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ permissionName: event.target.value });
  };

  handleCreatePermissionClick = async (): Promise<void> => {
    this.setState({ showPermissionModal: false });
    const response = await this.adapter.storePermission(
      this.state.permissionName
    );

    if (response == "422") {
      createNotification(
        "error",
        "Пермишин не сохранен, не все данные были введены"
      );
    }

    if (response == "Permission is saved") {
      this.setPermissionsList();
      createNotification("success", "Пермишин создан");
    }
  };

  handleEditPermissionClick = async (): Promise<void> => {
    this.setState({ showPermissionModal: false });
    const response = await this.adapter.updatePermission(
      this.state.permissionId,
      this.state.permissionName
    );
    if (response == "Permission is updated") {
      this.setPermissionsList();
      createNotification("success", "Пермишин обновлен");
    }
  };

  getDeletePermissionModal(): JSX.Element {
    return (
      <Modal
        className={cn(commonClasses.modalDiolog)}
        show={this.state.showDeletePermissionModal}
        onHide={this.handleRenouncementDeletePermissionModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              Вы уверены что хотите удалить выбранный пункт?
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="secondary"
            onClick={this.handleConfirmationDeletePermissionModalClick}
          >
            <Typography color="white" variant="span">
              Да
            </Typography>
          </Button>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="primary"
            onClick={this.handleRenouncementDeletePermissionModalClick}
          >
            <Typography color="white" variant="span">
              Нет
            </Typography>
          </Button>
        </Modal.Body>
      </Modal>
    );
  }

  handleConfirmationDeletePermissionModalClick = async (): Promise<void> => {
    this.setState({ showDeletePermissionModal: false });
    const response = await this.adapter.deletePermission(
      this.state.permissionId
    );

    if (response == "Permission is removed") {
      this.setPermissionsList();
      createNotification("error", "Пермишин удалён");
    }
  };

  handleRenouncementDeletePermissionModalClick = (): void => {
    this.setState({ showDeletePermissionModal: false });
  };
}
export default PerrmissionPage;
