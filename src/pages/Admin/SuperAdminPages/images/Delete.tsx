import React from "react";

const Delete = () => {
  return (
    <svg height="30px" viewBox="0 0 64 64" width="30px" role="img">
      <circle data-name="layer1" cx="32" cy="32" r="30" fill="#ff5252"></circle>
      <path
        data-name="stroke"
        fill="none"
        stroke="#ffffff"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M42.999 21.001l-22 22m22 0L21 21"
      ></path>
    </svg>
  );
};

export default Delete;
