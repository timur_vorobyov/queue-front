import React from "react";
import { PureComponent } from "react";
import { Button, Form, Modal, Table } from "react-bootstrap";
import Typography from "src/components/Typography";
import cn from "clsx";
import classes from "./RolePage.css";
import commonClasses from "../SuperAdminPages.css";
import RolePageAdapter, {
  Role,
  Roles,
  Permission,
  Permissions,
} from "src/services/SuperAdminPagesServices/RolePageService/RolePageAdapter";
// @ts-ignore
import Select from "react-select";
import Delete from "src/pages/Admin/SuperAdminPages/images/Delete";
import Edit from "src/pages/Admin/SuperAdminPages/images/Edit";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { createNotification } from "src/helpers/crud-notifications";

interface RolePageStates {
  showRoleModal: boolean;
  showDeleteRoleModal: boolean;
  roleName: string;
  action: string;
  rolesList: Roles;
  roleId: number | string;
  permissionsList: Permissions;
  permissionIds: number[];
}

class RolePage extends PureComponent<{}, RolePageStates> {
  private adapter: RolePageAdapter;

  constructor(props: {}) {
    super(props);
    this.adapter = new RolePageAdapter();
    this.state = {
      showRoleModal: false,
      showDeleteRoleModal: false,
      roleName: "",
      rolesList: [{ id: 1, name: "", permissions: [] }],
      permissionsList: [{ id: 1, name: "" }],
      roleId: 0,
      action: "",
      permissionIds: [],
    };
  }

  componentDidMount(): void {
    this.setRolesList();
    this.setPermissionsList();
  }

  async setPermissionsList(): Promise<void> {
    const permissionsList = await this.adapter.getPermissionsList();
    this.setState({ permissionsList: permissionsList });
  }

  async setRolesList(): Promise<void> {
    const rolesList = await this.adapter.getRolesList();
    this.setState({ rolesList: rolesList });
  }

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <NotificationContainer />
        <div className={cn(classes.tableContent)}>
          <div className={cn(classes.tableHeader)}>
            <Typography variant="span">Роль</Typography>
            <Button
              variant="primary"
              onClick={() => {
                this.handleShowRoleModalClick("create", 0);
              }}
            >
              {" "}
              <Typography color="white" variant="span">
                Добавить
              </Typography>{" "}
            </Button>
          </div>
          <Table className={cn(classes.roleTable)} striped size="lg">
            <thead className={cn(classes.roleTableHeader)}>
              <tr>
                <th>
                  <Typography variant="span">#</Typography>
                </th>
                <th>
                  <Typography variant="span">Название</Typography>
                </th>
                <th>
                  <Typography variant="span">Пермишины</Typography>
                </th>
                <th>
                  <Typography variant="span">Действие</Typography>
                </th>
              </tr>
            </thead>
            <tbody>{this.getRolesList()}</tbody>
          </Table>
          {this.getRoleModal()}
          {this.getDeleteRoleModal()}
        </div>
      </main>
    );
  }

  getRolesList(): JSX.Element[] {
    return this.state.rolesList.map((role: Role, index: number) => {
      return (
        <tr key={role.id} className={cn(classes.roleTableRaw)}>
          <td>{index + 1}</td>
          <td>{role.name}</td>
          <td className={classes.permissionsRow}>
            {this.getPermissionsRows(role.permissions)}
          </td>
          <td>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                this.handleDeleteRoleClick(role.id);
              }}
            >
              {Delete()}
            </button>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                this.handleShowRoleModalClick("update", role.id);
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  }

  handleDeleteRoleClick = async (roleId: number): Promise<void> => {
    await this.setState({ showDeleteRoleModal: true });
    await this.setState({ roleId: roleId });
  };

  handleShowRoleModalClick = (action: string, roleId: number): void => {
    if (action == "update") {
      const role = this.state.rolesList.find((i) => i.id == roleId);
      if (role) {
        this.setState({ roleName: role.name });
        const permissionIds = [];
        for (let i = 0; i < role.permissions.length; i++) {
          permissionIds.push(role.permissions[i].id);
        }
        this.setState({ permissionIds: permissionIds });
      }
    } else {
      this.setState({ roleName: "" });
      this.setState({ permissionIds: [] });
    }
    this.setState({ action: action });
    this.setState({ roleId: roleId });
    this.setState({ showRoleModal: true });
  };

  getPermissionsRows(permissions: Permissions): any {
    const arrayLength = permissions.length;
    return permissions.map((permission: Permission, index: number) => {
      if (arrayLength == index + 1) {
        return permission.name;
      }

      return permission.name + ", ";
    });
  }

  getRoleModal(): JSX.Element {
    return (
      <Modal
        className={cn(classes.modalRoleDiolog)}
        show={this.state.showRoleModal}
        onHide={this.handleCloseRoleModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              {this.state.action == "create"
                ? "Добавить роль"
                : "Изменить роль"}
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <Form.Group>
              <Typography variant="label">Имя роли</Typography>
              <Form.Control
                type="text"
                value={this.state.roleName}
                onChange={this.setRoleName}
              />
            </Form.Group>
            <Form.Group>
              <Typography variant="label">Пермишины</Typography>
              <Select
                options={this.getRoleOptions()}
                isMulti
                onChange={this.setPermissionsId}
              />
            </Form.Group>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={
              this.state.action == "create"
                ? this.handleCreateRoleClick
                : this.handleEditRoleClick
            }
          >
            <Typography color="white" variant="span">
              {this.state.action == "create" ? "Сохранить" : "Обновить"}
            </Typography>
          </Button>
          <Button variant="primary" onClick={this.handleCloseRoleModalClick}>
            <Typography color="white" variant="span">
              Закрыть
            </Typography>
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  handleCloseRoleModalClick = (): void => {
    this.setState({ showRoleModal: false });
  };

  setRoleName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ roleName: event.target.value });
  };

  getRoleOptions(): { value: number; label: string }[] {
    const options = this.state.permissionsList.map((permisison: Permission) => {
      return { value: permisison.id, label: permisison.name };
    });
    return options;
  }

  setPermissionsId = async (
    selectedPermission: [{ value: number }]
  ): Promise<void> => {
    const options = selectedPermission;
    const selectedOptions = [];

    for (let i = 0; i < options.length; i++) {
      selectedOptions.push(options[i].value);
    }

    await this.setState({ permissionIds: selectedOptions });
  };

  handleCreateRoleClick = async (): Promise<void> => {
    this.setState({ showRoleModal: false });
    const response = await this.adapter.storeRole(
      this.state.roleName,
      this.state.permissionIds
    );

    if (response == "422") {
      createNotification(
        "error",
        "Роль не сохранена, не все данные были введены"
      );
    }

    if (response == "Role is saved") {
      this.setRolesList();
      createNotification("success", "Роль сохранена");
    }
  };

  handleEditRoleClick = async (): Promise<void> => {
    this.setState({ showRoleModal: false });
    const response = await this.adapter.updateRole(
      this.state.roleId,
      this.state.roleName,
      this.state.permissionIds
    );
    if (response == "Role is updated") {
      this.setRolesList();
      createNotification("success", "Роль обновлена");
    }
  };

  getDeleteRoleModal(): JSX.Element {
    return (
      <Modal
        className={cn(commonClasses.modalDiolog)}
        show={this.state.showDeleteRoleModal}
        onHide={this.handleRenouncementDeleteRoleModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              Вы уверены что хотите удалить выбранный пункт?
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="secondary"
            onClick={this.handleConfirmationDeleteRoleModalClick}
          >
            <Typography color="white" variant="span">
              Да
            </Typography>
          </Button>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="primary"
            onClick={this.handleRenouncementDeleteRoleModalClick}
          >
            <Typography color="white" variant="span">
              Нет
            </Typography>
          </Button>
        </Modal.Body>
      </Modal>
    );
  }

  handleConfirmationDeleteRoleModalClick = async (): Promise<void> => {
    this.setState({ showDeleteRoleModal: false });
    const response = await this.adapter.deleteRole(this.state.roleId);

    if (response == "Role is removed") {
      this.setRolesList();
      createNotification("error", "Пользователь удалён");
    }
  };

  handleRenouncementDeleteRoleModalClick = (): void => {
    this.setState({ showDeleteRoleModal: false });
  };
}

export default RolePage;
