import classes from "./VisitPurposePage.css";
import commonClasses from "../SuperAdminPages.css";
import cn from "clsx";
import React, { PureComponent } from "react";
import Typography from "src/components/Typography";
import {
  Button,
  Form,
  Modal,
  OverlayTrigger,
  Table,
  Tooltip,
} from "react-bootstrap";
import VisitPurposePageAdapter, {
  VisitPurposes,
  VisitPurpose,
  BranchOffices,
  BranchOffice,
  Services,
  Service,
} from "src/services/SuperAdminPagesServices/VisitPurposePageService/VisitPurposePageAdapter";
import Pagination, {
  PaginationData,
} from "src/components/Pagination/Pagination";
import Delete from "../images/Delete";
import Edit from "../images/Edit";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { createNotification } from "src/helpers/crud-notifications";
// @ts-ignore
import Select from "react-select";
import exclamationPoint from "src/pages/Admin/SuperAdminPages/VisitPurposePage/images/exclamation-point.svg";

interface VisitPurposePageStates {
  showVisitPurposeModal: boolean;
  showDeleteVisitPurposeModal: boolean;
  visitPurposeName: string;
  tjMessage: string;
  ruMessage: string;
  action: string;
  visitPurposesList: VisitPurposes;
  visitPurposeId: number | string;
  searchExpression: string;
  currentPage: number;
  pageLimit: number;
  paginationData: PaginationData;
  branchOfficeId: string;
  filterBranchOfficeId: string;
  servicesList: Services;
  branchOfficesList: BranchOffices;
  series: string;
  servicesIds: number[];
}

class VisitPurposePage extends PureComponent<{}, VisitPurposePageStates> {
  private adapter: VisitPurposePageAdapter;
  private pagesCountLimmit: Array<number>;

  constructor(props: {}) {
    super(props);
    this.adapter = new VisitPurposePageAdapter();
    this.pagesCountLimmit = [5, 10, 15, 20, 50, 100];

    this.state = {
      showVisitPurposeModal: false,
      showDeleteVisitPurposeModal: false,
      visitPurposeName: "",
      ruMessage: "",
      tjMessage: "",
      visitPurposesList: [
        {
          id: 0,
          name: "",
          series: "",
          services: [{ name: "" }],
          tjMessage: "",
          ruMessage: "",
        },
      ],
      visitPurposeId: 0,
      branchOfficeId: "",
      action: "",
      searchExpression: "",
      currentPage: 1,
      pageLimit: 5,
      paginationData: { currentPage: 0, totalPages: 0, totalRecords: 0 },
      servicesList: [{ id: 0, name: "", isActive: false }],
      branchOfficesList: [{ id: 0, branchOfficeName: "" }],
      series: "",
      filterBranchOfficeId: "",
      servicesIds: [],
    };
  }

  componentDidMount(): void {
    this.setVisitPurposesList();
    this.setBranchOfficesList();
    this.setServicesList();
  }

  async setBranchOfficesList(): Promise<void> {
    const branchOfficesList = await this.adapter.getBranchOfficesList();
    this.setState({ branchOfficesList: branchOfficesList });
  }

  async setServicesList(): Promise<void> {
    const servicesList = await this.adapter.getServicesList();
    this.setState({ servicesList: servicesList });
  }

  async setVisitPurposesList(currentPage: null | number = null): Promise<void> {
    const nextPage = currentPage ? currentPage : this.state.currentPage;
    const visitPurposesList = await this.adapter.getVisitPurposesList(
      this.state.filterBranchOfficeId,
      this.state.searchExpression,
      this.state.pageLimit,
      nextPage
    );
    const paginationData = this.adapter.getPaginationData();
    await this.setState({
      visitPurposesList: visitPurposesList,
      paginationData: paginationData,
    });
  }

  onPageChanged = async (page: number): Promise<void> => {
    await this.setState({ currentPage: page });
    await this.setVisitPurposesList();
  };

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <NotificationContainer />
        <div className={cn(classes.tableContent)}>
          <div className={cn(classes.filters)}>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.pageLimit}
                onChange={this.setPage}
                custom
              >
                {this.getPageOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.filterBranchOfficeId}
                onChange={this.setFilterBranchOfficeId}
                custom
              >
                <option value="0">Все филиалы</option>
                {this.getBranchOfficeOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                type="text"
                value={this.state.searchExpression}
                onChange={this.setSearchExpression}
              />
            </Form.Group>
            <Button variant="primary" onClick={this.filter}>
              <Typography color="white" variant="span">
                Фильтр
              </Typography>
            </Button>
            <Button
              variant="primary"
              onClick={() => {
                this.handleShowVisitPurposeModalClick("create", 0);
              }}
            >
              <Typography color="white" variant="span">
                Добавить
              </Typography>
            </Button>
          </div>
          <Table className={cn(classes.table)} striped size="lg">
            <thead className={cn(classes.tableHeader)}>
              <tr>
                <th>
                  <Typography variant="span">Название</Typography>
                </th>
                <th>
                  <Typography variant="span">Сервисы</Typography>
                </th>
                <th>
                  <Typography variant="span">Серия</Typography>
                </th>
                <th>
                  <Typography variant="span">Действие</Typography>
                </th>
              </tr>
            </thead>
            <tbody>{this.getVisitPurposesList()}</tbody>
          </Table>
          {this.getVisitPurposeModal()}
          {this.getDeleteVisitPurposeModal()}
          <div>
            {this.state.paginationData.totalPages != 0 ? (
              <Pagination
                totalPages={this.state.paginationData.totalPages}
                totalRecords={this.state.paginationData.totalRecords}
                onPageChanged={this.onPageChanged}
                currentPage={this.state.paginationData.currentPage}
              />
            ) : null}
          </div>
        </div>
      </main>
    );
  }

  setPage = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    this.setState({ pageLimit: Number(event.target.value) });
  };

  getPageOptions(): JSX.Element[] {
    return this.pagesCountLimmit.map((pageCount: number, index: number) => {
      return (
        <option key={index} value={pageCount}>
          {pageCount}
        </option>
      );
    });
  }

  setFilterBranchOfficeId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const filterBranchOfficeId = event.target.value;
    this.setState({ filterBranchOfficeId: filterBranchOfficeId });
  };

  getBranchOfficeOptions(): JSX.Element[] {
    return this.state.branchOfficesList.map(
      (branchOffice: BranchOffice, index: number) => {
        return (
          <option key={index} value={branchOffice.id}>
            {branchOffice.branchOfficeName}
          </option>
        );
      }
    );
  }

  getServiceOptions(): any {
    const options = this.state.servicesList.map((service: Service) => {
      return { value: service.id, label: service.name };
    });
    return options;
  }

  setServicesIds = async (
    selectedService: [{ value: number }]
  ): Promise<void> => {
    const options = selectedService;
    const selectedOptions = [];

    for (let i = 0; i < options.length; i++) {
      selectedOptions.push(options[i].value);
    }

    await this.setState({ servicesIds: selectedOptions });
  };

  setSearchExpression = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ searchExpression: event.target.value });
  };

  filter = (): void => {
    this.setVisitPurposesList(1);
  };

  handleShowVisitPurposeModalClick = (
    action: string,
    visitPurposeId: number
  ): void => {
    if (action == "update") {
      const visitPurpose = this.state.visitPurposesList.find(
        (i) => i.id == visitPurposeId
      );

      if (visitPurpose) {
        this.setState({ visitPurposeName: visitPurpose.name });
        this.setState({ series: visitPurpose.series });
        this.setState({ tjMessage: visitPurpose.tjMessage });
        this.setState({ ruMessage: visitPurpose.ruMessage });
      }
    } else {
      this.setState({ visitPurposeName: "" });
      this.setState({ series: "" });
      this.setState({ tjMessage: "" });
      this.setState({ ruMessage: "" });
    }

    this.setState({ action: action });
    this.setState({ visitPurposeId: visitPurposeId });
    this.setState({ showVisitPurposeModal: true });
  };

  getVisitPurposesList(): JSX.Element[] {
    return this.state.visitPurposesList.map((visitPurpose: VisitPurpose) => {
      return (
        <tr key={visitPurpose.id} className={cn(classes.tableRaw)}>
          <td>{visitPurpose.name}</td>
          <td>{this.getServicesRows(visitPurpose.services)}</td>
          <td>{visitPurpose.series}</td>
          <td>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                this.handleDeleteVisitPurposeClick(visitPurpose.id);
              }}
            >
              {Delete()}
            </button>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                this.handleShowVisitPurposeModalClick(
                  "update",
                  visitPurpose.id
                );
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  }

  getServicesRows(services: { name: string }[]): any {
    const arrayLength = services.length;
    return services.map((service: { name: string }, index: number) => {
      if (arrayLength == index + 1) {
        return service.name;
      }

      return service.name + ", ";
    });
  }

  handleDeleteVisitPurposeClick = async (
    visitPurposeId: number
  ): Promise<void> => {
    await this.setState({ showDeleteVisitPurposeModal: true });
    await this.setState({ visitPurposeId: visitPurposeId });
  };

  getVisitPurposeModal(): JSX.Element {
    return (
      <Modal
        className={cn(commonClasses.modalDiolog)}
        show={this.state.showVisitPurposeModal}
        onHide={this.handleCloseVisitPurposeModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              {this.state.action == "create"
                ? "Добавить цель визита"
                : "Изменить цель визита"}
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <Form.Group>
              <Typography variant="label">Имя цели визита</Typography>
              <Form.Control
                type="text"
                value={this.state.visitPurposeName}
                onChange={this.setVisitPurposeName}
              />
            </Form.Group>
            <Form.Group>
              <div className={cn(classes.labelRow)}>
                <Typography variant="label">Сообщение на таджикском</Typography>
                {this.instructionTooltip()}
              </div>
              <Form.Control
                type="text"
                value={this.state.tjMessage}
                onChange={this.setTjMessage}
              />
            </Form.Group>
            <Form.Group>
              <div className={cn(classes.labelRow)}>
                <Typography variant="label">Сообщение на русском</Typography>
                {this.instructionTooltip()}
              </div>
              <Form.Control
                type="text"
                value={this.state.ruMessage}
                onChange={this.setRuMessage}
              />
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Typography variant="label">Сервисы</Typography>
              <Select
                options={this.getServiceOptions()}
                isMulti
                onChange={this.setServicesIds}
              />
            </Form.Group>
            <Form.Group>
              <Typography variant="label">Серия</Typography>
              <Form.Control
                type="text"
                value={this.state.series}
                onChange={this.setSeries}
              />
            </Form.Group>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={
              this.state.action == "create"
                ? this.handleCreateVisitPurposeClick
                : this.handleEditVisitPurposeClick
            }
          >
            <Typography color="white" variant="span">
              {this.state.action == "create" ? "Сохранить" : "Обновить"}
            </Typography>
          </Button>
          <Button
            variant="primary"
            onClick={this.handleCloseVisitPurposeModalClick}
          >
            <Typography color="white" variant="span">
              Закрыть
            </Typography>
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
  instructionTooltip() {
    return (
      <OverlayTrigger
        placement="top"
        onEntering={this.entering}
        overlay={
          <Tooltip id="tooltip-top">
            {`desk_number - номерстолика \n ticket_number - номер очереди \n expert_name - имя экперта`}
          </Tooltip>
        }
      >
        {({ ref, ...triggerHandler }) => (
          <Button
            variant="transparent"
            {...triggerHandler}
            className={cn(
              classes.buttunExclamationPoint,
              "d-inline-flex align-items-center"
            )}
          >
            <img
              ref={ref}
              className={cn(classes.checkMark)}
              src={exclamationPoint}
            />
          </Button>
        )}
      </OverlayTrigger>
    );
  }

  entering = (e: any) => {
    e.children[1].style.padding = "0.25rem 1.5rem";
    e.children[1].style.maxWidth = "250px";
    e.children[1].style.textAlign = "left";
    e.children[1].style.borderRadius = "1.25rem";
    e.children[1].style.backgroundColor = "gray";
  };

  handleCloseVisitPurposeModalClick = (): void => {
    this.setState({ showVisitPurposeModal: false });
  };

  setBranchOfficeId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const branchOfficeId = event.target.value;
    this.setState({ branchOfficeId: branchOfficeId });
  };

  setVisitPurposeName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ visitPurposeName: event.target.value });
  };

  setTjMessage = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ tjMessage: event.target.value });
  };

  setRuMessage = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ ruMessage: event.target.value });
  };

  setVisitPurposeId = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const visitPurposeId = event.target.value;
    this.setState({ visitPurposeId: visitPurposeId });
    const visitPurpose = this.state.visitPurposesList.find(
      (i) => i.id == Number(visitPurposeId)
    );

    if (visitPurpose) {
      this.setState({ visitPurposeName: visitPurpose.name });
    }
  };

  setSeries = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ series: event.target.value });
  };

  handleCreateVisitPurposeClick = async (): Promise<void> => {
    this.setState({ showVisitPurposeModal: false });
    const response = await this.adapter.storeVisitPurpose(
      this.state.visitPurposeName,
      this.state.servicesIds,
      this.state.series,
      this.state.tjMessage,
      this.state.ruMessage
    );

    if (response == "SERIES_ERROR") {
      createNotification("error", "Цель с такой серией уже существует");
    }

    if (response == "422") {
      createNotification(
        "error",
        "Цель визита не сохранена, не все данные были введены"
      );
    }

    if (response == "Visit purpose is saved") {
      this.setVisitPurposesList();
      createNotification("success", "Цель визита сохранена");
    }
  };

  handleEditVisitPurposeClick = async (): Promise<void> => {
    this.setState({ showVisitPurposeModal: false });
    const response = await this.adapter.updateVisitPurpose(
      this.state.visitPurposeId,
      this.state.visitPurposeName,
      this.state.servicesIds,
      this.state.series,
      this.state.tjMessage,
      this.state.ruMessage
    );
    if (response == "Visit purpose is updated") {
      this.setVisitPurposesList();
      createNotification("success", "Цель визита обнавлёна");
    }
  };

  getDeleteVisitPurposeModal(): JSX.Element {
    return (
      <Modal
        className={cn(commonClasses.modalDiolog)}
        show={this.state.showDeleteVisitPurposeModal}
        onHide={this.handleRenouncementDeleteVisitPurposeModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              Вы уверены что хотите удалить выбранный пункт?
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="secondary"
            onClick={this.handleConfirmationDeleteVisitPurposeModalClick}
          >
            <Typography color="white" variant="span">
              Да
            </Typography>
          </Button>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="primary"
            onClick={this.handleRenouncementDeleteVisitPurposeModalClick}
          >
            <Typography color="white" variant="span">
              Нет
            </Typography>
          </Button>
        </Modal.Body>
      </Modal>
    );
  }

  handleConfirmationDeleteVisitPurposeModalClick = async (): Promise<void> => {
    this.setState({ showDeleteVisitPurposeModal: false });
    const response = await this.adapter.deleteVisitPurpose(
      this.state.visitPurposeId
    );

    if (response == "Visit purpose is removed") {
      this.setVisitPurposesList();
      createNotification("error", "Цель визита удалена");
    }
  };

  handleRenouncementDeleteVisitPurposeModalClick = (): void => {
    this.setState({ showDeleteVisitPurposeModal: false });
  };
}

export default VisitPurposePage;
