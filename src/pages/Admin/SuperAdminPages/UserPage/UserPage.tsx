import classes from "./UserPage.css";
import commonClasses from "../SuperAdminPages.css";
import cn from "clsx";
import React, { PureComponent } from "react";
import Typography from "src/components/Typography";
import { Button, Form, Modal, Table } from "react-bootstrap";
import UserPageAdapter, {
  BranchOffice,
  BranchOffices,
  Role,
  Roles,
  Town,
  Towns,
  User,
  Users,
  VisitPurpose,
  VisitPurposes,
} from "src/services/SuperAdminPagesServices/UserPageService/UserPageAdapter";
import Pagination, {
  PaginationData,
} from "src/components/Pagination/Pagination";
import Delete from "../images/Delete";
import Edit from "../images/Edit";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { createNotification } from "src/helpers/crud-notifications";
// @ts-ignore
import Select from "react-select";

interface UserPageState {
  usersList: Users;
  showUserModal: boolean;
  showDeleteUserModal: boolean;
  userId: number;
  townId: string;
  action: string;
  branchOfficeId: string;
  filterBranchOfficeId: string;
  branchOfficesList: BranchOffices;
  filteredBranchOfficesList: BranchOffices;
  townsList: Towns;
  visitPurposesList: VisitPurposes;
  visitPurposesIds: number[];
  rolesList: Roles;
  roleId: string;
  searchExpression: string;
  currentPage: number;
  pageLimit: number;
  paginationData: PaginationData;
  firstName: string;
  lastName: string;
  isActive: boolean;
  isAllowedHaveSameDeskNumber: boolean;
  email: string;
  password: string;
}

class UserPage extends PureComponent<{}, UserPageState> {
  private adapter: UserPageAdapter;
  private pagesCountLimmit: Array<number>;

  constructor(props: {}) {
    super(props);
    this.state = {
      showUserModal: false,
      showDeleteUserModal: false,
      action: "",
      userId: 0,
      townId: "",
      branchOfficeId: "",
      searchExpression: "",
      currentPage: 1,
      pageLimit: 5,
      townsList: [{ id: 0, name: "" }],
      branchOfficesList: [
        { id: 0, branchOfficeName: "", townId: 0, initial: "", townName: "" },
      ],
      filteredBranchOfficesList: [
        { id: 0, branchOfficeName: "", townId: 0, initial: "", townName: "" },
      ],
      usersList: [
        {
          id: 0,
          branchOfficeName: "",
          email: "",
          deskNumber: "",
          isActive: false,
          isAllowedHaveSameDeskNumber: false,
          fullName: "",
          firstName: "",
          lastName: "",
          createdAt: "",
          branchOfficeId: "",
          visitPurposesIds: [],
          role: { id: 0, name: "" },
        },
      ],
      paginationData: { currentPage: 0, totalPages: 0, totalRecords: 0 },
      firstName: "",
      lastName: "",
      isActive: true,
      isAllowedHaveSameDeskNumber: false,
      email: "",
      password: "",
      visitPurposesIds: [],
      visitPurposesList: [{ id: 0, name: "" }],
      rolesList: [{ id: 0, name: "" }],
      roleId: "",
      filterBranchOfficeId: "",
    };

    this.pagesCountLimmit = [5, 10, 15, 20, 50, 100];
    this.adapter = new UserPageAdapter();
  }

  componentDidMount(): void {
    this.setUsersList();
    this.setTownsList();
    this.setBranchOfficesList();
    this.setRolesList();
  }

  async setUsersList(currentPage: null | number = null): Promise<void> {
    const nextPage = currentPage ? currentPage : this.state.currentPage;
    const usersList = await this.adapter.getPaginatedUsersList(
      this.state.filterBranchOfficeId,
      this.state.searchExpression,
      this.state.pageLimit,
      nextPage
    );
    const paginationData = this.adapter.getPaginationData();
    await this.setState({
      usersList: usersList,
      paginationData: paginationData,
    });
  }

  async setTownsList(): Promise<void> {
    const townsList = await this.adapter.getTownsList();
    this.setState({ townsList: townsList });
  }

  async setBranchOfficesList(): Promise<void> {
    const branchOfficesList = await this.adapter.getBranchOfficesList();
    this.setState({ branchOfficesList: branchOfficesList });
  }

  async setRolesList(): Promise<void> {
    const rolesList = await this.adapter.getRolesList();
    this.setState({ rolesList: rolesList });
  }

  onPageChanged = async (page: number): Promise<void> => {
    await this.setState({ currentPage: page });
    await this.setUsersList();
  };

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <NotificationContainer />
        <div>
          <div className={cn(classes.filters)}>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.pageLimit}
                onChange={this.setPage}
                custom
              >
                {this.getPageOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.filterBranchOfficeId}
                onChange={this.setFilterBranchOfficeId}
                custom
              >
                <option value="0">Все филиалы</option>
                {this.getBranchOfficeOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                type="text"
                value={this.state.searchExpression}
                onChange={this.setSearchExpression}
              />
            </Form.Group>
            <Button variant="primary" onClick={this.filter}>
              <Typography color="white" variant="span">
                Фильтр
              </Typography>
            </Button>
            <Button
              variant="primary"
              onClick={() => {
                this.handleShowUserModalClick("create", 0);
              }}
            >
              <Typography color="white" variant="span">
                Добавить
              </Typography>
            </Button>
          </div>
          <div className={cn(classes.userContent)}>
            <Table className={cn(classes.userTable)} striped size="lg">
              <thead className={cn(classes.tableHeader)}>
                <tr>
                  <th>
                    <Typography variant="span">Имя</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Роль</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Филиал</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Окно</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Дата создания</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Статус</Typography>
                  </th>
                  <th>
                    <Typography variant="span">Действия</Typography>
                  </th>
                </tr>
              </thead>
              <tbody>{this.getPaginatedUsersList()}</tbody>
            </Table>
            {this.getUserModal()}
            {this.getDeleteUserModal()}
          </div>
        </div>
        <div>
          {this.state.paginationData.totalPages != 0 ? (
            <Pagination
              totalPages={this.state.paginationData.totalPages}
              totalRecords={this.state.paginationData.totalRecords}
              onPageChanged={this.onPageChanged}
              currentPage={this.state.paginationData.currentPage}
            />
          ) : null}
        </div>
      </main>
    );
  }

  getPageOptions(): JSX.Element[] {
    return this.pagesCountLimmit.map((pageCount: number, index: number) => {
      return (
        <option key={index} value={pageCount}>
          {pageCount}
        </option>
      );
    });
  }

  getTownOptions(): any {
    return this.state.townsList.map((town: Town, index: number) => {
      return (
        <option key={index} value={town.id}>
          {town.name}
        </option>
      );
    });
  }

  getFilteredBranchOfficeOptions(): any {
    return this.state.filteredBranchOfficesList.map(
      (branchOffice: BranchOffice, index: number) => {
        return (
          <option key={index} value={branchOffice.id}>
            {branchOffice.branchOfficeName}
          </option>
        );
      }
    );
  }

  getBranchOfficeOptions(): any {
    return this.state.branchOfficesList.map(
      (branchOffice: BranchOffice, index: number) => {
        return (
          <option key={index} value={branchOffice.id}>
            {branchOffice.branchOfficeName}
          </option>
        );
      }
    );
  }

  setSearchExpression = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ searchExpression: event.target.value });
  };

  setPage = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    this.setState({ pageLimit: Number(event.target.value) });
  };

  filter = (): void => {
    this.setUsersList(1);
  };

  setFilterBranchOfficeId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    await this.setState({ filterBranchOfficeId: event.target.value });
  };

  handleShowUserModalClick = async (
    action: string,
    userId: number
  ): Promise<void> => {
    if (action == "update") {
      const user = this.state.usersList.find((i) => i.id == userId);
      if (user) {
        await this.setState({
          userId: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          isActive: user.isActive,
          isAllowedHaveSameDeskNumber: user.isAllowedHaveSameDeskNumber,
          branchOfficeId: user.branchOfficeId,
          roleId: user.role.id.toString(),
        });

        const visitPurposesList = await this.adapter.getVisitPurposesList(
          user.branchOfficeId
        );
        await this.setState({
          visitPurposesList: visitPurposesList,
          visitPurposesIds: user.visitPurposesIds,
        });
        await this.setVisitPurposesIds;
      }
    } else {
      await this.setState({
        userId: 0,
        firstName: "",
        lastName: "",
        email: "",
        isActive: true,
        branchOfficeId: "",
        visitPurposesList: [{ id: 0, name: "" }],
        visitPurposesIds: [],
        roleId: "",
      });
    }

    await this.setState({ action: action });
    await this.setState({ showUserModal: true });
  };

  getPaginatedUsersList(): any {
    return this.state.usersList.map((user: User) => {
      return (
        <tr key={user.id} className={cn(classes.tableRaw)}>
          <td>{user.fullName}</td>
          <td>{user.role.name}</td>
          <td>{user.branchOfficeName}</td>
          <td>{user.deskNumber}</td>
          <td>{user.createdAt}</td>
          <td>
            <div>{user.isActive ? "Активен" : "Не активен"}</div>
          </td>
          <td>
            <div>
              <button
                className={cn(commonClasses.buttonAction)}
                type="button"
                onClick={() => {
                  this.handleDeleteUserClick(user.id);
                }}
              >
                {Delete()}
              </button>
              <button
                className={cn(commonClasses.buttonAction)}
                type="button"
                onClick={() => {
                  this.handleShowUserModalClick("update", user.id);
                }}
              >
                {Edit()}
              </button>
            </div>
          </td>
        </tr>
      );
    });
  }

  handleDeleteUserClick = async (userId: number): Promise<void> => {
    await this.setState({ showDeleteUserModal: true });
    await this.setState({ userId: userId });
  };

  getUserModal(): JSX.Element {
    return (
      <Modal
        className={cn(classes.modalPermissionDiolog)}
        show={this.state.showUserModal}
        onHide={this.handleCloseUserModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              {this.state.action == "create"
                ? "Добавить юзера"
                : "Изменить юзер"}
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <Form.Group className={cn(classes.formGroup)}>
              <Typography variant="label">Город</Typography>
              <Form.Control
                as="select"
                value={this.state.townId}
                onChange={this.setTownId}
                custom
              >
                <option value="0">Пожалуйста выберите город</option>
                {this.getTownOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Typography variant="label">Филиал</Typography>
              <Form.Control
                as="select"
                value={this.state.branchOfficeId}
                onChange={this.setBranchOfficeId}
                custom
              >
                <option value="0">Пожалуйста выберите филиал</option>
                {this.getFilteredBranchOfficeOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group>
              <Typography variant="label">Имя юзера</Typography>
              <Form.Control
                type="text"
                value={this.state.firstName}
                onChange={this.setFirstName}
              />
            </Form.Group>
            <Form.Group>
              <Typography variant="label">Фамилия юзера</Typography>
              <Form.Control
                type="text"
                value={this.state.lastName}
                onChange={this.setLastName}
              />
            </Form.Group>
            <Form.Group>
              <Typography variant="label">Email</Typography>
              <Form.Control
                type="email"
                value={this.state.email}
                onChange={this.setEmail}
              />
            </Form.Group>
            <Form.Group>
              <Typography variant="label">Пароль</Typography>
              <Form.Control
                type="text"
                value={this.state.password}
                onChange={this.setPassword}
              />
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Typography variant="label">Цель обслуживания</Typography>
              <Select
                options={this.getVisitPurposesOptions()}
                isMulti
                onChange={this.setVisitPurposesIds}
              />
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Typography variant="label">Роль</Typography>
              <Form.Control
                as="select"
                value={this.state.roleId}
                onChange={this.setRoleId}
                custom
              >
                <option value="0">Пожалуйста выберите роль юзера</option>
                {this.getRoleOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Check
                className={cn(classes.customControl)}
                type="switch"
                id="custom-switch"
                label="Активный?"
                checked={this.state.isActive}
                onChange={this.setIsActive}
              />
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Check
                className={cn(classes.customControl)}
                type="switch"
                id="is-allowed-have-same-desk-number"
                label="Позволить создавать одинаковые столы?"
                checked={this.state.isAllowedHaveSameDeskNumber}
                onChange={this.setIsAllowedHaveSameDeskNumber}
              />
            </Form.Group>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={
              this.state.action == "create"
                ? this.handleCreateUserClick
                : this.handleEditUserClick
            }
          >
            <Typography color="white" variant="span">
              {this.state.action == "create" ? "Сохранить" : "Обновить"}
            </Typography>
          </Button>
          <Button variant="primary" onClick={this.handleCloseUserModalClick}>
            <Typography color="white" variant="span">
              Закрыть
            </Typography>
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  handleCloseUserModalClick = (): void => {
    this.setState({ showUserModal: false });
  };

  setTownId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const townId = event.target.value;
    this.setState({ townId: townId });
    const filteredBranchOfficesList = await this.adapter.getVisitPurposesListFilteredByTownId(
      townId
    );
    this.setState({ filteredBranchOfficesList: filteredBranchOfficesList });
  };

  setBranchOfficeId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const branchOfficeId = event.target.value;
    this.setState({ branchOfficeId: branchOfficeId });
    const visitPurposesList = await this.adapter.getVisitPurposesList(
      branchOfficeId
    );
    this.setState({ visitPurposesList: visitPurposesList });
  };

  setFirstName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ firstName: event.target.value });
  };

  setLastName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ lastName: event.target.value });
  };

  setEmail = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ email: event.target.value });
  };

  setPassword = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ password: event.target.value });
  };

  getVisitPurposesOptions(): any {
    const options = this.state.visitPurposesList.map(
      (visitPurpose: VisitPurpose) => {
        return { value: visitPurpose.id, label: visitPurpose.name };
      }
    );
    return options;
  }

  setVisitPurposesIds = async (
    selectedVisitPurpose: [{ value: number }]
  ): Promise<void> => {
    const options = selectedVisitPurpose;
    const selectedOptions = [];

    for (let i = 0; i < options.length; i++) {
      selectedOptions.push(options[i].value);
    }

    await this.setState({ visitPurposesIds: selectedOptions });
  };

  setRoleId = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    this.setState({ roleId: event.target.value });
  };

  setIsActive = (): void => {
    if (this.state.isActive) {
      this.setState({ isActive: false });
    } else {
      this.setState({ isActive: true });
    }
  };

  setIsAllowedHaveSameDeskNumber = (): void => {
    if (this.state.isAllowedHaveSameDeskNumber) {
      this.setState({ isAllowedHaveSameDeskNumber: false });
    } else {
      this.setState({ isAllowedHaveSameDeskNumber: true });
    }
  };

  getRoleOptions(): JSX.Element[] {
    return this.state.rolesList.map((role: Role, index: number) => {
      return (
        <option key={index} value={role.id}>
          {role.name}
        </option>
      );
    });
  }

  handleCreateUserClick = async (): Promise<void> => {
    this.setState({ showUserModal: false });
    const data = {
      branchOfficeId: this.state.branchOfficeId,
      visitPurposesIds: this.state.visitPurposesIds,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      password: this.state.password,
      email: this.state.email,
      isActive: this.state.isActive,
      isAllowedHaveSameDeskNumber: this.state.isAllowedHaveSameDeskNumber,
      roleId: this.state.roleId,
    };
    const response = await this.adapter.storeUser(data);

    if (response == "EMAIL_ERROR") {
      createNotification("error", "Пользователь с таким email уже существует");
    }

    if (response == "422") {
      createNotification(
        "error",
        "Пользователь не сохранён, не все данные были введены"
      );
    }

    if (response == "User is saved") {
      this.setUsersList();
      createNotification("success", "Пользователь сохранён");
    }
  };

  handleEditUserClick = async (): Promise<void> => {
    this.setState({ showUserModal: false });
    const data = {
      branchOfficeId: this.state.branchOfficeId,
      visitPurposesIds: this.state.visitPurposesIds,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      password: this.state.password,
      email: this.state.email,
      isActive: this.state.isActive,
      isAllowedHaveSameDeskNumber: this.state.isAllowedHaveSameDeskNumber,
      roleId: this.state.roleId,
    };

    const response = await this.adapter.updateUser(this.state.userId, data);
    if (response == "EMAIL_ERROR") {
      createNotification("error", "Пользователь с таким email уже существует");
    }

    if (response == "422") {
      createNotification(
        "error",
        "Пользователь не сохранён, не все данные были введены"
      );
    }

    if (response == "User is updated") {
      this.setUsersList();
      createNotification("success", "Пользователь обновлён");
    }
  };

  getDeleteUserModal(): JSX.Element {
    return (
      <Modal
        className={cn(commonClasses.modalDiolog)}
        show={this.state.showDeleteUserModal}
        onHide={this.handleRenouncementDeleteUserModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">
              Вы уверены что хотите удалить выбранный пункт?
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="secondary"
            onClick={this.handleConfirmationDeleteUserModalClick}
          >
            <Typography color="white" variant="span">
              Да
            </Typography>
          </Button>
          <Button
            className={cn(commonClasses.modalDeleteActionButton)}
            variant="primary"
            onClick={this.handleRenouncementDeleteUserModalClick}
          >
            <Typography color="white" variant="span">
              Нет
            </Typography>
          </Button>
        </Modal.Body>
      </Modal>
    );
  }

  handleConfirmationDeleteUserModalClick = async (): Promise<void> => {
    this.setState({ showDeleteUserModal: false });
    const response = await this.adapter.deleteUser(this.state.userId);

    if (response == "User is removed") {
      this.setUsersList();
      createNotification("error", "Пользователь удалён");
    }
  };

  handleRenouncementDeleteUserModalClick = (): void => {
    this.setState({ showDeleteUserModal: false });
  };
}

export default UserPage;
