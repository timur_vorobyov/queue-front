import React from "react";
import cn from "clsx";
import commonClasses from "../ScreensPages.css";

const RightArrow = () => {
  return (
    <svg
      width="80px"
      height="80px"
      className={cn(commonClasses.arrow)}
      viewBox="0 0 64 64"
      aria-labelledby="title"
      aria-describedby="desc"
      role="img"
    >
      <path
        fill="#39b980"
        data-name="layer1"
        d="M38.879 47.121l13.993-13.993.012-.012L55 31 38.879 14.879a3 3 0 0 0-4.242 4.242l8.879 8.88H13.999a3 3 0 0 0 0 6h29.515l-8.877 8.878a3 3 0 0 0 4.242 4.242z"
      ></path>
    </svg>
  );
};

export default RightArrow;
