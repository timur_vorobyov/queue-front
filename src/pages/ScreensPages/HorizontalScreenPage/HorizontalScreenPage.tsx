import { PureComponent } from "react";
import ScreensPagesService, {
  AcceptedTicket,
  TicketPending,
  AcceptedTickets,
  TicketsPending,
} from "src/services/ScreensPagesService/ScreensPagesService";
import cn from "clsx";
import classes from "./HorizontalScreenPage.css";
import React from "react";
import Typography from "src/components/Typography";
import { createEchoInstance } from "src/helpers/echo";
import { Col, Row } from "react-bootstrap";
import RightArrow from "../images/RightArrow";
import { routes } from "src/config/routes";
import { Redirect } from "react-router-dom";
import tune from "../audio/queueSound.mp3";
import queryString from "query-string";

interface HorizontalScreenPageStates {
  ticketsPendingCards: TicketsPending;
  acceptedTicketsCards: AcceptedTickets;
  currentTime: string;
  redirect: string;
}

class HorizontalScreenPage extends PureComponent<
  {},
  HorizontalScreenPageStates
> {
  private echo: any;
  private service: ScreensPagesService;
  private audio: HTMLAudioElement;
  private branchOfficeId: string;
  private townId: string;
  private visitPurposeIds: Array<string>;

  constructor(props: {}) {
    super(props);
    this.service = new ScreensPagesService();
    const today = new Date();
    this.audio = new Audio(tune);
    this.state = {
      ticketsPendingCards: [],
      acceptedTicketsCards: [],
      currentTime:
        today.getHours() + ":" + ("0" + today.getMinutes()).slice(-2),
      redirect: "",
    };
    const parsed = queryString.parse(location.search);
    if (
      parsed.branchOfficeId &&
      parsed.townId &&
      parsed.visitPurposeIds &&
      !Array.isArray(parsed.branchOfficeId) &&
      !Array.isArray(parsed.townId) &&
      !Array.isArray(parsed.visitPurposeIds)
    ) {
      this.branchOfficeId = parsed.branchOfficeId;
      this.townId = parsed.townId;
      this.visitPurposeIds = parsed.visitPurposeIds.split(",");
    } else {
      this.branchOfficeId = "";
      this.townId = "";
      this.visitPurposeIds = [];
    }
  }

  componentDidMount(): void {
    setInterval(() => {
      this.run();
    }, 1000);
    if (!this.townId || !this.branchOfficeId) {
      this.setState({ redirect: routes.screens });
    } else {
      this.setCards();
      this.connectEcho();
      this.listenTicketsEvent();
    }
  }

  componentWillUnmount(): void {
    if (this.townId || this.branchOfficeId) {
      this.disconnectEcho();
    }
  }

  private connectEcho(): void {
    this.echo = createEchoInstance();
  }

  private disconnectEcho(): void {
    this.echo.disconnect();
  }

  private listenTicketsEvent(): void {
    this.visitPurposeIds.map((visitPurposeId: string) => {
      this.echo
        .channel(
          "ticket." +
            this.branchOfficeId +
            "." +
            visitPurposeId +
            ".pending.recover"
        )
        .listen(
          ".ticket-pending",
          (data: { shortTicketPending: TicketPending }): void => {
            const ticketsPendingCards = [
              ...this.state.ticketsPendingCards,
              data.shortTicketPending,
            ];
            this.setState({ ticketsPendingCards: ticketsPendingCards });
          }
        );

      this.echo
        .channel(
          "ticket." +
            this.branchOfficeId +
            "." +
            visitPurposeId +
            ".pending.add"
        )
        .listen(
          ".ticket-pending",
          (data: { shortTicketPending: TicketPending }): void => {
            const ticketsPendingCards = [
              ...this.state.ticketsPendingCards,
              data.shortTicketPending,
            ];
            const sortedTicketsPendingCards = ticketsPendingCards.sort(
              (x, y) => {
                if (x.isSalomCache === y.isSalomCache) return 0;
                if (x.isSalomCache) return -1;
                return 1;
              }
            );
            this.setState({ ticketsPendingCards: sortedTicketsPendingCards });
          }
        );

      this.echo
        .channel(
          "ticket.all." +
            this.branchOfficeId +
            "." +
            visitPurposeId +
            ".accepted.add"
        )
        .listen(
          ".ticket-accepted",
          (data: { acceptedTicket: AcceptedTicket }): void => {
            const acceptedTicketsCards = [
              ...this.state.acceptedTicketsCards,
              data.acceptedTicket,
            ];
            this.audio.play();
            this.setState({ acceptedTicketsCards: acceptedTicketsCards });
          }
        );

      this.echo
        .channel(
          "ticket." +
            this.branchOfficeId +
            "." +
            visitPurposeId +
            ".pending.delete"
        )
        .listen(
          ".ticket-pending",
          async (data: {
            shortTicketPending: TicketPending;
          }): Promise<void> => {
            const index = Object.values(
              this.state.ticketsPendingCards
            ).findIndex((value) => value.id === data.shortTicketPending.id);
            if (index != -1) {
              this.state.ticketsPendingCards.splice(index, 1);
              const ticketsPendingCards = [...this.state.ticketsPendingCards];
              await this.setState({ ticketsPendingCards: ticketsPendingCards });
            }
            const ticketsPendingCards = [...this.state.ticketsPendingCards];
            const sortedTicketsPendingCards = ticketsPendingCards.sort(
              (x, y) => {
                if (x.isSalomCache === y.isSalomCache) return 0;
                if (x.isSalomCache) return -1;
                return 1;
              }
            );
            this.setState({ ticketsPendingCards: sortedTicketsPendingCards });
          }
        );

      this.echo
        .channel(
          "ticket.all." +
            this.branchOfficeId +
            "." +
            visitPurposeId +
            ".accepted.delete"
        )
        .listen(
          ".ticket-accepted",
          async (data: {
            shortAcceptedTicket: TicketPending;
          }): Promise<void> => {
            const index = Object.values(
              this.state.acceptedTicketsCards
            ).findIndex((value) => value.id === data.shortAcceptedTicket.id);
            if (index != -1) {
              this.state.acceptedTicketsCards.splice(index, 1);
              const acceptedTicketsCards = [...this.state.acceptedTicketsCards];
              await this.setState({
                acceptedTicketsCards: acceptedTicketsCards,
              });
            }
          }
        );
    });
  }

  getTicketsPendingCards(): JSX.Element[] {
    return this.state.ticketsPendingCards
      .slice(0, 9)
      .map((ticketPending: TicketPending, index: number) => {
        return (
          <div key={index} className={cn(classes.ticketCard)}>
            <Typography variant="span">
              {ticketPending.ticketFullNumber}
            </Typography>
          </div>
        );
      });
  }

  setCards = async (): Promise<void> => {
    const acceptedTicketsList = await this.service.getAcceptedTicketsList(
      this.branchOfficeId,
      this.visitPurposeIds
    );
    const ticketsPendingList = await this.service.getTicketsPendingList(
      this.branchOfficeId,
      this.visitPurposeIds
    );

    await this.setState({
      ticketsPendingCards: ticketsPendingList,
      acceptedTicketsCards: acceptedTicketsList,
    });
  };

  getEvenAcceptedTicketsCards(): any {
    return this.state.acceptedTicketsCards
      .slice(0, 6)
      .map((acceptedTicket: AcceptedTicket, index: number) => {
        if (index % 2 == 0) {
          return (
            <div key={index} className={cn(classes.ticketInfo)}>
              <Typography variant="span">
                {acceptedTicket.ticketFullNumber}
              </Typography>
              <div className={cn(classes.arrow)}>
                <RightArrow />
              </div>
              <Typography variant="span">
                {acceptedTicket.deskNumber}
              </Typography>
            </div>
          );
        } else {
          return null;
        }
      });
  }

  getOddAcceptedTicketsCards(): any {
    return this.state.acceptedTicketsCards
      .slice(0, 6)
      .map((acceptedTicket: AcceptedTicket, index: number) => {
        if (index % 2 != 0) {
          return (
            <div key={index} className={cn(classes.ticketInfo)}>
              <Typography variant="span">
                {acceptedTicket.ticketFullNumber}
              </Typography>
              <div className={cn(classes.arrow)}>
                <RightArrow />
              </div>
              <Typography variant="span">
                {acceptedTicket.deskNumber}
              </Typography>
            </div>
          );
        } else {
          return null;
        }
      });
  }

  run = () => {
    const today = new Date();
    const time = today.getHours() + ":" + ("0" + today.getMinutes()).slice(-2);
    this.setState({ currentTime: time });
  };

  render(): React.ReactNode {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <main className={cn(classes.container)}>
        <Row className={cn(classes.containerRow)}>
          <Col lg={10} className={cn(classes.leftSide)}>
            <div className={cn(classes.leftSideHeader)}>
              <Typography fontWeight="bold" variant="h1">
                Навбати электронӣ
              </Typography>
              <Typography fontWeight="bold" variant="h1">
                {this.state.currentTime}
              </Typography>
            </div>
            <Row className={cn(classes.ticketsListRow)}>
              <Col lg={6}>
                <div className={cn(classes.ticketsListHeader)}>
                  <span className={cn(classes.ticketListHeader)}>Мизоҷ</span>
                  <span className={cn(classes.ticketListHeader)}>Миз</span>
                </div>
                {this.state.acceptedTicketsCards.length > 0
                  ? this.getEvenAcceptedTicketsCards()
                  : null}
              </Col>
              <Col lg={6}>
                <div className={cn(classes.ticketsListHeader)}>
                  <span className={cn(classes.ticketListHeader)}>Мизоҷ</span>
                  <span className={cn(classes.ticketListHeader)}>Миз</span>
                </div>
                {this.state.ticketsPendingCards.length > 0
                  ? this.getOddAcceptedTicketsCards()
                  : null}
              </Col>
            </Row>
          </Col>
          <Col lg={2} className={cn(classes.rightSide)}>
            <div className={cn(classes.rightSideHeader)}>
              <Typography fontWeight="bold" color="black" variant="h3">
                Дар навбат
              </Typography>
            </div>
            {this.getTicketsPendingCards()}
          </Col>
        </Row>
      </main>
    );
  }
}

export default HorizontalScreenPage;
