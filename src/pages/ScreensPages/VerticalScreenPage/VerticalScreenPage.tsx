import { PureComponent } from "react";
import ScreensPagesService, {
  AcceptedTicket,
  TicketPending,
  AcceptedTickets,
  TicketsPending,
} from "src/services/ScreensPagesService/ScreensPagesService";
import cn from "clsx";
import classes from "./VerticalScreenPage.css";
import React from "react";
import Typography from "src/components/Typography";
import { createEchoInstance } from "src/helpers/echo";
import RightArrow from "../images/RightArrow";
import { routes } from "src/config/routes";
import { Redirect } from "react-router-dom";
import tune from "../audio/queueSound.mp3";
import queryString from "query-string";

interface VerticalScreenPageStates {
  ticketsPendingCards: TicketsPending;
  acceptedTicketsCards: AcceptedTickets;
  currentTime: string;
  redirect: string;
}

class VerticalScreenPage extends PureComponent<{}, VerticalScreenPageStates> {
  private echo: any;
  private service: ScreensPagesService;
  private location: string;
  private audio: HTMLAudioElement;
  private branchOfficeId: string;
  private townId: string;
  private visitPurposeIds: Array<string>;

  constructor(props: {}) {
    super(props);
    this.service = new ScreensPagesService();
    const today = new Date();
    this.audio = new Audio(tune);
    const time = today.getHours() + ":" + today.getMinutes();
    this.state = {
      ticketsPendingCards: [],
      acceptedTicketsCards: [],
      currentTime: time.toLocaleString(),
      redirect: "",
    };
    this.location = window.location.pathname;
    const parsed = queryString.parse(location.search);

    if (
      parsed.branchOfficeId &&
      parsed.townId &&
      parsed.visitPurposeIds &&
      !Array.isArray(parsed.branchOfficeId) &&
      !Array.isArray(parsed.townId) &&
      !Array.isArray(parsed.visitPurposeIds)
    ) {
      this.branchOfficeId = parsed.branchOfficeId;
      this.townId = parsed.townId;
      this.visitPurposeIds = parsed.visitPurposeIds.split(",");
    } else {
      this.branchOfficeId = "";
      this.townId = "";
      this.visitPurposeIds = [];
    }
  }

  componentDidMount(): void {
    this.setCards();
    this.connectEcho();
    this.listenTicketsEvent();
    if (!this.townId || !this.branchOfficeId) {
      this.setState({ redirect: routes.screens });
    }
  }

  componentWillUnmount(): void {
    this.disconnectEcho();
  }

  private connectEcho(): void {
    this.echo = createEchoInstance();
  }

  private disconnectEcho(): void {
    this.echo.disconnect();
  }

  private listenTicketsEvent(): void {
    this.visitPurposeIds.map((visitPurposeId: string) => {
      this.echo
        .channel(
          "ticket." +
            this.branchOfficeId +
            "." +
            visitPurposeId +
            ".pending.recover"
        )
        .listen(
          ".ticket-pending",
          (data: { shortTicketPending: TicketPending }): void => {
            const ticketsPendingCards = [
              ...this.state.ticketsPendingCards,
              data.shortTicketPending,
            ];
            const sortedTicketsPendingCards = ticketsPendingCards.sort(
              (x, y) => {
                if (x.isSalomCache === y.isSalomCache) return 0;
                if (x.isSalomCache) return -1;
                return 1;
              }
            );
            this.setState({ ticketsPendingCards: sortedTicketsPendingCards });
          }
        );

      this.echo
        .channel(
          "ticket." +
            this.branchOfficeId +
            "." +
            visitPurposeId +
            ".pending.add"
        )
        .listen(
          ".ticket-pending",
          (data: { shortTicketPending: TicketPending }): void => {
            const ticketsPendingCards = [
              ...this.state.ticketsPendingCards,
              data.shortTicketPending,
            ];
            const sortedTicketsPendingCards = ticketsPendingCards.sort(
              (x, y) => {
                if (x.isSalomCache === y.isSalomCache) return 0;
                if (x.isSalomCache) return -1;
                return 1;
              }
            );
            this.setState({ ticketsPendingCards: sortedTicketsPendingCards });
          }
        );

      this.echo
        .channel(
          "ticket.all." +
            this.branchOfficeId +
            "." +
            visitPurposeId +
            ".accepted.add"
        )
        .listen(
          ".ticket-accepted",
          (data: { acceptedTicket: AcceptedTicket }): void => {
            const acceptedTicketsCards = [
              ...this.state.acceptedTicketsCards,
              data.acceptedTicket,
            ];
            this.audio.play();
            this.setState({ acceptedTicketsCards: acceptedTicketsCards });
          }
        );

      this.echo
        .channel(
          "ticket." +
            this.branchOfficeId +
            "." +
            visitPurposeId +
            ".pending.delete"
        )
        .listen(
          ".ticket-pending",
          async (data: {
            shortTicketPending: TicketPending;
          }): Promise<void> => {
            const index = Object.values(
              this.state.ticketsPendingCards
            ).findIndex((value) => value.id === data.shortTicketPending.id);
            if (index != -1) {
              this.state.ticketsPendingCards.splice(index, 1);
              const ticketsPendingCards = [...this.state.ticketsPendingCards];
              await this.setState({ ticketsPendingCards: ticketsPendingCards });
            }
            const ticketsPendingCards = [...this.state.ticketsPendingCards];
            const sortedTicketsPendingCards = ticketsPendingCards.sort(
              (x, y) => {
                if (x.isSalomCache === y.isSalomCache) return 0;
                if (x.isSalomCache) return -1;
                return 1;
              }
            );
            this.setState({ ticketsPendingCards: sortedTicketsPendingCards });
          }
        );

      this.echo
        .channel(
          "ticket.all." +
            this.branchOfficeId +
            "." +
            visitPurposeId +
            ".accepted.delete"
        )
        .listen(
          ".ticket-accepted",
          (data: { shortAcceptedTicket: TicketPending }): void => {
            const index = Object.values(
              this.state.acceptedTicketsCards
            ).findIndex((value) => value.id === data.shortAcceptedTicket.id);
            if (index != -1) {
              this.state.acceptedTicketsCards.splice(index, 1);
            }
            const acceptedTicketsCards = [...this.state.acceptedTicketsCards];
            this.setState({ acceptedTicketsCards: acceptedTicketsCards });
          }
        );
    });
  }

  setCards = async (): Promise<void> => {
    const acceptedTicketsList = await this.service.getAcceptedTicketsList(
      this.branchOfficeId,
      this.visitPurposeIds
    );
    const ticketsPendingList = await this.service.getTicketsPendingList(
      this.branchOfficeId,
      this.visitPurposeIds
    );

    await this.setState({
      ticketsPendingCards: ticketsPendingList,
      acceptedTicketsCards: acceptedTicketsList,
    });
  };

  getTicketsPendingCards(): JSX.Element[] {
    return this.state.ticketsPendingCards
      .slice(0, 4)
      .map((ticketPending: TicketPending, index: number) => {
        return (
          <div key={index} className={cn(classes.ticketCard)}>
            <span
              className={cn([
                this.location == "/led-white" ? classes.black : classes.white,
                classes.tvTicketsPendingData,
              ])}
            >
              {index + 1 == this.state.ticketsPendingCards.length
                ? ticketPending.ticketFullNumber
                : ticketPending.ticketFullNumber + ", "}
            </span>
          </div>
        );
      });
  }

  getAcceptedTicketsCards(): JSX.Element[] {
    return this.state.acceptedTicketsCards.map(
      (acceptedTicket: AcceptedTicket, index: number) => {
        return (
          <div
            key={index}
            className={cn(
              this.location == "/led-white"
                ? classes.whiteThemeTicketInfo
                : classes.ticketInfo
            )}
          >
            <span
              className={cn([
                this.location == "/led-white" ? classes.black : classes.white,
                classes.tvAcceptedTicketsData,
              ])}
            >
              {acceptedTicket.ticketFullNumber}
            </span>
            <div className={cn(classes.arrow)}>
              <RightArrow />
            </div>
            <span
              className={cn([
                this.location == "/led-white" ? classes.black : classes.white,
                classes.tvAcceptedTicketsData,
              ])}
            >
              {acceptedTicket.deskNumber}
            </span>
          </div>
        );
      }
    );
  }

  render(): React.ReactNode {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <main
        className={cn(
          this.location == "/led-white"
            ? classes.whiteThemeContainer
            : classes.container
        )}
      >
        <div className={cn(classes.content)}>
          <div>
            <div className={cn(classes.ticketsListRow)}>
              <div className={cn(classes.ticketsListHeader)}>
                <Typography
                  variant="span"
                  color={this.location == "/led-white" ? "black" : "white"}
                  className="white"
                >
                  Мизоҷ
                </Typography>
                <Typography
                  variant="span"
                  color={this.location == "/led-white" ? "black" : "white"}
                  className="white"
                >
                  Миз
                </Typography>
              </div>
              {this.state.acceptedTicketsCards.length > 0
                ? this.getAcceptedTicketsCards()
                : null}
            </div>
          </div>
          <div>
            <div
              className={cn(
                this.location == "/led-white"
                  ? classes.whiteThemeBottomSide
                  : classes.bottomSide
              )}
            >
              <span
                className={cn([
                  this.location == "/led-white" ? classes.black : classes.white,
                  classes.tvTicketsPendingDataHeader,
                ])}
              >
                ОЯНДА &nbsp;
              </span>
              {this.state.acceptedTicketsCards.length > 0
                ? this.getTicketsPendingCards()
                : null}
            </div>
          </div>
        </div>
      </main>
    );
  }
}

export default VerticalScreenPage;
