import React, { PureComponent } from "react";
import classes from "./NotFound.css";
import cn from "clsx";
import { Redirect } from "react-router-dom";
import { Button } from "react-bootstrap";
import NotFoundImage from "./images/NotFound";
interface NotFoundStates {
  redirect: string;
}

class NotFound extends PureComponent<{}, NotFoundStates> {
  constructor(props: {}) {
    super(props);
    this.state = { redirect: "" };
  }

  handleHomeRedirectClick = () => {
    this.setState({ redirect: "/" });
  };

  render(): React.ReactNode {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <main className={cn(classes.container)}>
        <div className={cn(classes.errorContainer)}>
          <div className={cn(classes.error)}>
            <span className={cn(classes.errorHeader)}>404</span>
            <span className={cn(classes.errorDef)}>
              Извините, но страница не найдена
            </span>
            <Button
              className={cn(classes.button)}
              onClick={this.handleHomeRedirectClick}
              variant="info"
            >
              Вернуться домой
            </Button>
          </div>
        </div>
        <div className={cn(classes.errorPageBack)}>{NotFoundImage()}</div>
      </main>
    );
  }
}

export default NotFound;
