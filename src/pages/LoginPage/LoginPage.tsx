import { useContext, useState } from "react";
import cn from "clsx";
import classes from "./LoginPage.css";
import React from "react";
import { Button, Form } from "react-bootstrap";
import Typography from "src/components/Typography";
import { UserDataContext } from "src/contexts/UserDataContext";
import { Redirect } from "react-router-dom";
import { routes } from "src/config/routes";
// @ts-ignore
import { NotificationContainer } from "react-notifications";

function LoginPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const userData = useContext(UserDataContext);

  if (userData.isAuth) {
    return <Redirect to={routes.queueService} />;
  }

  const handleSubmitClick = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    userData.fetchUserData(email, password);
  };

  const setEmailState = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };

  const setPasswordState = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.loginFormContainer)}>
        <Typography variant="h4">Вход</Typography>
        <Typography variant="p">Пожалуйста войдите в свой аккаунт.</Typography>
        <Form onSubmit={handleSubmitClick}>
          <Form.Group controlId="formBasicEmail">
            <Typography variant="label">Email</Typography>
            <Form.Control type="email" value={email} onChange={setEmailState} />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Typography variant="label">Password</Typography>
            <Form.Control
              type="password"
              value={password}
              onChange={setPasswordState}
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Войти
          </Button>
        </Form>
      </div>
    </main>
  );
}

export default LoginPage;
