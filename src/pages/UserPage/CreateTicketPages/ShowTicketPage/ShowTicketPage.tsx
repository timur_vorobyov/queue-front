import React from "react";
import { PureComponent } from "react";
import { Redirect } from "react-router-dom";
import cn from "clsx";
import classes from "./ShowTicketPage.css";
import { routes } from "src/config/routes";
import CreateTicketPagesService from "src/services/CreateTicketPagesService/CreateTicketPagesService";
import ticketSsavedImg from "src/pages/UserPage/CreateTicketPages/images/ticket-saved.svg";
import { T } from "react-targem";
interface ShowTicketPageStates {
  redirect: string;
  ticketFullNumber: string;
}

class ShowTicketPage extends PureComponent<{}, ShowTicketPageStates> {
  private service: CreateTicketPagesService;

  constructor(props: {}) {
    super(props);
    this.state = {
      redirect: "",
      ticketFullNumber: "",
    };
    this.service = new CreateTicketPagesService();
  }

  async componentDidMount(): Promise<void> {
    const response = await this.service.createTicket();
    this.setState({ ticketFullNumber: response });
    setTimeout(() => {
      this.setState({ redirect: routes.registerTicket });
    }, 5000);
  }

  render(): React.ReactNode {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }

    if (this.state.ticketFullNumber != "") {
      return (
        <main className={cn(classes.container)}>
          <div className={cn(classes.ticket)}>
            <div className={cn(classes.ticketContent)}>
              <img className={cn(classes.checkMark)} src={ticketSsavedImg} />
              <span className={cn(classes.header)}>
                <T message="Спасибо" />
              </span>
              <div className={cn(classes.ticketInfoHeader)}>
                <span className={cn(classes.subheader)}>
                  <T message="Номер вашей очереди отправлен вам в виде СМС на телефон" />
                </span>
                <span className={cn(classes.subheader)}>
                  <T message="Проверьте ваш телефон, пожалуйста" />
                </span>
              </div>
              <div className={cn(classes.ticketInfoContent)}>
                <span className={cn(classes.ticketInfo)}>
                  <T message="Ваш номер очереди" />
                </span>
                <div className={cn(classes.ticketInfo)}>
                  <span className={cn(classes.ticketNumber)}>
                    {this.state.ticketFullNumber}
                  </span>
                </div>
                <span className={cn(classes.ticketInfo)}>
                  <T message="Не забудьте" />
                </span>
              </div>
            </div>
            <div className={cn(classes.ticketFooter)}></div>
          </div>
        </main>
      );
    } else {
      return null;
    }
  }
}

export default ShowTicketPage;
