import React from "react";
import { PureComponent } from "react";
import cn from "clsx";
import classes from "./CreateTicketPage.css";
import { Redirect } from "react-router-dom";
import { Button } from "react-bootstrap";
import { routes } from "src/config/routes";
import { T, WithLocale, withLocale } from "react-targem";
import { setIsRegisteredTicketValue } from "src/redux/CreateTicket/actions";
import store from "src/redux/CreateTicket";
import logImg from "src/pages/UserPage/CreateTicketPages/images/logo-alif.svg";
import tjFlag from "src/pages/UserPage/CreateTicketPages/images/tj-flag.svg";
import ruFlag from "src/pages/UserPage/CreateTicketPages/images/ru-flag.svg";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
interface CreateTicketPageStates {
  redirect: string;
}

interface CreateTicketPageProps extends WithLocale {
  changeLocale: (locale: string) => {};
}

class CreateTicketPage extends PureComponent<
  CreateTicketPageProps,
  CreateTicketPageStates
> {
  constructor(props: CreateTicketPageProps) {
    super(props);
    this.state = { redirect: "" };
  }

  handleChangeLocationButton(locale: string) {
    this.props.changeLocale(locale);
  }

  getDesiredLocaleButton() {
    if (this.props.locale == "tj") {
      return (
        <button
          className={cn("btn", classes.btnTranslator)}
          onClick={() => {
            this.handleChangeLocationButton("ru");
          }}
        >
          <div className={cn(classes.buttonContent)}>
            <img className={cn(classes.ruFlag)} src={ruFlag} />
            <span className={cn(classes.btnTranslatorText)}>Русский</span>
          </div>
        </button>
      );
    } else {
      return (
        <button
          className={cn("btn", classes.btnTranslator)}
          onClick={() => {
            this.handleChangeLocationButton("tj");
          }}
        >
          <div className={cn(classes.buttonContent)}>
            <img className={cn(classes.tjFlag)} src={tjFlag} />
            <span className={cn(classes.btnTranslatorText)}>Тоҷикӣ</span>
          </div>
        </button>
      );
    }
  }

  handleCreateTicketClick = (): void => {
    store.dispatch(setIsRegisteredTicketValue(true));
    this.setState({ redirect: routes.ticketVisitPurpose });
  };

  render(): React.ReactNode {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <main className={cn(classes.container)}>
        <NotificationContainer />
        <div className={cn(classes.description)}>
          <img className={cn(classes.logo)} src={logImg} />
          <span className={cn(classes.header)}>
            <T message="Добро пожаловать" />
          </span>
          <span className={cn(classes.header)}>
            <T message="Нажмите, чтобы получить очередь" />
          </span>
        </div>
        <div className={cn(classes.actions)}>
          <Button
            variant="secondary"
            type="button"
            onClick={this.handleCreateTicketClick}
            className={cn(classes.getTicketButton)}
          >
            <span className={cn(classes.btnText)}>
              <T message="Получить очередь" />
            </span>
          </Button>
          {this.getDesiredLocaleButton()}
        </div>
      </main>
    );
  }
}

export default withLocale(CreateTicketPage);
