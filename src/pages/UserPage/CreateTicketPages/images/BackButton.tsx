import React from "react";
import cn from "clsx";
import classes from "../CustomerPhoneNumberPage/CustomerPhoneNumberPage.css";

const BackButton = () => {
  return (
    <svg
      width="187"
      height="96"
      viewBox="0 0 187 96"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        className={cn(classes.redirectBackButtonImg)}
        d="M184.89 86C184.89 90.4183 181.308 94 176.89 94L42.2483 94C39.9269 94 37.7201 92.9917 36.2005 91.2368L4.06113 54.1205C1.4998 51.1626 1.45387 46.786 3.95253 43.7749L36.2194 4.89126C37.7393 3.05972 39.9957 2 42.3758 2L176.89 2C181.308 2 184.89 5.58172 184.89 10V86Z"
        fill="white"
        stroke="#F1F4F5"
        strokeWidth="4"
      />
    </svg>
  );
};

export default BackButton;
