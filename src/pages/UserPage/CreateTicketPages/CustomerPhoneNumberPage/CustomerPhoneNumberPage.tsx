import React from "react";
import { PureComponent } from "react";
import { Alert, Button } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import Typography from "src/components/Typography";
import cn from "clsx";
import classes from "./CustomerPhoneNumberPage.css";
import store from "src/redux/CreateTicket";
import { setCustomerPhoneNumberValue } from "src/redux/CreateTicket/actions";
import { routes } from "src/config/routes";
// @ts-ignore
import InputMask from "react-input-mask";
import { T, WithLocale, withLocale } from "react-targem";
import removeNumberImg from "src/pages/UserPage/CreateTicketPages/images/remove-number.svg";
import { createNotification } from "src/helpers/crud-notifications";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import BackButton from "../images/BackButton";
import SentButton from "../images/SentButton";

interface CustomerPhoneNumberPageStates {
  redirect: string;
  customerPhoneNumber: string;
  showAlert: boolean;
  disabled: boolean;
}

interface CustomerPhoneNumberPageProps extends WithLocale {}

class CustomerPhoneNumberPage extends PureComponent<
  CustomerPhoneNumberPageProps,
  CustomerPhoneNumberPageStates
> {
  private timer: NodeJS.Timeout;

  constructor(props: CustomerPhoneNumberPageProps) {
    super(props);
    this.state = {
      showAlert: false,
      customerPhoneNumber: "",
      redirect: "",
      disabled: true,
    };
    this.timer = setTimeout(() => {
      this.setState({ redirect: routes.registerTicket });
    }, 60000);
  }

  componentDidMount(): void {
    if (!store.getState().visitPurposeId) {
      this.setState({ redirect: routes.registerTicket });
    }
  }

  setCustomerPhoneNumber = async (
    event: React.ChangeEvent<HTMLInputElement>
  ): Promise<void> => {
    await this.setState({ customerPhoneNumber: event.target.value.slice(6) });
    if (this.state.customerPhoneNumber.length == 12) {
      await this.setState({ disabled: false });
    } else {
      await this.setState({ disabled: true });
    }
  };

  handleAddNumberClick = async (number: string): Promise<void> => {
    if (this.state.customerPhoneNumber.length != 10) {
      if (this.state.customerPhoneNumber == "") {
        /**it is impossible to set the first number the same as the non-editable characters in the mask, without space ' ' before the first number*/
        const customerPhoneNumber = " " + number;
        await this.setState({ customerPhoneNumber: customerPhoneNumber });
      } else {
        const customerPhoneNumber = this.state.customerPhoneNumber + number;
        await this.setState({ customerPhoneNumber: customerPhoneNumber });
        await this.setState({ disabled: true });
      }
    }

    if (this.state.customerPhoneNumber.length == 10) {
      const customerPhoneNumber = this.state.customerPhoneNumber.replace(
        /\s+/g,
        ""
      );
      const regexp = /^(00|11|90|91|92|93|98|50|55|88|77|99|94|70|80|20)\d{7}$/;
      if (regexp.test(customerPhoneNumber)) {
        await this.setState({ disabled: false });
      } else {
        createNotification("error", "Номер не соответствует требованиям");
      }
    }
  };

  handleRemoveNumberClick = () => {
    const customerPhoneNumber = this.state.customerPhoneNumber.slice(0, -1);
    this.setState({ customerPhoneNumber: customerPhoneNumber });
    this.setState({ disabled: true });
  };

  handleReturnBackButtonClick = () => {
    clearTimeout(this.timer);
    this.setState({ redirect: routes.ticketVisitPurpose });
  };

  handleGetTicketButtonClick = () => {
    clearTimeout(this.timer);
    store.dispatch(setCustomerPhoneNumberValue(this.state.customerPhoneNumber));
    const customerPhoneLength = this.state.customerPhoneNumber.replace(
      /\s+/g,
      ""
    ).length;
    if (customerPhoneLength != 9 && customerPhoneLength != 0) {
      this.setState({ showAlert: true });
    } else {
      this.setState({ redirect: routes.showTicket });
    }
  };

  render(): React.ReactNode {
    if (this.state.redirect) {
      clearTimeout(this.timer);
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <main className={cn(classes.container)}>
        <NotificationContainer />
        <div className={cn(classes.headersContainer)}>
          <span className={cn(classes.header)}>
            <T message="Введите номер вашего мобильного телефона." />
          </span>
          <span className={cn(classes.header, classes.subheader)}>
            <T message="Вы получите SMS с номером билета." />
          </span>
        </div>
        <Alert show={this.state.showAlert} variant="danger">
          <Typography variant="p">
            Пожалуйста полностью введите ваш номер
          </Typography>
        </Alert>
        <div className={cn(classes.inputPhoneNumberController)}>
          <InputMask
            mask="(+\9\92) 999 999 999"
            maskChar=""
            alwaysShowMask={true}
            value={this.state.customerPhoneNumber}
            className={cn(classes.phoneNumberInput)}
            onChange={this.setCustomerPhoneNumber}
          />
          <button
            type={"button"}
            className={cn(classes.removeNumberButton)}
            onClick={this.handleRemoveNumberClick}
          >
            <img
              src={removeNumberImg}
              className={cn(classes.removeNumberImage)}
            />
          </button>
        </div>
        <div className={cn(classes.numbersContainer)}>
          <div>
            <Button
              className={cn(classes.customerPhoneNumberButton)}
              type="button"
              variant="info"
              onClick={() => this.handleAddNumberClick("1")}
            >
              1{" "}
            </Button>
            <Button
              className={cn(classes.customerPhoneNumberButton)}
              type="button"
              variant="info"
              onClick={() => this.handleAddNumberClick("2")}
            >
              2{" "}
            </Button>
            <Button
              className={cn(classes.customerPhoneNumberButton)}
              type="button"
              variant="info"
              onClick={() => this.handleAddNumberClick("3")}
            >
              3{" "}
            </Button>
          </div>
          <div>
            <Button
              className={cn(classes.customerPhoneNumberButton)}
              type="button"
              variant="info"
              onClick={() => this.handleAddNumberClick("4")}
            >
              4{" "}
            </Button>
            <Button
              className={cn(classes.customerPhoneNumberButton)}
              type="button"
              variant="info"
              onClick={() => this.handleAddNumberClick("5")}
            >
              5{" "}
            </Button>
            <Button
              className={cn(classes.customerPhoneNumberButton)}
              type="button"
              variant="info"
              onClick={() => this.handleAddNumberClick("6")}
            >
              6{" "}
            </Button>
          </div>
          <div>
            <Button
              className={cn(classes.customerPhoneNumberButton)}
              type="button"
              variant="info"
              onClick={() => this.handleAddNumberClick("7")}
            >
              7{" "}
            </Button>
            <Button
              className={cn(classes.customerPhoneNumberButton)}
              type="button"
              variant="info"
              onClick={() => this.handleAddNumberClick("8")}
            >
              8{" "}
            </Button>
            <Button
              className={cn(classes.customerPhoneNumberButton)}
              type="button"
              variant="info"
              onClick={() => this.handleAddNumberClick("9")}
            >
              9{" "}
            </Button>
          </div>
          <div className="text-center">
            <Button
              className={cn(classes.customerPhoneNumberButton)}
              type="button"
              variant="info"
              onClick={() => this.handleAddNumberClick("0")}
            >
              0{" "}
            </Button>
          </div>
        </div>
        <div className={cn(classes.redirectButtons)}>
          <button
            onClick={this.handleReturnBackButtonClick}
            className={cn(classes.redirectButton, "btn")}
          >
            {BackButton()}
            <span className={cn(classes.backRedirectButtonText)}>
              <T message="Назад" />
            </span>
          </button>
          <button
            onClick={this.handleGetTicketButtonClick}
            className={cn(classes.redirectButton, "btn")}
            disabled={this.state.disabled}
          >
            {SentButton()}
            <span className={cn(classes.sentButtonText)}>
              <T message="Отправить" />
            </span>
          </button>
        </div>
        <Button
          type="button"
          variant="link"
          onClick={this.handleGetTicketButtonClick}
          className={cn(classes.redirectButton)}
        >
          <T message="Нет мобильного телефона" />
        </Button>
      </main>
    );
  }
}

export default withLocale(CustomerPhoneNumberPage);
