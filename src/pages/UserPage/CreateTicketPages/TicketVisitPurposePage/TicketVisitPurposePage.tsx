import React from "react";
import { PureComponent } from "react";
import cn from "clsx";
import classes from "./TicketVisitPurposePage.css";
import { Button } from "react-bootstrap";
import TicketVisitPurposePageService, {
  VisitPurpose,
  VisitPurposes,
} from "src/services/CreateTicketPagesService/TicketVisitPurposePageService/TicketVisitPurposePageService";
import { Redirect } from "react-router-dom";
import { routes } from "src/config/routes";
import store from "src/redux/CreateTicket";
import { setVisitPurposeIdValue } from "src/redux/CreateTicket/actions";
import { T, WithLocale, withLocale } from "react-targem";
import { branchOfficeFullName } from "src/helpers/auth-data";

interface TicketVisitPurposePageStates {
  visitPurposeOptions: VisitPurposes;
  redirect: string;
}

interface TicketVisitPurposePageProps extends WithLocale {}

class TicketVisitPurposePage extends PureComponent<
  TicketVisitPurposePageProps,
  TicketVisitPurposePageStates
> {
  private service: TicketVisitPurposePageService;
  private timer: NodeJS.Timeout;

  constructor(props: TicketVisitPurposePageProps) {
    super(props);
    this.state = { redirect: "", visitPurposeOptions: [{ id: 0, name: "" }] };
    this.service = new TicketVisitPurposePageService();
    this.timer = setTimeout(() => {
      this.setState({ redirect: routes.registerTicket });
    }, 60000);
  }

  componentDidMount(): void {
    this.setVisitPurposeList();
    if (!store.getState().isRegisteredTicket) {
      this.setState({ redirect: routes.registerTicket });
    }
  }

  async setVisitPurposeList() {
    const userJsonData = localStorage.getItem("queue-user-data");
    const userData = JSON.parse(userJsonData ? userJsonData : " ");
    const branchOfficeId = userData.branchOfficeId;
    const visitPurposeList = await this.service.getVisitPurposeList(
      branchOfficeId
    );
    this.setState({ visitPurposeOptions: visitPurposeList });
  }

  getVisitPurposeList() {
    return this.state.visitPurposeOptions.map(
      (visitPurpose: VisitPurpose, index: number) => {
        return (
          <Button
            variant="secondary"
            key={index}
            className={cn(classes.visitPurposeButton)}
            type="button"
            onClick={() => this.handleButtonClick(visitPurpose.id)}
          >
            {visitPurpose.name}
          </Button>
        );
      }
    );
  }

  handleButtonClick = (visitPurposeId: number) => {
    clearTimeout(this.timer);
    store.dispatch(setVisitPurposeIdValue(visitPurposeId));
    this.setState({ redirect: routes.ticketCustomerPhoneNumber });
  };

  render(): React.ReactNode {
    if (this.state.redirect) {
      clearTimeout(this.timer);
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <main className={cn(classes.container)}>
        <div className={cn(classes.visitPurposesContainer)}>
          <span className={cn(classes.header)}>
            <T message="Цель вашего визита" />
          </span>
          <span className={cn(classes.header, classes.branchOffice)}>
            {branchOfficeFullName()}
          </span>
          {this.getVisitPurposeList()}
        </div>
        <div className={cn(classes.redirectButtonContainer)}>
          <Button
            type="button"
            variant="link"
            onClick={this.handleRedirectToRegisterTicketPageClick}
            className={cn(classes.redirectButton)}
          >
            <span className={cn(classes.redirectButtonText)}>
              <T message="Вернуться на главную" />
            </span>
          </Button>
        </div>
      </main>
    );
  }

  handleRedirectToRegisterTicketPageClick = () => {
    clearTimeout(this.timer);
    this.setState({ redirect: routes.registerTicket });
  };
}

export default withLocale(TicketVisitPurposePage);
