import React from "react";
import { PureComponent } from "react";
import cn from "clsx";
import classes from "./ProfilePage.css";
import Typography from "src/components/Typography";
import { Button, Form, Modal } from "react-bootstrap";
import ProfilePageService, {
  BranchOffice,
  BranchOffices,
  VisitPurpose,
  VisitPurposes,
} from "src/services/ProfilePageService/ProfilePageService";
import {
  branchOfficeFullName,
  branchOfficeId,
  deskNumber,
  userFullName,
  visitPurposesIds,
  visitPurposesNames,
} from "src/helpers/auth-data";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { createNotification } from "src/helpers/crud-notifications";
// @ts-ignore
import Select from "react-select";
interface ProfilePageStates {
  deskNumber: string;
  branchOfficeId: string;
  branchOfficeOptions: BranchOffices;
  showProfileData: boolean;
  disabled: boolean;
  visitPurposesList: VisitPurposes;
  visitPurposesIds: number[];
  showDeskNumberCheckModal: boolean;
  userFirstName: string;
  userIdWithSameDeskNumber: number | null;
}

class ProfilePage extends PureComponent<{}, ProfilePageStates> {
  private service: ProfilePageService;

  constructor(props: {}) {
    super(props);

    this.state = {
      deskNumber: deskNumber(),
      branchOfficeId: branchOfficeId(),
      visitPurposesIds: visitPurposesIds(),
      visitPurposesList: [{ id: 0, name: "" }],
      branchOfficeOptions: [{ id: 1, branchOfficeName: "" }],
      showProfileData: false,
      disabled: true,
      showDeskNumberCheckModal: false,
      userFirstName: "",
      userIdWithSameDeskNumber: null,
    };
    this.service = new ProfilePageService();
  }

  componentDidMount(): void {
    this.setDisabledFalse();
    if (deskNumber() == "" || deskNumber() == null) {
      createNotification(
        "warning",
        "У вас не выбран номер столика, пожалуйста выберите"
      );
    }
    if (visitPurposesIds().length == 0) {
      createNotification(
        "warning",
        "У вас не выбрана цель визита, пожалуйста выберите"
      );
    }
    if (branchOfficeId() == "" || branchOfficeId() == null) {
      createNotification(
        "warning",
        "У вас не выбран филиал, пожалуйста выберите"
      );
    }
    this.setBranchOfficeList();
  }

  async setBranchOfficeList(): Promise<void> {
    const branchOfficeList = await this.service.getBranchOfficeList();
    await this.setState({ branchOfficeOptions: branchOfficeList });
    await this.setVisitPurposeList(this.state.branchOfficeId);
  }

  getBranchOfficeList(): JSX.Element[] {
    return this.state.branchOfficeOptions.map(
      (branchOffice: BranchOffice, index: number) => {
        return (
          <option key={index} value={branchOffice.id}>
            {branchOffice.branchOfficeName}
          </option>
        );
      }
    );
  }

  async setVisitPurposeList(branchOfficeId: string): Promise<void> {
    const visitPurposesList = await this.service.getVisitPurposeList(
      Number(branchOfficeId)
    );
    this.setState({ visitPurposesList: visitPurposesList });
  }

  setDeskNumber = async (
    event: React.ChangeEvent<HTMLInputElement>
  ): Promise<void> => {
    await this.setState({ deskNumber: event.target.value });
    this.setDisabledFalse();
  };

  setBranchOfficeId = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ branchOfficeId: event.target.value });
    this.setVisitPurposeList(event.target.value);
    this.setDisabledFalse();
  };

  getVisitPurposesOptions(): any {
    const options = this.state.visitPurposesList.map(
      (visitPurpose: VisitPurpose) => {
        return { value: visitPurpose.id, label: visitPurpose.name };
      }
    );
    return options;
  }

  setVisitPurposesIds = async (
    selectedVisitPurpose: [{ value: number }]
  ): Promise<void> => {
    const options = selectedVisitPurpose;
    const selectedOptions = [];

    for (let i = 0; i < options.length; i++) {
      selectedOptions.push(options[i].value);
    }

    await this.setState({ visitPurposesIds: selectedOptions });
    await this.setDisabledFalse();
  };

  setDisabledFalse() {
    if (
      this.state.branchOfficeId != "" &&
      this.state.branchOfficeId != "0" &&
      this.state.deskNumber != "" &&
      this.state.visitPurposesIds.length != 0
    ) {
      this.setState({ disabled: false });
    } else {
      this.setState({ disabled: true });
    }
  }

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <NotificationContainer />
        <div className={cn(classes.userSettingsContainer)}>
          {this.getConfirmationOfUpdatingUserSettingsModal()}
          {this.getProfileBlock()}
        </div>
      </main>
    );
  }

  getConfirmationOfUpdatingUserSettingsModal(): JSX.Element {
    return (
      <Modal
        className={cn(classes.modalDiolog)}
        show={this.state.showDeskNumberCheckModal}
        onHide={this.handleCloseConfirmationOfUpdatingUserSettingsModalClick}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography variant="span">Изменить настройки</Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Typography variant="span">
            На данный момент столик занят {this.state.userFirstName}. Хотите
            обновить настройки?
          </Typography>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleUpdateUserSettingClick}
          >
            <Typography color="white" variant="span">
              Да
            </Typography>
          </Button>
          <Button
            variant="primary"
            onClick={
              this.handleCloseConfirmationOfUpdatingUserSettingsModalClick
            }
          >
            <Typography color="white" variant="span">
              Нет
            </Typography>
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  handleUpdateUserSettingClick = async (): Promise<void> => {
    const response = await this.service.updateUserSettings(
      this.state.userIdWithSameDeskNumber,
      this.state.deskNumber,
      this.state.branchOfficeId,
      this.state.visitPurposesIds
    );

    if (response == "User's settings are updated") {
      createNotification("success", "Пользователь обновлён");
    }

    this.handleCloseConfirmationOfUpdatingUserSettingsModalClick();
  };

  handleCloseConfirmationOfUpdatingUserSettingsModalClick = (): void => {
    this.setState({ showDeskNumberCheckModal: false });
  };

  getProfileBlock() {
    if (this.state.showProfileData) {
      return (
        <Form className={cn(classes.formContainer)}>
          <div className={cn(classes.userSettings)}>
            <div className={cn(classes.userSettingsCard)}>
              <span className={cn(classes.settingCardHeader)}>
                Шаг 1. Выберите филиал
              </span>
              <div>
                <Form.Control
                  as="select"
                  value={this.state.branchOfficeId}
                  onChange={this.setBranchOfficeId}
                  className={cn(classes.branchOfficeSelector)}
                  custom
                >
                  <option value="0">Пожалуйста выберите филиал</option>
                  {this.getBranchOfficeList()}
                </Form.Control>
              </div>
            </div>
            <div className={cn(classes.userSettingsCard)}>
              <span className={cn(classes.settingCardHeader)}>
                Шаг 2. Выберите номер столика
              </span>
              <Form.Control
                type="number"
                value={this.state.deskNumber}
                onChange={this.setDeskNumber}
                min="1"
              />
            </div>
            <div className={cn(classes.userSettingsCard)}>
              <span className={cn(classes.settingCardHeader)}>
                Шаг 3. Выберите цель визита клиента
              </span>
              <Form.Group className={cn(classes.formGroup)}>
                <Select
                  options={this.getVisitPurposesOptions()}
                  isMulti
                  onChange={this.setVisitPurposesIds}
                />
              </Form.Group>
            </div>
          </div>
          <div className={cn(classes.buttonsContainer)}>
            <Button
              className={cn(classes.setButton)}
              variant="secondary"
              type="submit"
              onClick={this.handleSubmitClick}
              disabled={this.state.disabled}
            >
              Установить настройки
            </Button>
            <Button
              variant="primary"
              type="submit"
              onClick={this.handleCancelEditionClick}
            >
              Отменить
            </Button>
          </div>
        </Form>
      );
    }

    return (
      <div>
        <div className={cn(classes.userSettings)}>
          <div className={cn(classes.userSettingsCard)}>
            <span className={cn(classes.settingCardHeader)}>
              Общие сведения пользователя
            </span>
            <div>
              <Typography variant="span">Полное имя: </Typography>
              <Typography variant="span">{userFullName()}</Typography>
            </div>
          </div>
          <div className={cn(classes.userSettingsCard)}>
            <span className={cn(classes.settingCardHeader)}>
              Текущий филиал
            </span>
            <div>
              <Typography variant="span">Филиал: </Typography>
              <Typography variant="span">{branchOfficeFullName()}</Typography>
            </div>
          </div>
          <div className={cn(classes.userSettingsCard)}>
            <span className={cn(classes.settingCardHeader)}>
              Текущий номер столика
            </span>
            <div>
              <Typography variant="span">Номер столика: </Typography>
              <Typography variant="span">{deskNumber()}</Typography>
            </div>
          </div>
          <div className={cn(classes.userSettingsCard)}>
            <span className={cn(classes.settingCardHeader)}>
              Текущая цели визита
            </span>
            <div>
              <Typography variant="span">Цель визита: </Typography>
              <Typography variant="span">
                {this.getVisitPurposesRows()}
              </Typography>
            </div>
          </div>
        </div>
        <div className={cn(classes.buttonsContainer)}>
          <Button
            variant="primary"
            type="submit"
            onClick={this.handleEditClick}
          >
            Редактировать настройки
          </Button>
        </div>
      </div>
    );
  }

  handleSubmitClick = async (
    event: React.MouseEvent<HTMLElement, MouseEvent>
  ): Promise<void> => {
    event.preventDefault();

    const response = await this.service.checkDeskNumberExistance(
      this.state.deskNumber,
      this.state.branchOfficeId,
      this.state.visitPurposesIds
    );

    if (response.message == "Update is allowed") {
      const response = await this.service.updateUserSettings(
        this.state.userIdWithSameDeskNumber,
        this.state.deskNumber,
        this.state.branchOfficeId,
        this.state.visitPurposesIds
      );

      if (response == "User's settings are updated") {
        createNotification("success", "Пользователь обновлён");
      }
    } else {
      this.setState({ userFirstName: response.userFirstName });
      this.setState({
        userIdWithSameDeskNumber: response.userIdWithSameDeskNumber,
      });
      this.setState({ showDeskNumberCheckModal: true });
    }
  };

  handleEditClick = (): void => {
    this.setState({ showProfileData: true });
  };

  handleCancelEditionClick = (): void => {
    this.setState({ showProfileData: false });
  };

  getVisitPurposesRows(): any {
    const arrayLength = visitPurposesNames().length;
    return visitPurposesNames().map(
      (visitPurposeName: string, index: number) => {
        if (arrayLength == index + 1) {
          return visitPurposeName;
        }

        return visitPurposeName + ", ";
      }
    );
  }
}

export default ProfilePage;
