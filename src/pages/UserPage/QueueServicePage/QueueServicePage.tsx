import { PureComponent } from "react";
import cn from "clsx";
import classes from "./QueueServicePage.css";
import React from "react";
import Typography from "src/components/Typography";
import QueueServicePageService, {
  Service,
  Services,
  Ticket,
  Tickets,
  VisitPurpose,
  VisitPurposes,
} from "src/services/QueueServicePageService/QueueServicePageService";
import { createEchoInstance } from "src/helpers/echo";
import { Button, Form, Modal } from "react-bootstrap";
import {
  userId,
  visitPurposesIds,
  branchOfficeId,
  deskNumber,
  timer,
} from "src/helpers/auth-data";
import statisticsImg from "src/pages/UserPage/QueueServicePage/images/statistics.svg";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { createNotification } from "src/helpers/crud-notifications";
// @ts-ignore
import ReactNotifications from "react-browser-notifications";

interface QueueServicePageStates {
  acceptedVisitPurposeName: string;
  ticketsPendingCards: Tickets;
  acceptedTicketCard: Ticket;
  ticketServingCard: Ticket;
  showModal: boolean;
  visitPurposeOptions: VisitPurposes;
  serviceOptions: Services;
  selectedServiceId: string;
  selectedVisitPurposeId: string;
  customersAmountUserServed: string;
  usersAverageServiceTime: string | null;
  usersMaxServiceTime: string | null;
  userAverageServiceTime: string | null;
  isDisabled: boolean;
  browserNotificationMessaage: string;
  isAcceptTicketButtonDisabled: boolean;
}

class QueueServicePage extends PureComponent<
  { isAcceptTicketButtonDisabled: boolean },
  QueueServicePageStates
> {
  private echo: any;
  private service: QueueServicePageService;
  private notification: ReactNotifications;
  private timer: number;
  private ticketPendingTimerLoop: any;
  private ticketServiceTimerLoop: any;

  constructor(props: { isAcceptTicketButtonDisabled: boolean }) {
    super(props);
    this.service = new QueueServicePageService();
    this.state = {
      acceptedVisitPurposeName: "",
      ticketsPendingCards: [],
      acceptedTicketCard: {
        id: 0,
        crmCustomerId: 0,
        ticketFullNumber: "",
        customerFullName: "",
        customerPhoneNumber: "",
        createdAt: "",
        pendingTime: "",
        visitPurposeName: "",
        visitPurposeId: 0,
        isSalomCache: false,
      },
      ticketServingCard: {
        id: 0,
        crmCustomerId: 0,
        ticketFullNumber: "",
        customerFullName: "",
        customerPhoneNumber: "",
        createdAt: "",
        pendingTime: "",
        visitPurposeName: "",
        visitPurposeId: 0,
        isSalomCache: false,
      },
      visitPurposeOptions: [{ id: 0, name: "" }],
      selectedServiceId: "",
      selectedVisitPurposeId: "",
      customersAmountUserServed: "",
      usersAverageServiceTime: "",
      usersMaxServiceTime: "",
      serviceOptions: [{ id: 0, name: "" }],
      userAverageServiceTime: "",
      showModal: false,
      isDisabled: true,
      browserNotificationMessaage: "",
      isAcceptTicketButtonDisabled: props.isAcceptTicketButtonDisabled,
    };
    this.timer = timer();
  }

  componentDidMount(): void {
    this.setTicketsPendingCards();
    this.setAcceptedTicketCard();
    this.setTicketServingCard();
    this.connectEcho();
    this.listenTicketsEvent();
    if (deskNumber() == "" || deskNumber() == null) {
      createNotification(
        "warning",
        "У вас не выбран номер столика, пожалуйста выберите"
      );
    }
    if (visitPurposesIds().length == 0) {
      createNotification(
        "warning",
        "У вас не выбрана цель визита, пожалуйста выберите"
      );
    }
    if (branchOfficeId() == "" || branchOfficeId() == null) {
      createNotification(
        "warning",
        "У вас не выбран филиал, пожалуйста выберите"
      );
    }
    this.setStatistics();
  }

  componentWillUnmount() {
    this.disconnectEcho();
  }

  private connectEcho(): void {
    this.echo = createEchoInstance();
  }

  private disconnectEcho(): void {
    this.echo.disconnect();
  }

  private listenTicketsEvent(): void {
    visitPurposesIds().map((visitPurposeId: number) => {
      this.echo
        .channel(
          "ticket." +
            branchOfficeId() +
            "." +
            visitPurposeId +
            ".pending.recover"
        )
        .listen(".ticket-pending", (data: { ticketPending: Ticket }): void => {
          const ticketsPendingCards = [
            data.ticketPending,
            ...this.state.ticketsPendingCards,
          ];
          this.setState({ ticketsPendingCards: ticketsPendingCards });
          if (
            this.state.ticketsPendingCards.length > 0 &&
            this.state.ticketServingCard.id == 0 &&
            this.state.acceptedTicketCard.id == 0
          ) {
            this.setTicketPendingTimer();
          }
        });

      this.echo
        .channel(
          "ticket." + branchOfficeId() + "." + visitPurposeId + ".pending.add"
        )
        .listen(".ticket-pending", (data: { ticketPending: Ticket }): void => {
          const ticketsPendingCards = [
            ...this.state.ticketsPendingCards,
            data.ticketPending,
          ];
          const sortedTicketsPendingCards = ticketsPendingCards.sort((x, y) => {
            if (x.isSalomCache === y.isSalomCache) return 0;
            if (x.isSalomCache) return -1;
            return 1;
          });
          this.setState({ ticketsPendingCards: sortedTicketsPendingCards });
          if (
            this.state.ticketsPendingCards.length > 0 &&
            this.state.ticketServingCard.id == 0 &&
            this.state.acceptedTicketCard.id == 0
          ) {
            this.setTicketPendingTimer();
          }
        });

      this.echo
        .channel(
          "ticket." +
            branchOfficeId() +
            "." +
            visitPurposeId +
            ".pending.delete"
        )
        .listen(
          ".ticket-pending",
          async (data: { ticketPending: Ticket }): Promise<void> => {
            const index = Object.values(
              this.state.ticketsPendingCards
            ).findIndex((value) => value.id === data.ticketPending.id);
            if (index != -1) {
              this.state.ticketsPendingCards.splice(index, 1);
              const ticketsPendingCards = [...this.state.ticketsPendingCards];
              await this.setState({ ticketsPendingCards: ticketsPendingCards });
            }
            const ticketsPendingCards = [...this.state.ticketsPendingCards];
            const sortedTicketsPendingCards = ticketsPendingCards.sort(
              (x, y) => {
                if (x.isSalomCache === y.isSalomCache) return 0;
                if (x.isSalomCache) return -1;
                return 1;
              }
            );
            this.setState({ ticketsPendingCards: sortedTicketsPendingCards });
          }
        );

      this.echo
        .channel(
          "ticket." +
            userId() +
            "." +
            branchOfficeId() +
            "." +
            visitPurposeId +
            ".accepted.add"
        )
        .listen(
          ".ticket-accepted",
          (data: { acceptedTicket: Ticket }): void => {
            this.setState({ acceptedTicketCard: data.acceptedTicket });
            this.setTicketServiceTimer();
          }
        );

      this.echo
        .channel(
          "ticket." +
            userId() +
            "." +
            branchOfficeId() +
            "." +
            visitPurposeId +
            ".accepted.delete"
        )
        .listen(".ticket-accepted", (): void => {
          this.setState({
            acceptedTicketCard: {
              id: 0,
              crmCustomerId: 0,
              ticketFullNumber: "",
              customerFullName: "",
              customerPhoneNumber: "",
              createdAt: "",
              pendingTime: "",
              visitPurposeName: "",
              visitPurposeId: 0,
              isSalomCache: false,
            },
          });
        });
    });

    this.echo
      .channel("ticket." + userId() + ".serving.add")
      .listen(".ticket-serving", (data: { ticketServing: Ticket }): void => {
        this.setState({ ticketServingCard: data.ticketServing });
        clearInterval(this.ticketServiceTimerLoop);
      });

    this.echo
      .channel("ticket." + userId() + ".serving.delete")
      .listen(".ticket-serving", (): void => {
        this.setState({
          ticketServingCard: {
            id: 0,
            crmCustomerId: 0,
            ticketFullNumber: "",
            customerFullName: "",
            customerPhoneNumber: "",
            createdAt: "",
            pendingTime: "",
            visitPurposeName: "",
            visitPurposeId: 0,
            isSalomCache: false,
          },
        });
      });
    this.echo.channel("users." + userId() + ".statistics").listen(
      ".statistics-data",
      (data: {
        userStatisticsData: {
          usersAverageServiceTime: string;
          userAverageServiceTime: string;
          customersAmountUserServed: string;
          usersMaxServiceTime: string;
        };
      }): void => {
        this.setState({
          userAverageServiceTime:
            data.userStatisticsData.userAverageServiceTime,
          usersAverageServiceTime:
            data.userStatisticsData.usersAverageServiceTime,
          customersAmountUserServed:
            data.userStatisticsData.customersAmountUserServed,
          usersMaxServiceTime: data.userStatisticsData.usersMaxServiceTime,
        });
      }
    );
  }

  setTicketServiceTimer() {
    clearInterval(this.ticketPendingTimerLoop);
    this.setState({
      browserNotificationMessaage:
        'Вы забыли нажать кнопку "обслуживаю", пожалуйста будьте внимательнее.',
    });
    this.ticketServiceTimerLoop = setTimeout(() => {
      this.notification.show();
    }, this.timer);
  }

  setTicketPendingTimer() {
    clearInterval(this.ticketServiceTimerLoop);
    clearInterval(this.ticketPendingTimerLoop);
    this.setState({
      browserNotificationMessaage:
        'Вы сейчас свободны, пожалуйста позовите клиента, нажмите кнопку "Принять клиента".',
    });
    this.ticketPendingTimerLoop = setInterval(() => {
      this.notification.show();
    }, this.timer);
  }
  setStatistics = async () => {
    const customersAmountUserServed = await this.service.getCustomersAmountUserServed();
    const userAverageServiceTime = await this.service.getUserAverageServiceTime();
    const usersAverageServiceTime = await this.service.getUsersAverageServiceTime();
    const usersMaxServiceTime = await this.service.getUsersMaxServiceTime();

    if (customersAmountUserServed) {
      this.setState({
        customersAmountUserServed: customersAmountUserServed,
        userAverageServiceTime: userAverageServiceTime,
        usersAverageServiceTime: usersAverageServiceTime,
        usersMaxServiceTime: usersMaxServiceTime,
      });
    }
  };

  setTicketsPendingCards = async () => {
    const ticketsPendingList = await this.service.getTicketsPendingList();
    if (ticketsPendingList) {
      await this.setState({ ticketsPendingCards: ticketsPendingList });
    }
  };

  getTicketsPendingCards(): JSX.Element[] {
    return this.state.ticketsPendingCards.map(
      (ticketPending: Ticket, index: number) => {
        return (
          <div key={index} className={cn(classes.ticketPendingCard)}>
            <div className={cn(classes.ticketInfo)}>
              <div className={cn(classes.visitPurposeContaner)}>
                <Typography fontWeight="bold" color="muted" variant="span">
                  {ticketPending.visitPurposeName}
                </Typography>
              </div>
              <div className={cn(classes.ticketNumber)}>
                <Typography color="muted" variant="span">
                  {ticketPending.ticketFullNumber}
                </Typography>
              </div>
            </div>
            <div className={cn(classes.mainInfo)}>
              <div>
                <div>
                  <Typography fontWeight="bold" color="muted" variant="span">
                    Выдали тикет:
                  </Typography>
                </div>
                <div>
                  <Typography color="muted" variant="span">
                    {ticketPending.createdAt}
                  </Typography>
                </div>
              </div>
              {ticketPending.crmCustomerId ? (
                <div className={cn(classes.externlaIntegrationCards)}>
                  <a
                    className={cn(classes.crmLink)}
                    href={`https://crm3.alif.tj/clients/${ticketPending.crmCustomerId}`}
                    target="_blank"
                    rel="noreferrer"
                  >
                    CRM
                  </a>
                  <a
                    className={cn(classes.abcLink)}
                    href={`https://core.alif.tj/core/clients/crm/${ticketPending.crmCustomerId}`}
                  >
                    ABC
                  </a>
                </div>
              ) : (
                <div>
                  <div className={cn(classes.newCustomer)}>
                    <Typography color="white" variant="span">
                      Новый клиент
                    </Typography>
                  </div>
                </div>
              )}
            </div>
          </div>
        );
      }
    );
  }

  setAcceptedTicketCard = async (): Promise<void> => {
    const acceptedTicket = await this.service.getAcceptedTicket();

    if (acceptedTicket != null) {
      if (Object.keys(acceptedTicket).length !== 0) {
        await this.setState({ acceptedTicketCard: acceptedTicket });
      }
    }
  };

  getAcceptedTicketCard(): JSX.Element {
    this.setState({
      acceptedVisitPurposeName: this.state.acceptedTicketCard.visitPurposeName,
    });
    return (
      <div className={cn(classes.ticketServiceCard)}>
        <div className={cn(classes.ticketInfo)}>
          <div className={cn(classes.visitPurposeContaner)}>
            <Typography fontWeight="bold" color="muted" variant="span">
              {this.state.acceptedTicketCard.visitPurposeName}
            </Typography>
          </div>
          <div className={cn(classes.ticketNumber)}>
            <Typography color="muted" variant="span">
              {this.state.acceptedTicketCard.ticketFullNumber}
            </Typography>
          </div>
        </div>
        <div className={cn(classes.customerPhoneNumber)}>
          <Typography fontWeight="bold" color="muted" variant="span">
            Номер телефона:
          </Typography>
          <Typography color="muted" variant="span">
            {this.state.acceptedTicketCard.customerPhoneNumber}
          </Typography>
        </div>
        <div className={cn(classes.pendingTime)}>
          <Typography fontWeight="bold" color="muted" variant="span">
            Сколько клиент ждал:
          </Typography>
          <Typography color="muted" variant="span">
            {this.state.acceptedTicketCard.pendingTime}
          </Typography>
        </div>
        <div className={cn(classes.mainInfo)}>
          <div>
            <div>
              <Typography fontWeight="bold" color="muted" variant="span">
                Выдали тикет:
              </Typography>
            </div>
            <div>
              <Typography color="muted" variant="span">
                {this.state.acceptedTicketCard.createdAt}
              </Typography>
            </div>
          </div>
          {this.state.acceptedTicketCard.crmCustomerId ? (
            <div className={cn(classes.externlaIntegrationCards)}>
              <a
                className={cn(classes.crmLink)}
                href={`https://crm3.alif.tj/clients/${this.state.acceptedTicketCard.crmCustomerId}`}
                target="_blank"
                rel="noreferrer"
              >
                CRM
              </a>
              <a
                className={cn(classes.abcLink)}
                href={`https://core.alif.tj/core/clients/crm/${this.state.acceptedTicketCard.crmCustomerId}`}
              >
                ABC
              </a>
            </div>
          ) : (
            <div>
              <div className={cn(classes.newCustomer)}>
                <Typography color="white" variant="span">
                  Новый клиент
                </Typography>
              </div>
            </div>
          )}
        </div>
        <div className={cn(classes.acceptedTicketButtons)}>
          <Button
            variant="primary"
            type="button"
            onClick={() => {
              this.handleServeTicketClick(this.state.acceptedTicketCard.id);
            }}
          >
            Обслужить клиента
          </Button>
          <Button
            variant="primary"
            type="button"
            onClick={() => {
              this.handleDeclineTicketClick(this.state.acceptedTicketCard.id);
            }}
          >
            Клиент не подошел
          </Button>
        </div>
      </div>
    );
  }

  setTicketServingCard = async (): Promise<void> => {
    const ticketServing = await this.service.getTicketServing();

    if (ticketServing != null) {
      if (Object.keys(ticketServing).length !== 0) {
        await this.setState({ ticketServingCard: ticketServing });
      }
    }
  };

  getTicketServingCard(): JSX.Element {
    return (
      <div className={cn(classes.ticketServiceCard)}>
        <div className={cn(classes.ticketInfo)}>
          <div className={cn(classes.visitPurposeContaner)}>
            <Typography fontWeight="bold" color="muted" variant="span">
              {this.state.ticketServingCard.visitPurposeName}
            </Typography>
          </div>
          <div className={cn(classes.ticketNumber)}>
            <Typography color="muted" variant="span">
              {this.state.ticketServingCard.ticketFullNumber}
            </Typography>
          </div>
        </div>
        <div className={cn(classes.customerPhoneNumber)}>
          <Typography fontWeight="bold" color="muted" variant="span">
            Номер телефона:
          </Typography>
          <Typography color="muted" variant="span">
            {this.state.ticketServingCard.customerPhoneNumber}
          </Typography>
        </div>
        <div className={cn(classes.pendingTime)}>
          <Typography fontWeight="bold" color="muted" variant="span">
            Сколько клиент ждал:
          </Typography>
          <Typography color="muted" variant="span">
            {this.state.ticketServingCard.pendingTime}
          </Typography>
        </div>
        <div className={cn(classes.mainInfo)}>
          <div>
            <div>
              <Typography fontWeight="bold" color="muted" variant="span">
                Выдали тикет:
              </Typography>
            </div>
            <div>
              <Typography color="muted" variant="span">
                {this.state.ticketServingCard.createdAt}
              </Typography>
            </div>
          </div>
          {this.state.ticketServingCard.crmCustomerId ? (
            <div className={cn(classes.externlaIntegrationCards)}>
              <a
                className={cn(classes.crmLink)}
                href={`https://crm3.alif.tj/clients/${this.state.ticketServingCard.crmCustomerId}`}
                target="_blank"
                rel="noreferrer"
              >
                CRM
              </a>
              <a
                className={cn(classes.abcLink)}
                href={`https://core.alif.tj/core/clients/crm/${this.state.ticketServingCard.crmCustomerId}`}
              >
                ABC
              </a>
            </div>
          ) : (
            <div>
              <div className={cn(classes.newCustomer)}>
                <Typography color="white" variant="span">
                  Новый клиент
                </Typography>
              </div>
            </div>
          )}
        </div>
        <div className={cn(classes.acceptedTicketButtons)}>
          <Button
            type="button"
            onClick={() => {
              this.handleShowModalClick();
            }}
          >
            Сделано
          </Button>
        </div>
        <Modal show={this.state.showModal} onHide={this.handleCloseModalClick}>
          <Modal.Header closeButton>
            <Modal.Title>Обслуживание</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <Form.Group>
                <Form.Control
                  as="select"
                  value={this.state.selectedVisitPurposeId}
                  onChange={this.setSelectedVisitPurposeId}
                  className={cn(classes.serviceSelector)}
                  custom
                >
                  <option>Пожалуйста укажите цель визита</option>
                  {this.getVisitPurposeOptions()}
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Control
                  as="select"
                  value={this.state.selectedServiceId}
                  onChange={this.setSelectedServiceId}
                  className={cn(classes.serviceSelector)}
                  custom
                >
                  <option>Пожалуйста укажите услугу</option>
                  {this.getServiceOptions()}
                </Form.Control>
              </Form.Group>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseModalClick}>
              Отмена
            </Button>
            <Button
              variant="primary"
              type="button"
              onClick={() => {
                this.handleCompleteServiceClick(
                  this.state.ticketServingCard.id
                );
              }}
              disabled={this.state.isDisabled}
            >
              Сохранить
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

  setSelectedVisitPurposeId = (
    event: React.ChangeEvent<HTMLSelectElement>
  ): void => {
    this.setState({ selectedVisitPurposeId: event.target.value });
    this.setServiceList(Number(event.target.value));
  };

  async setServiceList(visitPurposeId: number): Promise<void> {
    const serviceList = await this.service.getServiceList(visitPurposeId);
    if (serviceList) {
      this.setState({ serviceOptions: serviceList });
    }
  }

  setSelectedServiceId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    await this.setState({ selectedServiceId: event.target.value });

    if (this.state.selectedServiceId) {
      await this.setState({ isDisabled: false });
    }
  };

  getVisitPurposeOptions(): JSX.Element[] {
    return this.state.visitPurposeOptions.map(
      (visitPurpose: VisitPurpose, index: number) => {
        return (
          <option key={index} value={visitPurpose.id}>
            {visitPurpose.name}
          </option>
        );
      }
    );
  }

  getServiceOptions(): JSX.Element[] {
    return this.state.serviceOptions.map((service: Service, index: number) => {
      return (
        <option key={index} value={service.id}>
          {service.name}
        </option>
      );
    });
  }

  handleAcceptTicketClick = (): void => {
    this.setState({ isAcceptTicketButtonDisabled: true });
    if (deskNumber() == "") {
      createNotification(
        "warning",
        "У вас не выбран номер столика, пожалуйста выберите"
      );
      return;
    }
    if (visitPurposesIds().length == 0) {
      createNotification(
        "warning",
        "У вас не выбрана цель визита, пожалуйста выберите"
      );
      return;
    }
    if (branchOfficeId() == "") {
      createNotification(
        "warning",
        "У вас не выбран филиал, пожалуйста выберите"
      );
      return;
    }
    this.service.acceptTicket();
  };

  handleServeTicketClick = (ticketId: number): void => {
    this.service.serveTicket(ticketId);
    clearInterval(this.ticketServiceTimerLoop);
  };

  handleDeclineTicketClick = (ticketId: number): void => {
    this.setState({ isAcceptTicketButtonDisabled: false });
    this.service.declineTicket(ticketId);
    this.setTicketPendingTimer();
  };

  handleCompleteServiceClick = (ticketId: number): void => {
    this.setState({ isAcceptTicketButtonDisabled: false });
    this.service.completeService(
      ticketId,
      this.state.selectedServiceId,
      this.state.selectedVisitPurposeId
    );
    this.setTicketPendingTimer();
    clearInterval(this.ticketServiceTimerLoop);
    this.handleCloseModalClick();
  };

  handleCloseModalClick = (): void => {
    this.setState({ showModal: false });
  };

  handleShowModalClick = (): void => {
    this.setVisitPurposeList();
    this.setState({ showModal: true });
  };

  async setVisitPurposeList(): Promise<void> {
    const visitPurposeList = await this.service.getVisitPurposeList();
    this.setState({ visitPurposeOptions: visitPurposeList });
  }

  getCurrentCard(): JSX.Element | "" {
    if (this.state.acceptedTicketCard.id) {
      return this.getAcceptedTicketCard();
    }

    if (this.state.ticketServingCard.id) {
      return this.getTicketServingCard();
    }

    if (this.state.ticketsPendingCards.length == 0) {
      return "";
    }

    return (
      <Button
        disabled={this.state.isAcceptTicketButtonDisabled}
        variant="primary"
        type="button"
        onClick={this.handleAcceptTicketClick}
      >
        Принять клиента
      </Button>
    );
  }

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <ReactNotifications
          onRef={(ref: ReactNotifications) => {
            this.notification = ref;
          }}
          title="Q.ALIF"
          body={this.state.browserNotificationMessaage}
          icon="r"
          timeout="2000"
        />

        <NotificationContainer />
        <div className={cn(classes.generalStatistics)}>
          <div className={cn(classes.cardStatistics)}>
            <div className={cn(classes.cardStatisticsInfoContainer)}>
              <div className={cn(classes.cardStatisticsInfo)}>
                <span className={cn(classes.usersAverageServiceTime)}>
                  {this.state.usersAverageServiceTime} мин.
                </span>
                <Typography fontWeight="bold" color="muted" variant="span">
                  {this.state.acceptedVisitPurposeName}
                </Typography>
              </div>
              <img
                src={statisticsImg}
                className={cn(classes.statisticsImage)}
              />
            </div>
            <div className={cn(classes.avarageTimeContainer)}>
              <Typography
                className="statisticTitle"
                color="white"
                variant="span"
              >
                Сред. время обслуживания
              </Typography>
            </div>
          </div>
          <div className={cn(classes.cardStatistics)}>
            <div className={cn(classes.cardStatisticsInfoContainer)}>
              <div className={cn(classes.cardStatisticsInfo)}>
                <span className={cn(classes.usersMaxServiceTime)}>
                  {this.state.usersMaxServiceTime} мин.
                </span>
                <Typography fontWeight="bold" color="muted" variant="span">
                  {this.state.acceptedVisitPurposeName}
                </Typography>
              </div>
              <img
                src={statisticsImg}
                className={cn(classes.statisticsImage)}
              />
            </div>
            <div className={cn(classes.maxTimeContainer)}>
              <Typography
                className="statisticTitle"
                color="white"
                variant="span"
              >
                Макс. время обслуживания
              </Typography>
            </div>
          </div>
          <div className={cn(classes.cardStatistics)}>
            <div className={cn(classes.cardStatisticsInfoContainer)}>
              <div className={cn(classes.cardStatisticsInfo)}>
                <span className={cn(classes.userAverageServiceTime)}>
                  {this.state.userAverageServiceTime} мин.{" "}
                </span>
                <Typography fontWeight="bold" color="muted" variant="span">
                  {this.state.acceptedVisitPurposeName}
                </Typography>
              </div>
              <img
                src={statisticsImg}
                className={cn(classes.statisticsImage)}
              />
            </div>
            <div className={cn(classes.userAvarageTimeContainer)}>
              <Typography
                className="statisticTitle"
                color="white"
                variant="span"
              >
                Моё сред. время обслуживания
              </Typography>
            </div>
          </div>
          <div className={cn(classes.cardStatistics)}>
            <div className={cn(classes.cardStatisticsInfoContainer)}>
              <div className={cn(classes.cardStatisticsInfo)}>
                <span className={cn(classes.customersAmountUserServed)}>
                  {this.state.customersAmountUserServed} клиентов
                </span>
                <Typography color="muted" variant="span">
                  Сегодня
                </Typography>
              </div>
              <img
                src={statisticsImg}
                className={cn(classes.statisticsImage)}
              />
            </div>
            <div className={cn(classes.servedCustomersNumber)}>
              <Typography
                className="statisticTitle"
                color="white"
                variant="span"
              >
                Обслужил
              </Typography>
            </div>
          </div>
        </div>
        <div className={cn(classes.tickets)}>
          <div className={cn(classes.ticket)}>
            <div className={cn(classes.ticketListHeader)}>
              <span className={cn(classes.ticketHeader)}>В ожидании</span>
              <div
                className={
                  this.getTicketsPendingCards().length != 0
                    ? classes.ticketsPendingNumber
                    : ""
                }
              >
                <span className={cn(classes.ticketsAmount)}>
                  {this.getTicketsPendingCards().length != 0
                    ? this.getTicketsPendingCards().length
                    : ""}
                </span>
              </div>
            </div>
            {this.getTicketsPendingCards().slice(0, 2)}
          </div>
          <div className={cn(classes.ticket)}>
            <div className={cn(classes.ticketListHeader)}>
              <span className={cn(classes.ticketHeader)}>Обслуживается</span>
            </div>
            {this.getCurrentCard()}
          </div>
        </div>
      </main>
    );
  }
}

export default QueueServicePage;
